<?php

return [

    'verified' => 'La tua Email è stato verificata!',
    'invalid' => 'Il link di verifica è invalido.',
    'already_verified' => 'Email già verificata.',
    'user' => 'Nessun utente trovata con queste email.',
    'sent' => 'Ti abbiamo inviato l\'email di verifica!',

];
