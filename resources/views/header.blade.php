<style>

    .table-esami{
      border: 1px solid #000;
      border-collapse: collapse;
      width: 100%;
    }

    tr,td,th{
      border: 1px solid #000;
    }

    td{
      padding-left: 5px;
    }

    .button{
        text-transform: uppercase;
        text-decoration: none;
        padding: 10px 15px;
        background: #17a2b8;
        color: #fff;
    }

</style>
