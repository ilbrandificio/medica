<?php 
    setlocale(LC_TIME, 'it_IT');
    $numero_esami= count($data['0']['esami']);
?>

<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<html>
    <head>
    </head>
    <style>
        table{
            margin-top: 30px;
            font-size:16px;
            width:100%;
            border-collapse: collapse;
        }
        table,td,th{
            border: 1px solid #000;  
            padding: 5px
        }
        .logo-pdf{
            width:220px !important;
            padding-top:10px;
            padding-right: 50px;
        }
        .testo-bold{
            font-weight: 800;
        }
        .page-break {
            page-break-after: always;
        }
        .row{
            border:1px solid black;
            padding:4px;
        }
        .column{
            width: auto;
            display:inline-block;
            vertical-align:middle;
        }

        label input[type="checkbox"]{
            position: relative;
            display: inline;
            vertical-align: middle;
        }

    </style>
  
    <body>
        
       <div>
           <p>
             <?php  echo nl2br($data['0']['medico']['intestazione_piano'])?> 
           </p>
       </div>

        <div>Data Prescrizione:  {{date('d/m/Y',strtotime($data['0']['created_at']))}}</div>

        <div class="row" style="margin-top: 30px">
            <div class="column" style="width:100%">
                <span style="text-transform: uppercase"><b>Dati Medico</b></span>   
            </div>
        </div>
        <div class="row">
            <div class="column" style="width:33.3%">
               Medico: Dott. {{$data['0']['medico']['nome']}} {{$data['0']['medico']['cognome']}}
            </div>
            <div class="column" style="width: 33.3%">
                Telefono: {{$data['0']['medico']['telefono']}} 
            </div>
        </div>
        <div class="row">
            <div class="column" style="width:33.3%">
                Codice ODM: {{$data['0']['medico']['codice_odm']}} 
            </div>
            <div class="column" style="width: 33.3%">
                Provincia ODM: {{$data['0']['medico']['provincia_odm']}} 
            </div>
            <div class="column" style="width: 33.3%">
                Numero Enpam: {{$data['0']['medico']['numero_enpam']}}
            </div>
        </div>

        <div class="row" style="margin-top: 30px">
            <div class="column" style="width:100%">
                <span style="text-transform: uppercase"><b>Dati Paziente</b></span>   
            </div>
        </div>
        <div class="row">
            <div class="column" style="width:50%">
                Paziente: {{$data['0']['paziente']['nome']}} {{$data['0']['paziente']['cognome']}}
            </div>
            <div class="column" style="width:50%">
                CF:  {{$data['0']['paziente']['codice_fiscale']}} 
            </div>
        </div>
        <div class="row">
            <div class="column" style="width:33.3%">
                Data Nascita: {{date('d/m/Y',strtotime($data['0']['paziente']['data_nascita']))}}
            </div>
            <div class="column" style="width:33.3%">
                Provincia Nascita:  {{$data['0']['paziente']['provincia_nascita']}} 
            </div>
            <div class="column" style="width:33.3%">
                Provinca ASL:  {{$data['0']['paziente']['provincia_asl']}} 
            </div>
        </div>

        <!-- START TABLE ESAME -->
        <table style="width: 100%">
            <tr>
                <th>
                  <span style="text-transform: uppercase;text-align:left">Esami Prescritti<span>
                </th>
            </tr>
            @if($data['0']['esami']['esami_laboratorio'])
                <tr>
                    <th>ESAMI LABORATORIO</th>
                </tr>
                
                @foreach ($data['0']['esami']['esami_laboratorio'] as $key=>$esame )
                    <tr>
                        <td style="text-transform:uppercase">
                            <p>{{str_replace('_',' ',$key)}}</p>

                            @if($esame['chiavi'])
                                @foreach($esame['chiavi'] as $key=>$value)
                                    <label>
                                        {{str_replace('_',' ',$key)}}
                                        <input type="checkbox" checked={{$value}}>
                                    </label>
                                  
                                @endforeach
                            @endif

                        </td>
                    </tr>
                @endforeach

            @endif
            @if($data['0']['esami']['esami_strumentali'])
                <tr>
                    <th>ESAMI STRUMENTALI</th>
                </tr>

                @foreach($data['0']['esami']['esami_strumentali'] as $key=>$esame_strumentale)
                    <tr>
                        <td style="text-transform:uppercase">
                            <p>{{str_replace('_',' ',$key)}}</p>

                            @if($esame_strumentale['chiavi'])
                                @foreach($esame_strumentale['chiavi'] as $key=>$value)
                                    <label>{{str_replace('_',' ',$key)}} </label>
                                    <input type="checkbox" checked={{$value}}>
                                @endforeach
                            @endif
                        </td>
                    </tr>
                @endforeach
  
            @endif
        </table>
 
        <br/>
        <br/>

        @include('firma')

        
    </body>
</html>
