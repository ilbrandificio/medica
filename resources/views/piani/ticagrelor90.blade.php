<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<html>
    <head>
    </head>
    <style>
        table{
            border:0px;
            font-size:12px;
        }
        tr{
            border:0px !important;
            outline:0px !important;
        }
        .logo-pdf{
            width:220px !important;
            padding-top:10px;
            padding-right: 50px;
        }
        .testo-bold{
            font-weight: 800;
        }
        .page-break {
        page-break-after: always;
    }
    .colonna_cliente{
      width:250px;
    }
    .testo-ordine{
        font-size:14px;
    }
    .table_cliente{
      margin-top:30px;
    }
    p,h6,h4{
      padding:0px;
      margin:0px;
    }
    .check{
      width:12px;
    }
      .fa {
    display: inline;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 1;
    font-family: FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

    </style>
    <body>

      @include('piani.header', array('intestazione'=>$piano['intestazione'],'codicePiano'=>$piano['codicePianoTerapeutico']))

      <h4>La prescrizione di ticagrelor è a carico del SSN solo se rispondente ad una delle seguenti condizioni:</h4>
      <p>
      @if (array_key_exists('ticagrelor_1',$piano['piano']['pianoterapeutico']))
      <img class="check" src="./images/checked_checkbox.png">
      @else
      <img class="check" src="./images/nochecked_checkbox.png">
      @endif
      Sindrome coronarica acuta con o senza innalzamento del tratto ST (angina instabile, infarto miocardio
      NSTEMI, infarto miocardico STEMI) con insorgenza dei sintomi da &#60; 24 ore in associazione con ASA in
      pazienti non trombolisati trattati farmacologicamente o mediante angioplastica coronarica (con o senza
      applicazione di stent).
      </p>
      @if (array_key_exists('ticagrelor_2',$piano['piano']['pianoterapeutico']))
      <img class="check" src="./images/checked_checkbox.png">
      @else
      <img class="check" src="./images/nochecked_checkbox.png">
      @endif
      Intervento di rivascolarizzazione miocardica in pazienti con sindrome coronarica acuta con o senza
     innalzamento del tratto ST in associazione con ASA
     Ticagrelor non deve essere utilizzato in caso di terapia trombolitica nelle 24 ore antecedenti, terapia
     anticoagulante orale, rischio aumentato di bradicardia, trattamento con farmaci inibitori o induttori del
     citocromo P-4503A.<br><br>
     <b>Il trattamento con ticagrelor deve essere iniziato durante il ricovero ospedaliero con dose di attacco di
     180 mg.</b><br><br>
     <b>Nei pazienti con NSTEMI è necessaria la presenza di almeno due dei seguenti criteri:</b><br>
     @if (array_key_exists('ticagrelor_3',$piano['piano']['pianoterapeutico']))
     <img class="check" src="./images/checked_checkbox.png">
     @else
     <img class="check" src="./images/nochecked_checkbox.png">
     @endif
      Alterazioni del tratto ST sull’elettrocardiogramma, indicative di ischemia al ricovero<br>
      @if (array_key_exists('ticagrelor_4',$piano['piano']['pianoterapeutico']))
      <img class="check" src="./images/checked_checkbox.png">
      @else
      <img class="check" src="./images/nochecked_checkbox.png">
      @endif
      Alterazioni di biomarcatori indicativi di danno miocardico<br>
      @if (array_key_exists('ticagrelor_5',$piano['piano']['pianoterapeutico']))
      <img class="check" src="./images/checked_checkbox.png">
      @else
      <img class="check" src="./images/nochecked_checkbox.png">
      @endif
      Uno dei seguenti fattori di rischio (età &#62; 60 anni, pregresso infarto miocardico, pregressa
     rivascolarizzazione miocardica, pregresso stroke ischemico, TIA, stenosi carotidea &#62; 50%, diabete mellito,
     arteriopatia periferica, insufficienza renale con clearance della creatinina &#60; 60 ml/min/1.73m<sup>2</sup>
     )<br><br>
     <b>Nei pazienti con STEMI è necessaria la presenza di:</b><br>
     @if (array_key_exists('ticagrelor_6',$piano['piano']['pianoterapeutico']))
     <img class="check" src="./images/checked_checkbox.png">
     @else
     <img class="check" src="./images/nochecked_checkbox.png">
     @endif
      Sopraslivellamento di almeno 0.1 mV in almeno due derivazioni contigue o blocco di branca sinistra di
     recente
     @if (array_key_exists('ticagrelor_7',$piano['piano']['pianoterapeutico']))
     <img class="check" src="./images/checked_checkbox.png">
     @else
     <img class="check" src="./images/nochecked_checkbox.png">
     @endif
     Intenzione di trattare il paziente con angioplastica primaria<br><br>
     <b>La durata massima del trattamento è di 12 mesi</b>
     @include('piani.footer',array('piano'=>$piano['piano']))

     
    <br/>
    <br/>

    @include('firma')


    </body>
</html>
