<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<html>
    <head>
    </head>
    <style>
        table{
            border:0px;
            font-size:12px;
        }
        tr{
            border:0px !important;
            outline:0px !important;
        }
        .logo-pdf{
            width:220px !important;
            padding-top:10px;
            padding-right: 50px;
        }
        .testo-bold{
            font-weight: 800;
        }
        .page-break {
        page-break-after: always;
    }
    .colonna_cliente{
      width:250px;
    }
    .testo-ordine{
        font-size:14px;
    }
    .table_cliente{
      margin-top:30px;
    }
    p,h6,h4{
      padding:0px;
      margin:0px;
    }
    .check{
      width:12px;
    }
      .fa {
    display: inline;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 1;
    font-family: FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

    </style>
    <body>

      @include('piani.header', array('intestazione'=>$piano['intestazione'],'codicePiano'=>$piano['codicePianoTerapeutico']))

      <p>
        Rivaroxaban, somministrato insieme con acido acetilsalicilico (ASA), è indicato per la prevenzione di eventi aterotrombotici in pazienti 
        adulti, ad alto rischio di eventi ischemici, che presentano coronaropatia (coronary artery disease, CAD) o arteriopatia periferica (peripheral 
        artery disease, PAD) sintomatica
      </p>

      <br/>

      <p>
        La rimborsabilità a carico del S.S.N., in regime di dispensazione A/RRL (cardiologo, angiologo, chirurgo vascolare), da parte dei centri ospedalieri o di specialisti individuati dalle Regioni, è limitata (nel rispetto della scheda tecnica del farmaco) ai pazienti adulti ad alto rischio di eventi ischemici, <span style="text-underline"><u><b>in aggiunta ad acido acetilsalicilico (ASA)</b></u></span>, che soddisfino la seguente condizione clinica:
      </p> 

      <br/>

      <p>

        @if(array_key_exists('arteriopatia',$piano['piano']['pianoterapeutico']))
        
            @if($piano['piano']['pianoterapeutico']['arteriopatia'] == 'on')
                <input type="checkbox" checked>
            @endif

            @else
            <input type="checkbox">
        @endif
        
        
        <label>Paziente con diagnosi di arteriopatia periferica sintomatica (dell’arto inferiore)<sup>§</sup> che non necessiti di doppia terapia antiaggregante o di terapia anticoagulante (a dose piena) o altra terapia antiaggregante diversa dall’ASA e per il quale la singola terapia con acido acetilsalicilico rappresenti lo standard di cura.</label>
      </p> 

      <br/>

      <table style="width:100%">
        <tr>
          <th> </th>
          <th>Posologia</th>
        </tr>
        <tr>
          <th>Rivaroxaban 2,5</th>
          <th>2,5mg x 2</th>
        </tr>
      </table>

      <br/> 

      <p>
        <sup>§</sup> Per arteriopatia periferica (PAD) (arti inferiori) sintomatica si intende: precedente intervento chirurgico di bypass
         aorto-femorale, intervento chirurgico di bypass dell'arto inferiore o intervento di rivascolarizzazione mediante PCA
         dell’arteria iliaca o delle arterie infra-inguinali, o pregressa amputazione dell'arto o del piede per malattia vascolare2
         arteriosa, o diagnosi clinica di claudicatio intermittens associata ad una o più delle seguenti condizioni: I) rapporto 
         pressione sanguinea caviglia/braccio &lt;	0,90, II) stenosi arteriosa periferica > 50% documentata con angiografia o con un 
         eco doppler arterioso o III) stenosi carotidea rivascolarizzata, o stenosi carotidea asintomatica >50% diagnosticata con 
         angiografia o eco doppler. 
       </p>

       <br/>

       <p>
         N.B. Con riferimento alle altre indicazioni autorizzate, l’utilizzo di rivaroxaban 2,5 mg non è rimborsato dal 
         SSN per le seguenti indicazioni:
       </p>
       <ul style="list-style: disc">
         <li>
           Rivaroxaban, somministrato insieme con il solo acido acetilsalicilico (acetylsalicylic acid, ASA) o con 
           ASA e clopidogrel o ticlopidina, è indicato per la prevenzione di eventi aterotrombotici in pazienti 
           adulti dopo una sindrome coronarica acuta (SCA) con biomarcatori cardiaci elevati.
         </li>
         <li>
           Rivaroxaban, somministrato insieme con acido acetilsalicilico (ASA), è indicato per la prevenzione di 
           eventi aterotrombotici in pazienti adulti, ad alto rischio di eventi ischemici, che presentano 
           coronaropatia (coronary artery disease, CAD).
         </li>
       </ul>
      
   
      @include('piani.footer',array('piano'=>$piano['piano']))

      <br/>
      <br/>

      @include('firma')

      
    </body>
</html>
