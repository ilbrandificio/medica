<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<html>
    <head>
    </head>
    <style>
        table{
            border:0px;
            font-size:12px;
        }
        tr{
            border:0px !important;
            outline:0px !important;
        }
        .logo-pdf{
            width:220px !important;
            padding-top:10px;
            padding-right: 50px;
        }
        .testo-bold{
            font-weight: 800;
        }
        .page-break {
        page-break-after: always;
    }
    .colonna_cliente{
      width:250px;
    }
    .testo-ordine{
        font-size:14px;
    }
    .table_cliente{
      margin-top:30px;
    }
    p,h6,h4{
      padding:0px;
      margin:0px;
    }
    .check{
      width:12px;
    }

    .fa {
      display: inline;
      font-style: normal;
      font-variant: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 1;
      font-family: FontAwesome;
      font-size: inherit;
      text-rendering: auto;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    .container{
      border: 1px solid #000;
      padding: 5px;
    }

    </style>
    <body>

      @include('piani.header', array('intestazione'=>$piano['intestazione'],'codicePiano'=>$piano['codicePianoTerapeutico']))
        
      <p><b>Piano Terapeutico per la prescrizione delle specialità medicinali VELTASSA<sup>&reg;</sup> e LOKELMA<sup>&reg;</sup> (sodio zirconio ciclosilicato)</b></p>

      <br>

      <p> 
         <i>La prescrivibilità di questi medicinali è consentita ai soli medici appartenenti a centri ospedalieri o specialisti nefrologo, cardiologo, internista</i>
      </p>

      <br>
      <p><b>Indicazione terapeutica</b>: trattamento dell'iperkaliemia negli adulti</p>

      <br>
      <p>
          <u><i>La  rimborsabilità  è  limitata  al  trattamento  dei  pazienti  adulti  con  Iperkaliemia  persistente  (livello  di
            potassiemia  >5.5mmol/L)  in  pazienti  con  risposta  insufficiente   o  controindicazione  alle   resine   (calcio
            polistirene sulfonato/sodio polistirene sulfonato).</i></u>
      </p>

      <br>


    <div class="container">
      <p><span style="text-transform: uppercase"><b>Criteri di eleggibilità al trattamento</b></span> <span>(devono essere soddisfatti entrambi i punti 1 e 2)</span></p>

      <br/>
      
      <p>
          1) Diagnosi: Iperkaliemia persistente (livello di potassiemia > 5.5mmol/L) in paziente con risposta
          insufficiente o controindicazione alle resine (calcio polistirene sulfonato/sodio polistirene sulfonato).
      </p>

      <br/>

      <p> <b> 2) Almeno una delle sugenti condizioni</b> (possibilità di scelta multipla):</p>

      
      <p><input id="stadio_3b" name="stadio_3b"  type="checkbox" <?= array_key_exists('stadio_3b', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?> /> Insufficienza renale: stadio 3b-CKD in pazienti <b>con</b> concomitante terapia con  RAASi</p>
   

      <p>
          <input id="stadio_45" name="stadio_45"  type="checkbox" <?= array_key_exists('stadio_45', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>/> Insufficienza renale: stadio 4 o 5 CKD <b>non in dialisi</b>, in pazienti <b>con o senza</b> concomitante terapia con RAASi
      </p>

      <p>
          <input id="stadio_5" name="stadio_5" type="checkbox" <?= array_key_exists('stadio_5', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>/> Insufficienza renale: stadio 5-CKD <b>in dialisi</b> (solo per sodio zirconio ciclosilicato)
      </p>

      <p>
          <input id="scompenso_cardiaco" name="scompenso_cardiaco" type="checkbox" <?= array_key_exists('scompenso_cardiaco', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?> /> Scompenso caridaco (frazione di eiezione <span><u>&lt;</u></span> 40%) in pazienti <b>con</b> concomitante terapia con RAASi in dose giudicata subottimale
      </p>
    </div>

      <br>
      
    <div class="container">
      <h4>FARMACO PRESCRITTO</h4>

      <p><input type="checkbox" name="veltassa" id="veltassa" <?= array_key_exists('veltassa', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <b>VELTASSA</b> (patiromer)

     
      <p><b>Pazienti NON in dialisi* </b>
        <input type="checkbox" name="veltassa_84" id="veltassa_84"  <?= array_key_exists('veltassa_84', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <label for="veltassa_84">8,4 g</label> 
        &nbsp; &nbsp;
        <input type="checkbox" name="veltassa_168" id="veltassa_168" <?= array_key_exists('veltassa_168', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <label for="veltassa_168">16,8 g</label> 
      <p>
       
      <p> *(nei pazienti in dialisi l'uso di veltassa non è rimborsato)</p>

      <br/>
      
      <p>
        Posologia di correzione(1): <u>{{$piano['piano']['pianoterapeutico']['vel_pos_cor']}}</u>
      </p>
      
      <p>Posologia di mantenimento(1): <u>{{$piano['piano']['pianoterapeutico']['vel_pos_mant']}}</u></p>

      
      <br/>

      <p><input type="checkbox" name="lokelma" id="lokelma" <?= array_key_exists('lokelma', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <label for="lokelma"><b>LOKELMA</b> (sodio zirconio ciclosilicato)</label></p>
      
      <p><b> 1. Pazienti NON in dialisi: </b> &nbsp; &nbsp;
      <input type="checkbox" name="lokelma_5" id="lokelma_5" <?= array_key_exists('lokelma_5', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <label for="lokelma_5">5 g</label> 
      &nbsp; &nbsp;
      <input type="checkbox" name="lokelma_10" id="lokelma_10" <?= array_key_exists('lokelma_10', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <label for="lokelma_10">10 g</label> 
      </p> 

      <p><b> 2. Pazienti in dialisi: </b> &nbsp; &nbsp;
        <input type="checkbox" name="lokelma_5_dial" id="lokelma_5_dial" <?= array_key_exists('lokelma_5_dial', $piano['piano']['pianoterapeutico']) ? 'checked' : '' ?>> <label for="lokelma_5_dial">5 g (trattamento nei giorni di non-dialisi)</label> 
        &nbsp; &nbsp;
      </p>
      
      <br/> 

      <p>Posologia di correzione(1) <u>{{$piano['piano']['pianoterapeutico']['lokelma_pos_cor']}}</u></p>
      <p>Posologia di mantenimento(1) <u>{{$piano['piano']['pianoterapeutico']['lokelma_pos_mant']}}</u></p>
    </div>

    @include('piani.footer',array('piano'=>$piano['piano']))

    <br/>
    <br/>

    @include('firma')

    </body>
</html>
