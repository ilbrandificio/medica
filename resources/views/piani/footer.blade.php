<style>

  .table_dose{
    font-size: 16px !important;
  }
    table{
        border: 1px solid #000;
        border-collapse: collapse;
    }

    tr,td,th{
        border: 1px solid #000;
        padding: 2px;
    }

    ul{
        list-style-type: none;
        margin:0;
    }

    li{
        vertical-align: middle;
    }

    input[type="checkbox"]{
        vertical-align: middle;
    }

    label{
        vertical-align: middle;
    }


</style>
<div class="sezione_dose">
    <h2 style="text-align:center">Dose e Durata del trattamento</h2>
    <table class="table_dose" style="width: 100%">
        <tr>
          <td>Dose</td>
          <td>{{$piano['dose']}}</td>
        </tr>
        <tr>
          <td>Validità Scheda di Montoriaggio</td>
          <td>{{$piano['validi']}}</td>
        </tr>
        <tr>
          <td>Numero confezioni di validità</td>
          <td>{{$piano['confe']}}</td>
      </tr>
      @if(array_key_exists('indi',$piano) )
        <tr>
          <td>Indicare Se: </td>
            <td>
              <ul>
                <li>
                  <input type="checkbox" <?= $piano['indi']=='prima' ? 'checked' : '' ?>><label> Prima Prescrizione</label>
                </li>
                <li>
                  <input type="checkbox" <?= $piano['indi']=='prosecuzione' ? 'checked' : '' ?> ><label> Prosecuzione della cura</label>
                </li>
              </ul>
            </td>
        </tr>
      @endif
      @if($piano['switch_check']==true)
        <tr>
          <td>
              <label> Switch Terapia</label>
          </td>
          <td>
            <ul>
                <li>
                    <input type="checkbox" <?= $piano['switch']=='reazione' ? 'checked' : '' ?>> <label> Reazione Avversa</label>
                </li>
                <li>
                    <input type="checkbox" <?= $piano['switch']=='mancata' ? 'checked' : '' ?>> <label> Mancata Efficacia</label>
                </li>
            </ul>
          </td>
        </tr>
        @endif
    </table>
    @if($piano['switch_check']==true)
    <br><br>
    <label> <b>Terapia precedente</b></label>
    <table class="table_dose" style="width: 100%">
      <tr>
        <th>
          Farmaco
        </th>
        <th>
          Durata
        </th>
      </tr>
      <tr>
        <td>
          <?= $piano['farmaco_1'] ?>
        </td>
        <td>
          <?= $piano['durata_1'] ?>
        </td>
      </tr>
      <tr>
        <td>
          <?= $piano['farmaco_2'] ?>
        </td>
        <td>
          <?= $piano['durata_2'] ?>
        </td>
      </tr>
      <tr>
        <td>
          <?= $piano['farmaco_3'] ?>
        </td>
        <td>
          <?= $piano['durata_3'] ?>
        </td>
      </tr>
    </table>
    @endif
</div>
