<?php
$link = $_ENV['APP_URL'];
$current_date = date('Y-m-d');
$subtractday = strtotime('- 1 days', strtotime($current_date));
$final_date = date('d/m/Y',$subtractday);

?>

@include('header')



<p>
Gentile <b>{{$anticoagulante['name']}}</b>, <br/>
Anticoagulante: <b>{{str_replace('_'," ",strtoupper($anticoagulante['anticoagulante']))}}</b> <br/>
inr con data <b>{{$final_date}}</b> non registrato
</p>

<br/>

<a href={{$link.'login'}} class="button" target="_blank" >Clicca qui per accedere</a>
