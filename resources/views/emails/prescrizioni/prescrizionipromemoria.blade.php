<?php
$link = $_ENV['APP_URL']
?>

@include('header')

<p>
  Gentile <b>{{$prescrizione['name']}}</b>, <br/>
  la prescrizione è in scadenza in data: <b>{{date('d/m/Y',strtotime($prescrizione['data_scadenza']))}}</b>.
</p>

<br/>

<table class="table-esami">
  <tr>
    <th>
      Esame
    </th>
    <th>
      Stato
    </th>
  </tr>

  @if($prescrizione['esami']['esami_laboratorio'])
    <tr>
      <th colspan="2">Esami Laboratorio</th>
    </tr>
    @foreach($prescrizione['esami']['esami_laboratorio'] as $key=>$esame)
    <tr>
      <td>
        <div>
          {{str_replace('_'," ",strtoupper($key))}}
        </div>
      </td>
      <td>
        {{strtoupper($esame['stato'])}}
      </td>
    </tr>
    @endforeach
  @endif

  @if($prescrizione['esami']['esami_strumentali'])
    <tr>
      <th colspan="2">Esami Strumentali</th>
    </tr>
    @foreach($prescrizione['esami']['esami_strumentali'] as $key=>$esame)
      <tr>
        <td>
          <div>
            {{str_replace('_'," ",strtoupper($key))}}
          </div>
        </td>
        <td>
          {{strtoupper($esame['stato'])}}
        </td>
      </tr>
    @endforeach
  @endif
</table>

<br/>
 
<a href={{$link.'/login'}} class="button" target="_blank" >Clicca qui per accedere</a>

