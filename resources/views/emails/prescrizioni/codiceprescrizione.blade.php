<?php
$link = $_ENV['APP_URL']
?>

@include('header')

<p>
  Gentile <b>{{$prescrizione['name']}}</b>, <br/>
  ecco il codice per sbloccare la prescrizione.
</p>

<p>
  Codice: {{$prescrizione['codice']}}
</p>


<br/>

<a href={{$link.'/login'}} class="button" target="_blank" >Clicca qui per accedere</a>
