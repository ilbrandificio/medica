<?php 
    $link = $_ENV['APP_URL']
?>

@include('header')

<p>
    Gentile <b>{{$esame['name']}}</b>, <br/>
    l'esame <b>{{str_replace('_'," ",strtoupper($esame['codiceEsame']))}}</b>
    è scaduto in data: <b>{{date('d/m/Y',strtotime($esame['data']))}}</b>.
</p>

<br/>

<a href={{$link.'/login'}} class="button" target="_blank" >Clicca qui per accedere</a>
