<?php 

    $numero_farmaci = count($data['0']['farmaci']);
?>

<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<html>
    <head>
    </head>
    <style>

        table{
            margin-top: 30px;
            font-size:16px;
            width:100%;
            border-collapse: collapse;
        }
        table,td,th{
             border: 1px solid #000;  
             padding: 5px
        }
        .logo-pdf{
            width:220px !important;
            padding-top:10px;
            padding-right: 50px;
        }
        .testo-bold{
            font-weight: 800;
        }
        .page-break {
        page-break-after: always;
    }
    </style>
  
    <body>

     
        @foreach($data['0']['farmaci'] as $key=>$farmaco)
        
        <div>
            <p>
                <?php echo nl2br($data['0']['medico']['intestazione_piano'])?>
            </p>
        </div>

        
        <div> Data Prescrizione : <span>{{date('d/m/Y',strtotime($data['0']['created_at']))}}</span></div>
        <!-- START INTESTAZIONE MEDICO -->
        <table>
            <tr>
                <th>Medico</th>
                <th>Telefono</th>
                <th>Codice ODM</th>
                <th>Provincia ODM</th>
                <th>Numero Enpam</th>
            </tr>
            <tr>
                <td>
                    {{$data['0']['medico']['nome']}} {{$data['0']['medico']['cognome']}}
                </td>
                <td>
                    {{$data['0']['medico']['telefono']}} 
                </td>
                <td>
                    {{$data['0']['medico']['codice_odm']}} 
                </td>
                <td>
                    {{$data['0']['medico']['provincia_odm']}} 
                </td>
                <td>
                    {{$data['0']['medico']['numero_enpam']}} 
                </td>
            </tr>
        </table>

         <!-- START INTESTAZIONE PAZIENTE -->
         <table>
            <tr>
                <th>Paziente</th>
                <th>Data Nascita</th>
                <th>Provincia Nascita</th>
                <th>Provincia ASL</th>
                <th>Codice Fiscale</th>
            </tr>
            <tr>
                <td>
                    {{$data['0']['paziente']['nome']}} {{$data['0']['paziente']['cognome']}}
                </td>
                <td>
                    {{$data['0']['paziente']['data_nascita']}} 
                </td>
                <td>
                    {{$data['0']['paziente']['provincia_nascita']}} 
                </td>
                <td>
                    {{$data['0']['paziente']['provincia_asl']}} 
                </td>
                <td>
                    {{$data['0']['paziente']['codice_fiscale']}} 
                </td>
            </tr>
        </table>


        <!-- START TABLE FARMACO -->
        <table>
            <tr>
                <th>
                    Farmaco
                </th>
                <th>
                    Dosaggio
                </th>
                <th>
                    Numero e Orario Somministrazione
                </th>
                <th>
                    Somministrazione
                </th>
                <th>
                    SSN
                </th>
            </tr>
            <tr>
                <td>
                     {{$farmaco['farmaco']}}
                </td>
                <td>
                    {{$farmaco['dosaggio']}}
               </td>
               <td>
                    {{$farmaco['numero_orario_somministrazione']}}
               </td>
               <td>
                    {{$farmaco['somministrazione']}}
                </td>
                <td>
                    {{$farmaco['ssn'] == 0 ? 'No' : 'Sì'}}
                </td>
            </tr>
        </table>
        
            <br/>

            @include('firma')

            @if($key != $numero_farmaci - 1)
                <div class="page-break"></div> 
            @endif
                
          

        @endforeach
    </body>
</html>
