
<?php 
    $data_inizio = date('d/m/Y',strtotime($data['data_inizio']));
    $data_fine = date('d/m/Y',strtotime($data['data_fine']));
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<style>
    h2{
        margin:0
    }
    
    .wrapper_tables{
        width: 100%;
    }   

    .table-anticoagulante{
        font-size: 14px;
        width: 50% !important;
    }


</style>

<body>

    @include('header_stampe')

    <h3>Periodo: {{$data_inizio}} - {{$data_fine}} <br/>
    Anticoagulante: {{str_replace('_',' ',$data['anticoagulante'])}}
    </h3>
    
    <div class="wrapper_tables">

    @foreach($data['registrazioni'] as $registrazione)
        <table class="table-anticoagulante">
            <tr>
                <th style="width:40%">Data</th>
                <th style="width:20%">INR</th>
                <th style="width:40%">Dose</th>
            </tr>
            @foreach($registrazione as $registrazione_mese)
             <tr>
                 <td>{{date('d/m/Y',strtotime($registrazione_mese['data']))}}</td>
                 <td>
                     {{$registrazione_mese['inr']}}
                 </td>
                 <td>
                    {{$registrazione_mese['dose']}}
                 </td>
             </tr>
            @endforeach
        </table>
    @endforeach
    </div>
    
</body>
</html>


