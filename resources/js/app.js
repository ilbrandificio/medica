import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import Variables from '~/components/App'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from '../icons/icons.js'
import VueFilterDateFormat from '@vuejs-community/vue-filter-date-format';
import VueDateFns from "vue-date-fns";
import Echo from 'laravel-echo';

import '~/plugins'
import '~/components'

Vue.config.productionTip = false
Vue.use(CoreuiVue)
Vue.use(VueFilterDateFormat);
Vue.use(VueDateFns);

window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    forceTLS: false
});

Vue.filter('check', function (value) {
  if (value) {
    return '<i class="fas fa-check"></i>'
  } else {
    return '<i class="fas fa-times"></i>'
  }
})

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  icons,
  Variables,
  ...App
})
