import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faUserMd, faHome,faComments,faUsers,faUserPlus, faArrowLeft,faUserCheck,faSignInAlt,
  faCalendarAlt,faBell,faFileMedical, faFileAlt,faPills,faLaptopMedical,faFileInvoice,faStethoscope, faHeartbeat, faHeadset, faEdit,
  faTrash, faPrint, faSearch,faFilter, faPlus, faChartArea, faPaperPlane , faMobileAlt,faFileMedicalAlt, faRss, faCheckDouble, faUnlock,
  faVideo,
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub, faWhatsapp

} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faGithub, faUserMd,faHome,faComments,faUsers,faUserPlus,faArrowLeft,faUserCheck,faSignInAlt,
  faCalendarAlt,faBell,faFileMedical,faFileAlt,faPills,faLaptopMedical,faFileInvoice,faStethoscope,faHeartbeat, faHeadset, faEdit, faTrash,
  faPrint, faSearch, faVideo, faFilter, faPlus , faChartArea, faPaperPlane , faMobileAlt, faFileMedicalAlt, faWhatsapp, faRss , faCheckDouble ,faUnlock
)

Vue.component('fa', FontAwesomeIcon)
