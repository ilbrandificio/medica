import Vue from 'vue'
var ambiente = process.env.NODE_ENV
if (ambiente == 'development') {
  var url = 'http://app.medica.com/'
  var url_empy='';
} else {
  var url = 'https://medicoremoto.it/'
  var url_empy='';
}

Vue.prototype.$resource_url = url
Vue.prototype.$resource_url_empty = url_empy
