import axios from "axios"

/*
  STATE
*/
export const state = {
  paziente:{
    idUser:'',
    name: '',
    url_media: '',
  }
}
/*
  GETTERS
*/
export const getters = {
  getPaziente: () => {
    return state.paziente
  },
}

/*
  MUTATION
*/
export const mutations = {
  setPaziente: (state, paziente) => {
    state.paziente.idUser = paziente.idPaziente
    state.paziente.name = paziente.name;
    state.paziente.url_media = paziente.url_media
  },
}
/*
  ACTIONS
*/
export const actions = {
  selezionaPaziente: (context, idPaziente) => {
      var url = '/api/seleziona-paziente/'+idPaziente;
      axios.get(url)
      .then(res =>{
        if(res.data){
          context.commit('setPaziente',res.data[0]);
          return true;
        }
        else{
          return [];
        }
      })
      .catch(err => {
        context.commit('setPreload',false)
      })
    }

}
