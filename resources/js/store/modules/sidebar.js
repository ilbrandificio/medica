 import Vue from 'vue'
 import Vuex from 'vuex'
 Vue.use(Vuex)

export const state = {
  sidebarShow: false,
  sidebarMinimize: false
}
// getters
export const getters = {
  statoSidebar: () => {
    return state.sidebarShow
  }
}

export const mutations = {
  toggleSidebarDesktop () {

  },
  toggleSidebarMobile (state) {
    state.sidebarShow = !state.sidebarShow
  },
  hideSidebarMobile (state) {
    state.sidebarShow = false
  },
  set (state, [variable, value]) {
    state[variable] = value
  }
}

export default new Vuex.Store({
  state,
  mutations
})
