import axios from "axios"

// state
export const state = {
  preload:false,
  errore:false,
  messaggio_errore:'',
  successo:false,
  messaggio_successo:'',
  idPaziente: '',
  messaggi: 0,
  allarmi_attivi: 0,
  richieste_pazienti: 0,
  profilo:{},
}

// getters
export const getters = {
  getPreloader: () => {
    return state.preload
  },
  getErrore: () => {
    return state.errore
  },
  getMessaggioErrore: () => {
    return state.messaggio_errore
  },
  getSuccesso: () => {
    return state.successo
  },
  getProfilo: () => {
    return state.profilo
  },
  getMessaggioSuccesso: () => {
    return state.messaggio_successo
  },
  getCounterMessaggi: () => {
    return state.messaggi
  },
  getCounterAllarmiAttivi: () => {
    return state.allarmi_attivi
  },
  getCounterRichiestePazienti: () => {
    return state.richieste_pazienti
  }
}

// mutation
export const mutations = {
  setPreload: (state, stato) => {
    state.preload = stato
  },
  setError: (state, stato) => {
    state.error = stato
  },
  setMessaggioErrore: (state, messaggio) => {
    state.messaggio_errore = messaggio
    state.errore = true
  },
  hideErrore: (state) => {
    state.errore = false
  },
  setSuccesso: (state, stato) => {
    state.successo = stato
  },
  setProfilo: (state, profilo) => {
    state.profilo = profilo
  },
  setMessaggioSuccesso: (state, messaggio) => {
    state.messaggio_successo = messaggio
    state.successo = true
  },
  hideSuccesso: (state) => {
    state.successo = false
  },
  setCounterAllarmiAttivi: (state,counter) => {
    state.allarmi_attivi =  counter
  },
  setCounterRichiestePazienti: (state,counter) => {
    state.richieste_pazienti = counter
  },
  reduceCounterRichiestePazienti: (state) => {
    if(state.richieste_pazienti > 0){
      state.richieste_pazienti = state.richieste_pazienti - 1 
    }
  },
  reduceCounterAllarmiAttivi: (state) => {
    if(state.allarmi_attivi>0){
      state.allarmi_attivi=state.allarmi_attivi-1
    }
  },
  setCounterMessaggi: (state,counter) => {
    state.messaggi = counter
  },
  reduceCounterMessaggi: (state) => {
    if(state.messaggi>0){
      state.messaggi=state.messaggi-1
    }
  },
 }
/*
  CARICA PROFILO
*/
export const actions = {

  caricaProfilo: (context, state) => {
    var url = '/api/profilo';
    context.commit('setPreload',true)
    axios.get(url)
      .then(res => {
        context.commit('setPreload',false)
      if(res.data){
          context.commit('setProfilo',res.data[0]);
        }
      else {
        context.commit('setMessaggioErrore', res.data.message);
         this.$store.commit('private/setMessaggioErrore', res.data.message);
        }
      })
      .catch(err => {
        context.commit('setPreload',false);
        context.commit('setMessaggioErrore', 'Errore Inserimento');
      })
  },

/*
  AGGIORNA PROFILO
*/
  aggiornaProfilo: (context, state) => {
    var url = '/api/profilo';
    const formData = new FormData(document.getElementById("formMedico"));
    let data = {};
    for (let [key, val] of formData.entries()) {
      Object.assign(data, { [key]: val })
    }
    var payload = {}
    payload.meta = data

    context.commit('setPreload',true)
    axios.put(url,payload)
    .then(res => {
      context.commit('setPreload',false)
      if(res.data.success === true){
        context.commit('setMessaggioSuccesso','Aggiornamento Riuscito')
        }
      else {
        context.commit('setMessaggioErrore', res.data.message)
      }
    })
    .catch(err => {
        context.commit('setPreload',false)
        context.commit('setMessaggioErrore',  'Errore Inserimento');
    })
  },

  aggiornaProfiloPaziente: (context, state) => {
    var url = '/api/profilo';
    const formData = new FormData(document.getElementById("formPaziente"));
    let data = {};

    for (let [key, val] of formData.entries()) {
      Object.assign(data, { [key]: val })
    }

    console.log(data);

    var payload = {}
    payload.meta = data

    context.commit('setPreload',true)
    axios.put(url,payload)
    .then(res => {
      context.commit('setPreload',false)
      if(res.data.success === true){
        context.commit('setMessaggioSuccesso','Aggiornamento Riuscito')
        }
      else {
        context.commit('setMessaggioErrore', res.data.message)
      }
    })
    .catch(err => {
        context.commit('setPreload',false)
        context.commit('setMessaggioErrore',  'Errore Inserimento');
    })
  },


  aggiornaProfiloInfermiere: (context, state) => {
    var url = '/api/profilo';
    const formData = new FormData(document.getElementById("formInfermiere"));
    let data = {};
    for (let [key, val] of formData.entries()) {
      Object.assign(data, { [key]: val })
    }
    var payload = {}
    payload.meta = data

    context.commit('setPreload',true)
    axios.put(url,payload)
    .then(res => {
      context.commit('setPreload',false)
      if(res.data.success === true){
        context.commit('setMessaggioSuccesso','Aggiornamento Riuscito')
        }
      else {
        context.commit('setMessaggioErrore', res.data.message)
      }
    })
    .catch(err => {
        context.commit('setPreload',false)
        context.commit('setMessaggioErrore',  'Errore Inserimento');
    })
  },

  countAllarmiAttivi(context){
    var url='api/allarmi-attivi'
    axios.get(url)
      .then(res => {
        if (res.data) {
          context.commit('setCounterAllarmiAttivi', res.data.allarmi_attivi)
        } else {
          context.commit('setMessaggioErrore', res.data.message)
        }
      })
      .catch(err => {
        console.log(err);
        context.commit('setMessaggioErrore', 'Errore Caricamento')
      })
  },

  countMessaggi(context){
    var url='api/messaggi-totali'
    axios.get(url)
      .then(res => {
        if (res.data) {

          context.commit('setCounterMessaggi',res.data.messaggi)

        } else {
          context.commit('setMessaggioErrore', res.data.message)
        }
      })
      .catch(err => {
        console.log(err);
        //this.$store.commit('private/setPreload', false)
        context.commit('setMessaggioErrore', 'Errore Caricamento')
      })
  },

  countRichiestePazienti(context){
    var url='api/dashboard-medico'
    axios.get(url)
      .then(res => {
        if (res.data) {
          context.commit('setCounterRichiestePazienti',res.data.pazienti_in_attesa)
        } else {
          context.commit('setMessaggioErrore', res.data.message)
        }
      })
      .catch(err => {
        console.log(err);
        //this.$store.commit('private/setPreload', false)
        context.commit('setMessaggioErrore', 'Errore Caricamento')
      })
  },
}
