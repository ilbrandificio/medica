<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('/user', 'UtentiController@utenteCorrente');

    Route::get('avatar/{idUtente}','UtentiController@getAvatar');

//CARICAMENTO PROFILO
    Route::get('profilo','UtentiController@getProfilo');
    Route::put('profilo','UtentiController@aggiornaProfilo');
    Route::delete('profilo','UtentiController@cancellaProfilo');

    //ROUTE PRINCIPIATTIVI
    Route::get('principi','PrincipiController@getPrincipi');

    Route::get('/agora-chat', 'AgoraVideoController@index');
    Route::post('/agora/token', 'AgoraVideoController@token');
    Route::post('/agora/call-user', 'AgoraVideoController@callUser');
});


//ACCESSO ADMIN
Route::group(['middleware' => ['auth:api','auth.role:admin']], function () {

//ROUTE UTENTI
    Route::get('utenti','UtentiController@getUtenti');
    Route::get('utente/{id}','UtentiController@getUtente');
    Route::put('utente-aggiorna/{id}','UtentiController@aggiornaUtenteAdmin');
    Route::put('aggiorna-password-utente/{id}','UtentiController@aggiornaPasswordRoleAdmin');
    Route::post('utente-store','UtentiController@storeForAdmin');
    Route::delete('utente/{id}','UtentiController@cancellaUtente');


    Route::put('medico/{id}','MediciController@aggiornaMedico');
    Route::post('medico','MediciController@storeMedico');
    Route::delete('medico/{id}','MediciController@cancellaMedico');

//ROUTE REPARTI
    Route::post('reparto','RepartiController@store');
    Route::put('reparto/{idReparto}','RepartiController@aggiorna');
    Route::delete('reparto/{idReparto}','RepartiController@cancellaReparto');

//ROUTE PRONTUARIO
    Route::post('prontuario','ProntuarioController@store');
    Route::put('prontuario/{id_principio}','ProntuarioController@aggiorna');
    Route::get('prontuario','ProntuarioController@getProntuario');
    Route::delete('prontuario/{id_principio}','ProntuarioController@cancella');


//ROUTE PAZIENTI
    Route::get('all-pazienti','UtentiController@getAllPazienti');
    Route::get('all-medici','UtentiController@getAllMedici');
    Route::get('all-infermieri','UtentiController@getAllInfermieri');


    Route::get('pazienti-medico/{idMedico}','PazientiMediciController@getPazientiMedico');
    Route::get('pazienti-infermiere/{idInfermiere}','PazientiInfermieriController@getPazientiInfermiere');

});


//ACCESSO ADMIN  MEDICO
Route::group(['middleware' => ['auth:api','auth.role:admin,medico']], function () {

//ROUTE REPARTI
    Route::get('reparti','RepartiController@getReparti');


//ROUTE PAZIENTI
    Route::get('pazienti','PazientiController@getPazienti');
    Route::get('seleziona-paziente/{idPaziente}','UtentiController@selezionaPaziente');
    Route::post('paziente','PazientiController@storePaziente');
    Route::delete('paziente/{id}','PazientiController@cancellaPaziente');


//ROUTE PAZIENTI MEDICI
    Route::get('richieste','PazientiMediciController@getRichieste');
    Route::get('pazienti-medici','PazientiMediciController@getPazientiMedici');
    Route::put('richiesta/{idRichiesta}','PazientiMediciController@accettaRichiesta');

    //TODO: TEST CON POSTMAN
    Route::delete('paziente-assegnato/{idMedico}/{idPazienteMedico}','PazientiMediciController@cancellaPazienteAssegnato');
    Route::delete('medico-assegnato/{idPaziente}/{idPazienteMedico}','PazientiMediciController@cancellaMedicoAssegnato');


//ROUTE TERAPIA
    Route::post('terapia/{idPaziente}','TerapiaController@store');
    Route::put('terapia/{idPaziente}/{idTerapia}','TerapiaController@aggiorna');
    Route::get('terapia/{idPaziente}/{idTerapia}','TerapiaController@getTerapia');
    Route::delete('terapia/{idPaziente}/{idTerapia}','TerapiaController@cancella');


//ROUTE PIANI TERAPEUTICI
    Route::post('pianoterapeutico/{idPaziente}','PianiTerapeuticiController@store');
    Route::put('pianoterapeutico/{idPaziente}/{idPianoTerapeutico}','PianiTerapeuticiController@aggiorna');
    Route::delete('pianoterapeutico/{idPaziente}/{idPianoTerapeutico}','PianiTerapeuticiController@cancella');


//ROUTE DIAGNOSI
    Route::post('diagnosi/{idPaziente}','DiagnosiController@store');
    Route::get('diagnosi','DiagnosiController@getDiagnosi');
    Route::delete('diagnosi/{idPaziente}','DiagnosiController@cancella');


//ROUTE APPUNTAMENTI
    Route::post('appuntamento/{idPaziente}','AppuntamentiController@store');
    Route::put('appuntamento/{idPaziente}/{idAppuntamento}','AppuntamentiController@aggiorna');
    Route::delete('appuntamento/{idPaziente}/{idAppuntamento}','AppuntamentiController@cancellaAppuntamento');
    Route::get('appuntamenti-medico','AppuntamentiController@getAppuntamentiMedico');


//ROUTE INTERVENTO
    Route::post('intervento/{idPaziente}','InterventiController@store');
    Route::put('interventi/{idPaziente}/{idIntervento}','InterventiController@aggiorna');
    Route::delete('intervento/{idPaziente}/{idIntervento}','InterventiController@cancella');

//ROUTE ALLARMI
    Route::get('allarmi','AllarmiController@getAllarmiAttiviMedico'); //RECUPERO GLI ALLARMI ATTIVATI
    Route::get('allarmi-esame/{idPaziente}','AllarmiController@getAllarmiAttiviByEsame');
    Route::get('allarmi-attivi','AllarmiController@countAllarmiAttivi'); //COUNTER ALLARMI ATTIVI
    Route::put('allarme-letto/{idPaziente}/{idAllarme}', 'AllarmiController@segnaComeLetto'); //
    Route::get('allarmi-paziente/{idPaziente}','AllarmiController@getAllarmi');
    Route::post('allarme/{idPaziente}','AllarmiController@store');
    Route::put('allarmi/{idPaziente}/{idAllarme}','AllarmiController@aggiorna');
    Route::delete('allarme/{idPaziente}/{idAllarme}','AllarmiController@cancella');

    //ROUTE MESSAGGI
    Route::get('messaggi-totali','MessaggiController@countMessaggi');
    Route::put('messaggi-letti/{idRicevente}/{idMittente}','MessaggiController@setMessaggiLetti');


//ROUTE PRESCRIZIONE
    Route::post('prescrizione/{idPaziente}','PrescrizioniController@store');
    Route::put('prescrizione/{idPaziente}/{idPrescrizione}','PrescrizioniController@aggiorna');
    Route::delete('prescrizione/{idPaziente}/{idPrescrizione}','PrescrizioniController@cancella');

//ROUTE PRESCRIZIONI ESAMI
    Route::post('prescrizione-esame/{idPaziente}','PrescrizioniEsamiController@store');
    //Route::put('prescrizione-esame/{idPaziente}/{idPrescrizioneEsame}','PrescrizioniEsamiController@aggiorna');
    Route::delete('prescrizione-esame/{idPaziente}/{idPrescrizioneEsame}','PrescrizioniEsamiController@cancella');


//ROUTE IMPOSTAZIONI
    Route::post('impostazioni','ImpostazioniController@aggiorna');
    Route::get('impostazioni','ImpostazioniController@getImpostazioni');

//ROUTE DOCUMENTI
    Route::post('carica-documento/{idPaziente}/{idAppuntamento}','DocumentiController@store');

//ROUTE API
    Route::post('api-key/{idPaziente}', 'ApiKeyController@store');
    Route::get('api-key/{idPaziente}', 'ApiKeyController@getApiKey');
    Route::delete('api-key/{idPaziente}/{idApiKey}','ApiKeyController@cancella');
});


//ACCESSO MEDICO
Route::group(['middleware' => ['auth:api','auth.role:medico']], function () {
    Route::get('profilo-medico','UtentiController@getProfiloMedico');
    Route::put('profilo-medico','MediciController@aggiorna');
    Route::put('teleconsulto','MediciController@aggiornaTeleconsulto');

    Route::get('dashboard-medico','MediciController@getDashboard');

});


//ACCESSO PAZIENTE
Route::group(['middleware' => ['auth:api','auth.role:paziente']], function () {

    Route::get('profilo-paziente','UtentiController@getProfiloPaziente');
    Route::put('profilo-paziente','PazientiController@aggiorna');

    //ROUTE MEDICI ASS
    Route::get('medici-ass','PazientiMediciController@getMediciPaziente'); // RECUPERO I MEDICI ASSEGENATI

    //ROUTE PROMEMORIA

    //ROUTE PRESCRIZIONE FARMACI 
    Route::post('check-codice','PrescrizioniController@checkCodice');
    Route::get('codice-prescrizione/{idPrescrizione}','NotificationController@sendCodicePrescrizione');

});


//ACCESSO ADMIN,INFERMIERE
Route::group(['middleware' => ['auth:api','auth.role:admin,infermiere']], function () {

    Route::get('richieste-pazienti-infermieri','PazientiInfermieriController@getRichieste');
    Route::put('richieste-pazienti-infermieri/{idRichiesta}','PazientiInfermieriController@accettaRichiesta');


});



//ACCESSO ADMIN , PAZIENTI, MEDICI
Route::group(['middleware' => ['auth:api','auth.role:admin,medico,paziente']], function () {

    //ROUTE TELECONSULTO
    Route::post('teleconsulto','TeleconsultoController@store');
    Route::put('conferma-teleconsulto/{idTeleconsulto}','TeleconsultoController@confermaTeleconsulto');
    Route::get('teleconsulti','TeleconsultoController@getTeleconsulti');
    Route::get('teleconsulto/{idTeleconsulto}','TeleconsultiController@getTeleconsultoByIdTeleconsulto');

    //ROUTE PRESCRIZIONI
    Route::get('prescrizioni/{idPaziente}','PrescrizioniController@getPrescrizioniByIdPaziente');
    Route::get('prescrizione/{idPrescrizione}','PrescrizioniController@getPrescrizioneByIdPrescrizione');
    Route::get('stampa-prescrizione/{idPrescrizione}','PrescrizioniController@stampa');

    //ROUTE PRESCRIZIONE ESAMI
    Route::get('prescrizioni-esami/{idPaziente}','PrescrizioniEsamiController@getPrescrizioniEsamiByIdPaziente');
    Route::get('prescrizione-esami/{idPrescrizioneEsame}','PrescrizioniEsamiController@getPrescrizioneByIdPrescrizioneEsame');
    Route::get('esami-prescrizione-promemoria/{idPrescrizioneEsame}','PrescrizioniEsamiController@getEsamiPromemoriaByIdPrescrizione');
    Route::get('stampa-prescrizione-esami/{idPrescrizioneEsame}','PrescrizioniEsamiController@stampa');

    Route::put('aggiorna-stato-esame/{idPaziente}/{idPrescrizioneEsame}','PrescrizioniEsamiController@aggiornaStatoEsame');

    //Route::get('esami-promemoria/{idPaziente}','EsamiController@getEsamiPromemoria');
    Route::get('prescrizioni-esami-promemoria/{idPaziente}','PrescrizioniEsamiController@getPrescrizioniEsamiPromemoria');

    //ROUTE ANTICOAGULANTE
    //Route::post('scheda-anticoagulante/{idAnticoagulante}','SchedaAnticoagulanteController@store');
    //Route::delete('scheda-anticoagulante/{idSchedaAnticoagulante}','SchedaAnticoagulanteController@cancella');

    Route::put('scheda-anticoagulante/{idAnticoagulante}','AnticoagulanteController@aggiornaSchedaAnticoagulante');
    Route::get('scheda-anticoagulante/{idAnticoagulante}','AnticoagulanteController@getAnticoagulanteById');
    Route::get('stampa-scheda-anticoagulante','AnticoagulanteController@stampa');


    //ROUTE REGISTRAZIONE DATI CLINICI
    Route::post('dati-clinici/{idPaziente}','DatiCliniciController@store');
    Route::put('dati-clinici/{idDatiClinici}','DatiCliniciController@aggiorna');
    Route::get('dati-clinici','DatiCliniciController@getDatiClinici');
    Route::delete('dati-clinici/{idPaziente}/{idDatiClinici}','DatiCliniciController@cancella');


//ROUTE DOCUMENTI
    Route::get('documenti/{idPaziente}/{idEntita}','DocumentiController@getDocumentiByIdEntita');

});




//ACCESSO  ADMIN, PAZIENTI, MEDICI, INFERMIERI
Route::group(['middleware' => ['auth:api','auth.role:admin,medico,infermiere,paziente']], function () {

  //ROUTE PROFILO GENERIC
  Route::get('profilo-corrente','UtentiController@getProfiloGeneric');


//ROUTE AGGIORNA PASSWORD
    Route::put('aggiorna-password','UtentiController@aggiornaPassword');

//ROUTE MEDIA
    Route::post('avatar','UtentiController@aggiornaAvatar');
//ROUTE GESTIONE MEDICI PAZIENTI
    Route::get('medici','UtentiController@getMedici');
    Route::get('medico/{idMedico}','UtentiController@getMedico');
    Route::post('paziente-medico','PazientiMediciController@assegnaMedico');
    Route::get('paziente-medici','PazientiMediciController@getPazienteMedici');


//ROUTE INFERMIERI
    Route::get('infermieri','UtentiController@getInfermieri');
    Route::post('paziente-infermiere','PazientiController@assegnaInfermiere');


//ROUTE PAZIENTI
    Route::get('paziente/{idPaziente}','UtentiController@getPaziente');
    Route::put('paziente/{idPaziente}','UtentiController@aggiornaSchedaClinica');
    Route::get('paziente-infermieri/{idPaziente}','PazientiController@getPazienteInfermieri');


//ROUTE DIAGNOSI
    Route::get('diagnosi-paziente/{idPaziente}','DiagnosiController@getDiagnosiPaziente');


//ROUTE TERAPIA
    Route::get('terapia-paziente/{idPaziente}','TerapiaController@getTerapiaPaziente');

//ROUTE ESAMI
    Route::get('esami-paziente/{idPaziente}','EsamiController@getEsamiPaziente');
    Route::get('esami-paziente-grafico/{idPaziente}/{codiceEsame}','EsamiController@getEsamiPazienteGrafico');
    Route::get('esame/{idPaziente}/{idEsame}','EsamiController@getEsamePaziente');
    Route::post('esame/{idPaziente}','EsamiController@store');
    Route::put('esame/{idPaziente}/{idEsame}','EsamiController@aggiorna');
    Route::delete('esame/{idPaziente}/{idEsame}','EsamiController@cancella');

//ROUTE ANTICOAGULANTE
    Route::post('anticoagulante/{idPaziente}','AnticoagulanteController@store');
    Route::put('anticoagulante/{idPaziente}/{idAnticoagulante}','AnticoagulanteController@aggiorna');
    Route::get('anticoagulanti/{idPaziente}','AnticoagulanteController@getAnticoagulanti');
    Route::delete('anticoagulante/{idPaziente}/{idAnticoagulante}','AnticoagulanteController@cancella');


//ROUTE INTERVENTI
    Route::get('interventi/{idPaziente}','InterventiController@getInterventi');


//ROUTE PIANI TERAPEUTICI
    Route::get('pianiterapeutici/{idPaziente}','PianiTerapeuticiController@getPianiTerapeutici');
    Route::get('pianoterapeutico/{idPaziente}/{idPianoTerapeutico}','PianiTerapeuticiController@getPianoById');
    Route::get('stampa-piano/{idPianoTerapeutico}','PianiTerapeuticiController@stampaPiano');


//ROUTE APPUNTAMENTI
    Route::get('appuntamento/{idAppuntamento}','AppuntamentiController@getAppuntamenti');
    Route::get('appuntamenti','AppuntamentiController@getAppuntamenti');
    Route::get('appuntamenti/{idPaziente}','AppuntamentiController@getAppuntamentiPaziente');

//ROUTE MESSAGGI
    Route::post('messaggio','MessaggiController@store');
    Route::get('messaggi','MessaggiController@getChat');
    Route::get('message-notification','MessaggiController@NotificationMessage');


//ROUTE DOCUMENTO
    Route::get('apri-documento/{filename}', 'DocumentiController@apriDocumento');
});


Route::group(['middleware' => ['web']], function () {

});

//TELAMATICO
Route::group(['middleware' => 'apikey'], function () {
    Route::post('monitoraggio', 'MonitoraggioController@store');
});

//PUBLIC API
   Route::post('registrazione-medico', 'Auth\RegistrazioneController@storeMedico');
   Route::post('registrazione-paziente', 'Auth\RegistrazioneController@storePaziente');
   Route::post('registrazione-infermiere', 'Auth\RegistrazioneController@storeInfermiere');

   Route::post('invio-email-verifica', 'EmailHelperController@invioVerificaEmail');
   Route::get('verifica-email', 'RichiesteController@verificaEmail');
   Route::post('login', 'Auth\LoginController@login');
   Route::post('registrazione', 'UtentiController@store');

//ROUTE PASSWORD RESET
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');

    //ROUTE NOTIFICATION
    //Route::get('notifica-esami-promemoria','NotificationController@sendEsamiPromemoriaNot');
    //Route::get('notifica-esami-scaduti','NotificationController@sendEsamiScadutiNot');
    Route::get('notifica-prescrizioni-scadute','NotificationController@sendPrescrizioniEsamiScaduti');
    Route::get('notifica-prescrizioni-promemoria','NotificationController@sendPrescrizioniEsamiPromemoria');
    Route::get('reminder-inr','NotificationController@sendReminderInr');
