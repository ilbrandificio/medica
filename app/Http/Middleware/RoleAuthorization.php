<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\Models\PazientiMedici;
use App\Models\PazientiInfermieri;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
class RoleAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {

        try {

            //Access token from the request
            $token = JWTAuth::parseToken();
            //Try authenticating user
            $user = $token->authenticate();
            $userId = $user->id;
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired
            //return route('login');
            $token = JWTAuth::refresh($token);
            JWTAuth::setToken($token);
            //return $this->unauthorized('Your token has expired. Please, login again.');
        } catch (TokenInvalidException $e) {
            //Thrown if token invalid
            return route('login');
            //return $this->unauthorized('Your token is invalid. Please, login again.');
        }catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return route('login');
           // return $this->unauthorized('Please, attach a Bearer Token to your request');
        }

        //If user was authenticated successfully and user is in one of the acceptable roles, send to next request.
        if ($user && in_array($user->role, $roles)) {

            if($user->role!='admin'){
                //SE LA CHIAMATA CONTIENE UN ID STRUTTURA
                if($user->role=='medico'){
                        if($request->idPaziente){
                            //CHECK SE IL PAZIENTE è ASSEGNATO AL MEDICO
                            if(PazientiMedici::checkPazientiMedici($request)){
                                return $next($request);
                                }
                                else{
                                    return $this->unauthorized();
                                }
                            }
                        else{
                            return $next($request);
                        }
                    }
            //CONTROLLO CHE L'INFERMIERE ABBIA ACCESSO AI PROPRI PAZIENTI
                else if($user->role== 'infermiere'){
                    if($request->idPaziente){
                        if(PazientiInfermieri::checkPazientiInfermieri($request)){
                            return $next($request);
                        }
                        else{
                            return $this->unauthorized();
                        }
                    }
                    else{
                        return $next($request);
                    }
                }
                else if($user->role=='paziente'){
                    
                    //CONTROLLO CHE IL PAZIENTE ABBIA ACCESSO SOLO AI SUOI DATI
                        if($request->idPaziente){
                            if($request->idPaziente==$userId){
                                return $next($request);
                            }
                            else{
                                return $this->unauthorized();
                            }
                        }

                        //SE ESISTE L'ID MEDICO CONTROLLO CHE IL PAZIENTE SIA COLLEGATO
                            if($request->idMedico){
                                if(PazientiMedici::checkPazientiMedici($request)){
                                    return $next($request);
                                    }
                                    else{
                                        return $this->unauthorized();
                                    }
                            }
                            return $next($request);
                        }
                    //SE HA UN RUOLO ERRATO
                    else{
                            return $this->unauthorized();
                        }
                }
                else{
                    return $next($request);
                }

        }
        return $this->unauthorized();
    }

    private function unauthorized($message = null){
        return response()->json([
            'message' => $message ? $message : 'Non Sei autorizzato ad accedere a questa risorsa',
            'success' => false
        ], 401);
    }
}
