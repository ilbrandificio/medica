<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ApiKey;
class AuthenticateApiKey
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request,Closure $next)
    {
        $api=$request->api_key;

        if(ApiKey::validateApi($api)) {
            return $next($request);
        }
        else{
            return response()->json([
               'Key Errata'
            ]);
        }

    }

}
