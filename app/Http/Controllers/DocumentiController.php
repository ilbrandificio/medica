<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Documenti;
use File;
use Response;

class DocumentiController extends Controller
{

    public function store(Request $request)
    {
        $result = Documenti::store($request);

        if($result['status']===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=>$result['message']
            ]);
        }
    }


    public function apriDocumento(Request $request,$filename)
    {
        $path = storage_path('documenti/' . $filename);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }


    public function getDocumentiByIdEntita(Request $request){

        $documenti = Documenti::getDocumentiByIdEntita($request->idEntita,$request->tipo);

        $api_documento= $_ENV['APP_URL'].'api/apri-documento/';

        foreach($documenti as $key=>$value){
            $documenti[$key]['url'] = $api_documento.$value['url'];
        }

        return response()->json(
            $documenti
        );

    }


}
