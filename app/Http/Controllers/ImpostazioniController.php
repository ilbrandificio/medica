<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Impostazioni;

class ImpostazioniController extends Controller
{
    public static function store(Request $request){

        $result = Impostazioni::store($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }


    public static function aggiorna(Request $request){

        $result = Impostazioni::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
    }


    public static function getImpostazioni(){

        $opzioni = Impostazioni::getImpostazioni();

        return response()->json([
            $opzioni
        ]);

    }


}
