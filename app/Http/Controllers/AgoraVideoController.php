<?php

namespace App\Http\Controllers;

use App\Models\Utenti;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Classes\AgoraDynamicKey\RtcTokenBuilder;
use App\Events\MakeAgoraCall;
use JWTAuth;

class AgoraVideoController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;

        if($role=='paziente'){
            $users = Utenti::where('id', '<>', $userId)->where('role','=','medico')->get()->toArray();
        }
        else if($role=='medico'){
            $users = Utenti::where('id', '<>', $userId)->where('role','=','paziente')->get()->toArray();
        }

        return $users;
    }

    public function token(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $name = $user->name;
        $role = $user->role;
        $idMedico = $request->idMedico;

        $appID = env('AGORA_APP_ID');
        $appCertificate = env('AGORA_APP_CERTIFICATE');
        $channelName = $request->channelName;
       // $user = Auth::user()->name;
        $user = $name;
        //$role = RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 3600;
        $currentTimestamp = now()->getTimestamp();
        $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;

        $token = RtcTokenBuilder::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);

        return $token;
    }

    public function callUser(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        
        $data['userToCall'] = $request->user_to_call;
        $data['channelName'] = $request->channel_name;
       // $data['from'] = Auth::id();
       $data['from'] = $userId;

        broadcast(new MakeAgoraCall($data))->toOthers();
    }
}