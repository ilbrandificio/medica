<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utenti;
use App\Models\PazientiMedici;
use App\Models\UtentiMeta;
use App\Models\DatiClinici;
use App\Models\Media;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class UtentiController extends Controller{



    public function storeForAdmin(Request $request){
        $utente='';
    
        DB::transaction(function() use ($request,&$utente)
            {
            $utente = Utenti::storeForAdmin($request);
                if($utente['success']===true){
                    foreach($request['meta'] as $key=>$valore){
                        UtentiMeta::store($utente['idUtente'],$key,$valore);
                    }
                }
        });
    
        if($utente['success']===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $utente['idUtente']
            ]);
        }
    }

/*
    INSERIMENTO UTENTE
*/
public function store(Request $request){
    $utente='';

    DB::transaction(function() use ($request,&$utente)
        {
        $utente = Utenti::store($request);
            if($utente['success']===true){
                foreach($request['meta'] as $key=>$valore){
                    UtentiMeta::store($utente['idUtente'],$key,$valore);
                }
            }
    });

    if($utente['success']===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente['idUtente']
        ]);
    }
}

/*
    CARICAMENTO PROFILO
*/
public function getProfilo(Request $request){
    $utente = Utenti::getProfilo($request);
    return response()->json([
        $utente
    ]);
}
/*
    AGGIORNAMENTO PROFILO
*/
public function aggiornaProfilo(Request $request){

    $utente = Utenti::aggiornaProfilo($request);

    if($utente['success']===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente['idUtente']
        ]);
    }
}
/* INSERISCI MEDICO */
/*
    public function storeMedico(Request $request){
        $utente='';
        DB::transaction(function() use ($request,&$utente)
            {
            $utente = Utenti::store($request,'medico');
                if($utente===true){
                    $idUtente=Utenti::getLastId($request);
                    MediciMeta::store($request,$idUtente);
                }
        });

        if($utente===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $utente
            ]);
        }
    }
/*
/* CARICA MEDICI */
public function getMedici(Request $request){
    $medici = Utenti::getMedici($request);

    return response()->json([
        $medici
    ]);
}


/** GET INFERMIERI */
public function getInfermieri(Request $request){
    $infermieri = Utenti::getInfermieri($request);

    return response()->json([
        $infermieri
    ]);
}

public function aggiornaUtenteAdmin(Request $request){

    $utente = '';

    DB::transaction(function() use ($request,&$utente)
        {
        $utente = Utenti::aggiornaUtente($request);
            if($utente['success']===true){
                foreach($request['meta'] as $key=>$valore){
                    UtentiMeta::aggiorna($utente['idUtente'],$key,$valore);
                }
            }
    });

    if($utente['success']===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente['idUtente']
        ]);
    }
  
}


/*AGGIORNA UTENTE*/
    public function aggiornaUtente(Request $request){
        $result = Utenti::aggiornaUtente($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }

    /* GET UTENTI */
    public function getUtenti(Request $request){
        $utenti = Utenti::getUtenti($request);

        return response()->json([
            $utenti
        ]);
    }


/** GET AVATAR */
    public function getAvatar(Request $request){
        $utente = Utenti::getAvatar($request->idUtente);

        return response()->json([
            $utente
        ]);

    }
    /** GET PAZIENTE */
    public function getPaziente(Request $request){

        $paziente = Utenti::getPaziente($request);

        return response()->json([
            $paziente
        ]);

    }

    /** GET MEDICO */
    public function getMedico(Request $request){

        $medico = Utenti::getMedico($request->idMedico);

        return response()->json([
            $medico
        ]);

    }
    /* GET UTENTE */
    public function getUtente(Request $request){
        $utente = Utenti::getUtente($request);

        return response()->json([
            $utente
        ]);
    }


    /** GET PROFILO GENERIC */
    public function getProfiloGeneric(){
        $utente = Utenti::getProfiloGeneric();

        return response()->json([
            $utente
        ]);
    }


/* GET PROFILO MEDICO */
    public function getProfiloMedico(){
        $medico = Utenti::getProfiloMedico();

        return response()->json([
            $medico
        ]);
    }

    /*
    PER SELEZIONARE UN PAZIENTE PER LO STORE
    */
   public function selezionaPaziente(Request $request) {

    $paziente = Utenti::selezionaPaziente($request);
        return response()->json(
            $paziente
        );
    }

/*AGGIORNA AVATAR*/
    public function aggiornaAvatar(Request $request){

        $idMedia = Media::salvaImmagine($request);

        if($idMedia!==false){
            $result=Utenti::aggiornaAvatar($idMedia);

            if($result === true){
                return response()->json([
                    'success'=>true
                ]);
            }
            else{
                return response()->json([
                    'success'=> false,
                    'message'=> 'Errore Caricamento'
                ]);
            }
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Errore Caricamento'
            ]);
        }


    }


/* GET PROFILO PAZIENTE */
    public function getProfiloPaziente(){
        $paziente = Utenti::getProfiloPaziente();

        return response()->json([
            $paziente
        ]);
    }

/*
    public function aggiornaProfilo(Request $request){
        $utente = Utenti::aggiornaProfilo($request);

        if($utente === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message' => $utente
            ]);
        }
    }
*/  


    /** AGGIORNA PASSWORD ROLE ADMIN*/
    public function aggiornaPasswordRoleAdmin(Request $request){

        $result = Utenti::aggiornaPasswordRoleAdmin($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    /** AGGIORNA PASSWORD */
    public function aggiornaPassword(Request $request){

        $result = Utenti::aggiornaPassword($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    /*CANCELLA UTENTE */
    public function cancellaUtente(Request $request){
        $result = Utenti::cancellaUtente($request);

        if($result === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
    }


    public static function aggiornaSchedaClinica(Request $request){

        $utente = Utenti::aggiornaSchedaClinica($request);

        if($utente === true){
            return response()->json([
                'success'=> true,
            ]);
        }
        else{
            return response()->json([
                'success'=> false
            ]);
        }

    }


    /*CANCELLA PROFILO */
    public function cancellaProfilo(Request $request){

        $result = Utenti::cancellaProfilo($request);

        if($result === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
    }


    /* CARICA MEDICI */
    public function utenteCorrente(Request $request){
        $utente = Utenti::utenteCorrente($request);
            return response()->json(
                $utente
            );
    }


    /** ADMIN ACCESS */
    
    /** 
     * GET ALL PAZIENTI
     * @param $request
     * @return json
    */
    public function getAllPazienti(Request $request){

        $pazienti =  Utenti::getAllPazienti($request);
        return response()->json($pazienti);
        
    }

    public function getAllMedici(Request $request){

        $pazienti =  Utenti::getAllMedici($request);
        return response()->json($pazienti);
        
    }


    public function getAllInfermieri(Request $request){

        $pazienti =  Utenti::getAllInfermieri($request);
        return response()->json($pazienti);
        
    }
   
   

}
?>
