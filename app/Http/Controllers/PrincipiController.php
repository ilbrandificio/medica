<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrincipiController extends Controller
{


    public function getPrincipi(Request $request){

        $keyword = $request->nome;

        $principi = DB::table('prontuario')->where(function($query) use ($keyword){
             $query->where('nomePrincipio','like','%'.$keyword.'%')
                 ->orWhere('nome_commerciale','like','%'.$keyword.'%');
        })->limit(30)->get()->toArray();

        return response()->json([
            $principi
        ]);
    }


}
