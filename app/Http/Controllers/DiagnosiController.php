<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Diagnosi;

class DiagnosiController extends Controller
{

    public function store(Request $request){
        $result = Diagnosi::store($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }


    public function cancella(Request $request){
        $result = Diagnosi::cancella($request->idDiagnosi);

        if($result === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
    }


    public function getDiagnosiPaziente(Request $request){
        $diagnosi = Diagnosi::getDiagnosiPaziente($request);

        return response()->json([
            $diagnosi
        ]);

    }

    public function getDiagnosi(Request $request){
        $descrizione = $request->descrizione;
        $diagnosi = DB::table('diagnosi')->where('descrizione','like','%'.$descrizione.'%')->limit(30)->get()->toArray();;
        return response()->json([
            $diagnosi
        ]);
    }
}
