<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiKey;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class ApiKeyController extends Controller{

/* CARICA API KEY PER PAZIENTE */
public function getApiKey(Request $request){

    $apiKey = ApiKey::getApiKey($request->idPaziente);
        return response()->json([
            $apiKey
        ]);
    }

/* CREA API KEY*/
public function store(Request $request){
    $apiKey = ApiKey::store($request);
    if($apiKey===true){
        return response()->json([
            'success' => true,
            'api_key' => $apiKey
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $apiKey
        ]);
    }
}

/* CANCELLA APIKEY */
public function cancella(Request $request){
    $apiKey = ApiKey::cancella($request->idApiKey);

    if($apiKey === true){
        return response()->json([
            'success'=>true
        ]);
    }
    else{
        return response()->json([
            'success'=>false,
            'message'=> 'Errore Cancellazione'
        ]);
    }
}


  
}

?>
