<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utenti;
use App\Models\Terapie;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class TerapiaController extends Controller{

/* CARICA TERAPIA */
public function getTerapiaPaziente(Request $request){
    $terapia = Terapie::getTerapiaPaziente($request->idPaziente);
        return response()->json([
            $terapia
        ]);
    }

/* APRI TERAPIA */
public function getTerapia(Request $request){
    $terapia = Terapie::getTerapia($request->idTerapia);

    return response()->json([
        $terapia
    ]);
}


/* AGGIORNA TERAPIA */
public function aggiorna(Request $request){
    $terapia = Terapie::aggiorna($request);

    if($terapia===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $terapia
        ]);
    }
}


/* CANCELLA TERAPIA */
public function cancella(Request $request){
    $terapia = Terapie::cancella($request->idTerapia);

    if($terapia === true){
        return response()->json([
            'success'=>true
        ]);
    }
    else{
        return response()->json([
            'success'=>false,
            'message'=> 'Errore Cancellazione'
        ]);
    }
}

/* INSERISCI TERAPIA SOLO PER PAZIENTI*/
public function store(Request $request){
    $terapia = Terapie::store($request);
    if($terapia===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $terapia
        ]);
    }
}
    public function stampa(Request $request)
    {
        if(!isset($request->idSquadra)){
            $idSquadra=null;
        }

        else{
            $idSquadra=$request->idSquadra;
        }

        if(!isset($request->data)){
            $data=date('Y-m-d');
        }
        else{
            if(empty($request->data)){
                $data=date('Y-m-d');
            }
            else{
                $data=$request->data;
            }
        }


        if(!isset($request->idOrdine)){
            $idOrdine=null;
        }
        else{
           $idOrdine= $request->idOrdine;
        }
        $data=Ordini::getOrdiniStampa($idSquadra, $idOrdine,$data);
        if(count($data)==0){
        }
       // return view('stampa',array('data' => $data));
            $pdf = PDF::loadView('stampa',array('data' => $data) );
            return $pdf->stream();

    }
}

?>
