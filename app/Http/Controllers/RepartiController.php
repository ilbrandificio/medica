<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reparti;

class RepartiController extends Controller
{


    public function getReparti(Request $request){

        $reparti = Reparti::getReparti($request);
        return response()->json([
            'result'=>$reparti
        ]);
    }


    /* INSERISCI REPARTO */
    public  function store(Request $request){

        $result = Reparti::store($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }
    /* INSERISCI REPARTO */
    public  function aggiorna(Request $request){

        $result = Reparti::aggiorna($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }

    /*CANCELLA REPARTO */
    public function cancellaReparto(Request $request){
        $result = Reparti::cancellaReparto($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'Errore Cancellazione'
            ]);
        }
    }
}
