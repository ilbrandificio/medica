<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Media;

class MediaController extends Controller
{


public function salvaImmagine(Request $request){

        $result = Media::salvaImmagine($request);

        if($result === true){
            return response()->json([
                'success'=>true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Errore Caricamento'
            ]);
        }

    }

}
