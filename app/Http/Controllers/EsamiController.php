<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utenti;
use App\Models\Esami;
use App\Models\Allarmi;
use App\Models\Documenti;
use JWTAuth;

class EsamiController extends Controller{

/* INSERISCI ESAME */
public function store(Request $request){


    $esame = Esami::store($request);

    if($esame===true){
        Allarmi::checkAllarmi($request);
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $esame
        ]);
    }
}

public function aggiorna(Request $request){

    $result = Esami::aggiorna($request);

    if($result === true){
        return response()->json([
            'success'=> true
        ]);
    }
    else{
        return response()->json([
            'message'=> $result,
            'success'=> false
        ]);
    }
}

    public function registra(Request $request){

        $result = Esami::registra($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'message'=> $result,
                'success'=> false
            ]);
        }
    }


    public function getEsamiPromemoria(Request $request){

        $esamipromemoria = Esami::getEsamiPromemoria($request);

        return response()->json([
            $esamipromemoria
        ]);
    }


    public function cancella(Request $request){
        $result = Esami::cancella($request->idEsame);

        if($result === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
    }

/* CARICA ESAMI PER PAZIENTE */
    public function getEsamiPaziente(Request $request){
        $esami = Esami::getEsamiPaziente($request);
        return response()->json([
            $esami
        ]);
    }

/* CARICA ESAMI PER PAZIENTE PER IL TREND*/
    public function getEsamiPazienteGrafico(Request $request){
        $esami = Esami::getEsamiPazienteGrafico($request);
        return response()->json([
            $esami
        ]);
    }

    /* CARICA ESAME */
    public function getEsamePaziente(Request $request){
        $esame = Esami::getEsamePaziente($request);

        return response()->json([
            $esame
        ]);
    }

}



?>
