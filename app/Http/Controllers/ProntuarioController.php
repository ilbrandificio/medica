<?php

namespace App\Http\Controllers;

use App\Models\Prontuario;
use Illuminate\Http\Request;

class ProntuarioController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
   public function store(Request $request){
       $result = Prontuario::store($request);

       if($result === true){
           return response()->json([
              'success'=> true
           ]);
       }
       else{
           return response()->json([
              'success'=> false,
              'message'=> $result
           ]);
       }
   }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
   public function aggiorna(Request $request){
       $result = Prontuario::aggiorna($request);

       if($result === true){
           return response()->json([
               'success'=> true
           ]);
       }
       else{
           return response()->json([
               'success'=> false,
               'message'=> $result
           ]);
       }
   }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
   public function getProntuario(Request $request){

       $prontuario = Prontuario::getProntuario($request);

       return response()->json($prontuario);

   }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
   public function cancella(Request $request){

       $result = Prontuario::cancella($request->id_principio);

       if($result === true){
           return response()->json([
               'success'=> true
           ]);
       }
       else{
           return response()->json([
              'success'=> false,
              'message'=> 'Errore Cancellazione'
           ]);
       }

   }


}
