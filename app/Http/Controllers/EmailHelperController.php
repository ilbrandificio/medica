<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Models\Utenti;
use Mail;
use Carbon\Carbon;

class EmailHelperController extends Controller{

public static function invioVerificaEmail($idUser){

        $user = Utenti::Select('id','name','email','role')
        ->where('id','=',$idUser)
        ->get()->toArray();

    $email=$user['0']['email'];
    $nome=$user['0']['name'];

    $url = URL::temporarySignedRoute(
        'verification.verify', Carbon::now()->addMinutes(60), ['user' => $idUser]
    );

    $url=str_replace('/api', '', $url);
    $data = array(
        'email'=>$email,
        'nome'=>$nome,
        'url'=>$url,
    );

    Mail::send('emails.verify',$data, function($message) use($email,$nome,$url) {
        $message->to($email, $nome)
                ->subject('Verifica la tua Email');
    });
    }

/*
    EMAIL CLIENTE IN CASO NEGATIVO
*/
public static function invioEmailCrifRossa($data){
    try{
    Mail::send('emails.emailclienterossa', ["data"=>$data], function($message) use($data) {
        $message->to($data['email'], $data['nome'])
                ->subject('Richiesta Finanziamento EasyCredit');
    });
    }
    catch(\Exception $e){
        }
        return true;
    }
/*
    EMAIL CLIENTE IN CASO POSITIVO
*/
public static function invioEmailCrifVerde($data){
    try{
    Mail::send('emails.emailclienteverde',["data"=>$data], function($message) use($data) {
        $message->to($data['email'], $data['nome'])
                ->subject('Richiesta Finanziamento EasyCredit');
    });
    }
    catch(\Exception $e){
        echo($e);
        exit();
        }
        return true;
    }
/*
  INVIO EMAIL CONFESERFIDI
*/
public static function invioEmailConfeserfidi($data){
    $oggetto='';
    if($data['codice_consulente']!==null){
        $oggetto='NON INOLTRARE - POSIZIONE INOLTRATA AUTOMATICAMENTE - Riepilogo Richiesta EasyCredit';
    }
    else{
        $oggetto='Riepilogo Richiesta EasyCredit';
    }
    try{
        Mail::send('emails.emailconfeserfidi', ["data"=>$data], function($message) use($data,$oggetto) {
                $message->to($data['email_confeserfidi'], 'Nuova Richiesta da EasyCredit')
                        ->subject($oggetto);
            });
            }
            catch(\Exception $e){
                echo($e);
                exit();
            }
    return true;
    }

}
