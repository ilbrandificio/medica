<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utenti;
use App\Models\PazientiMeta;
use App\Models\UtentiMeta;
use App\Models\PazientiInfermieri;
use App\Models\Allarmi;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class PazientiController extends Controller{

/* INSERISCI PAZIENTE */
public function storePaziente(Request $request){
    $utente='';
    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $userRole = $user->role;
    $idMedico = $request->idMedico;


    DB::transaction(function() use ($request,&$utente,$userRole,$userId,$idMedico)
        {
        $utente = Utenti::store($request,'paziente');
            if($utente===true){
                $idPaziente=Utenti::getLastId($request);
                PazientiMeta::store($request,$idPaziente);
                //PazientiMedici::assegnaMedico($idMedico,$idPaziente);
                if($userRole=='admin'){
                    //PazientiMedici::assegnaMedico($userId,$idPaziente);
                }
            }
    });

    if($utente===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente
        ]);
    }
}

/* INSERISCI PAZIENTE */
/*public function aggiornaSchedaClinica(Request $request){

    $paziente=PazientiMeta::aggiornaSchedaClinica($request);
    if($paziente){
        return true;
    }
    else{
        return false;
    }
}
*/




    /** ASSEGNA INFERMIERE */
    public function assegnaInfermiere(Request $request){

        $result = PazientiInfermieri::assegnaInfermiere($request->idInfermiere);

        if($result === true){
            return response()->json([
                'success'=> true,
                'message'=> 'Infermiere Assegnato'
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }


    }


   /** CANCELLA MEDICO ASS.*/
   public function cancellaMedicoAss(Request $request){
       
        $idPaziente = $request->idPaziente;
        $idPazienteMedico = $request->idMedico;

        $result = PazientiMedici::cancellaMedicoAss($idPaziente,$idPazienteMedico);

        if($result['success'] === true){
            return response()->json([
                'success'=>true
            ]);
        }
        else{
            return response()->json([
                'success'=>false,
                'message'=> $result['message']
            ]);
        }

   }


   public function aggiorna(Request $request){

        $result =  PazientiMeta::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
            ]);
        }
   }


   public function checkAccessoPaziente(Request $request) {
        $pazientimedici = PazientiMedici::checkAccessoPaziente($request->idPaziente);

        if($pazientimedici === true){
            return response()->json([
                $pazientimedici
            ]);
        }
   }



    public function getPazienteInfermieri(Request $request) {
        $pazienteinfermieri = PazientiInfermieri::getPazienteInfermieri($request->idPaziente);

            return response()->json([
                $pazienteinfermieri
            ]);
        }



/*
    CARICA PAZIENTI
*/
public function getPazienti(Request $request){
    $pazienti = PazientiMeta::getPazienti($request);
        return response()->json([
            $pazienti
        ]);
    }

/*
    CARICA PAZIENTE
*/
public function getPaziente(Request $request){

    $paziente = UtentiMeta::getPaziente($request);

        return response()->json([
            $paziente
        ]);

    }
}

?>
