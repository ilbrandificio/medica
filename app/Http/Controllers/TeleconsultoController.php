<?php

namespace App\Http\Controllers;

use App\Models\Teleconsulto;
use Illuminate\Http\Request;

class TeleconsultoController extends Controller
{
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = Teleconsulto::store($request);

        if($result === true){
            return response()->json([
                'success'=>true,
                'message'=> 'Richiesta Inviata'
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }


     /** TELECONSULTO  */
     public function confermaTeleconsulto(Request $request){

        $result = Teleconsulto::confermaTeleconsulto($request);

        if($result === true){
            return response()->json([
                'success'=> true,
                'message'=> 'Teleconsulto Confermato'
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }
   
    /** AGGIORNA  */
    public function aggiorna(Request $request){

        $result = Teleconsulto::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true,
                'message'=> 'Teleconsulto Aggiornato'
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    /** GET TELECONSULTI  */
    public function getTeleconsulti(Request $request){

        $teleconsulti = Teleconsulto::getTeleconsulti($request);

        return response()->json([
            $teleconsulti
        ]);

    }

}
