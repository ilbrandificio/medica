<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Utenti;
use App\Models\MediciMeta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RegistrazioneController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

/* INSERISCI MEDICO */
public function storeMedico(Request $request){
    $utente='';
    DB::transaction(function() use ($request,&$utente)
        {
        $utente = Utenti::registrazioneMedico($request);
    });

    if($utente===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente
        ]);
    }
}

/* INSERISCI MEDICO */
public function storePaziente(Request $request){
    $utente='';
    DB::transaction(function() use ($request,&$utente)
        {
        $utente = Utenti::registrazionePaziente($request);
    });

    if($utente===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente
        ]);
    }
}

/* INSERISCI INFERMIERE */
public function storeInfermiere(Request $request){
    $utente='';
    DB::transaction(function() use ($request,&$utente)
        {
        $utente = Utenti::registrazioneInfermiere($request);
    });

    if($utente===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente
        ]);
    }
}
}
