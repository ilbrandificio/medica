<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Messaggi;


class MessaggiController extends Controller
{


    public function getChat(Request $request){

        $messaggi = Messaggi::getChat($request);
        return response()->json(
            $messaggi
        );
    }


    /* INSERISCI REPARTO */
    public  function store(Request $request){

        $messaggio = Messaggi::store($request);

        if($messaggio===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }


    /** NOTIFICATION MESSAGGE */
    public static function NotificationMessage(){

        $messaggi = Messaggi::NotificationMessage();

        return response()->json([
            $messaggi
        ]);

    }

    
    /** COUNT MESSAGGI */
    public static function countMessaggi(){

        $messaggi =  Messaggi::countMessaggi();

        return response()->json([
           'messaggi'=> $messaggi
        ]);

    }

    /** SET MESSAGGI LETTI */
    public static function setMessaggiLetti(Request $request){

        $result = Messaggi::setMessaggiLetti($request->idRicevente, $request->idMittente);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

}
