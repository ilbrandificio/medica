<?php

namespace App\Http\Controllers;

use App\Models\Prescrizioni;
use Illuminate\Http\Request;
use PDF;

class PrescrizioniController extends Controller
{
   
    public function store(Request $request){
        $result = Prescrizioni::store($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }


    public function checkCodice(Request $request){

        $result = Prescrizioni::checkCodice($request->idPrescrizione,$request->codice);

        if($result === true){
            $aggiorna_stato = Prescrizioni::aggiornaStatoPrescrizione($request->idPrescrizione);

            if($aggiorna_stato === true){
                return response()->json([
                    'success'=> true
                ]);
            }
            else{
                return response()->json([
                    'success'=> false,
                    'message'=> 'Errore Aggiornamento'
                ]);
            }
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Codice Errato'
            ]);
        }
        
    }


    public function aggiorna(Request $request){
        $result = Prescrizioni::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }

    public function stampa(Request $request){
        $prescrizioni=Prescrizioni::stampa($request->idPrescrizione);
     
        $pdf = PDF::loadView('prescrizioni',array('data' =>$prescrizioni))->setPaper('A4','landscape');
        return $pdf->stream();
    }

    public function getPrescrizioniByIdPaziente(Request $request){

        $prescrizioni = Prescrizioni::getPrescrizioniByIdPaziente($request);

        return response()->json([
            $prescrizioni
        ]);
        
    }

    public function getPrescrizioneByIdPrescrizione(Request $request){

        $prescrizione = Prescrizioni::getPrescrizioneByIdPrescrizione($request);

        return response()->json([
            $prescrizione
        ]);

    }

    public function cancella(Request $request){

        $result = Prescrizioni::cancella($request->idPrescrizione);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }

}
