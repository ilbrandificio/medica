<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Monitoraggio;
use JWTAuth;

class MonitoraggioController extends Controller{


/* STORE DATA*/
    public function store(Request $request){
        $result = Monitoraggio::store($request);
        if($result===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }

}

?>
