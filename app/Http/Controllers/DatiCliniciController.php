<?php

namespace App\Http\Controllers;

use App\Models\DatiClinici;
use Illuminate\Http\Request;


class DatiCliniciController extends Controller
{

    public function store(Request $request){

        $result = DatiClinici::store($request);

        if($result === true){

            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    public function aggiorna(Request $request){

        $result = DatiClinici::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    public function getDatiClinici(Request $request){

        $daticlinici = DatiClinici::getDatiClinici($request);


        return response()->json([
            $daticlinici
        ]);
    }

    public function cancella(Request $request){

        $result = DatiClinici::cancella($request->idDatiClinici);

        if ($result === true) {
            return response()->json([
                'success'=> true,

            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Errore Cancellazione'
            ]);
        }

    }

}
