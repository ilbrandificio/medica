<?php

namespace App\Http\Controllers;

use App\Models\SchedaAnticoagulante;
use Illuminate\Http\Request;

class SchedaAnticoagulanteController extends Controller
{
   
    public function store(Request $request){

        $result = SchedaAnticoagulante::store($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]); 
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }


    public static function  getSchedaAnticoagulanteById(Request $request){

        $scheda_anticoagulante = SchedaAnticoagulante::getSchedaAnticoagulanteById($request->idAnticoagulante);

        return response()->json(
            $scheda_anticoagulante
        );

    }

    public static function cancella(Request $request){

        $result = SchedaAnticoagulante::cancella($request->idSchedaAnticoagulante);

        if ($result === true) {
            return response()->json([
                'success'=> true
            ]);       
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Errore Cancellazione'
            ]);
        }
    }

}
