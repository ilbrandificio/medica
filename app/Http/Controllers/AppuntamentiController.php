<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appuntamenti;

class AppuntamentiController extends Controller
{
    public function store(Request $request){
        $result = Appuntamenti::store($request);

        if($result===true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $result
            ]);
        }
    }


    public function getAppuntamentiPaziente(Request $request){

        $visite = Appuntamenti::getAppuntamentiPaziente($request);

        return response()->json([
            $visite
        ]);

    }


    public function getAppuntamenti(Request $request){

        $visite = Appuntamenti::getAppuntamenti($request);

        return response()->json([
            $visite
        ]);

    }


    public function getAppuntamentiMedico(){

        $appuntamenti = Appuntamenti::getAppuntamentiMedico();

        return response()->json(
            $appuntamenti
        );

    }


    public function aggiorna(Request $request){

        $result = Appuntamenti::aggiorna($request);

        if($result['success'] === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'message' => $result['visita'],
                'success'=> false
            ]);
        }
    }

    public function cancellaAppuntamento(Request $request){
        $result = Appuntamenti::cancellaAppuntamento($request->idAppuntamento);

        if($result === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
        }
}
