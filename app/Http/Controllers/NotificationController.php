<?php

namespace App\Http\Controllers;

use JWTAuth;
use Notification;
use App\Models\Esami;
use App\Models\Utenti;
use App\Models\Prescrizioni;
use Illuminate\Http\Request;
use App\Models\Anticoagulante;
use App\Models\PrescrizioniEsami;
use App\Notifications\ReminderInr;
use App\Notifications\CodicePrescrizione;
use App\Notifications\EsamiScadutiNotification;
use App\Notifications\PrescrizioniEsamiScaduti;
use App\Notifications\EsamiPromemoriaNotification;
use App\Notifications\PrescrizioniEsamiPromemoria;

class NotificationController extends Controller
{

    /** SEND ESAMI PROMEMORIA */
    public function sendEsamiPromemoriaNot(Request $request){

        $esami = Esami::getEsamiPromemoriaNot($request->giorni);

        if($esami){
            foreach($esami as $key=>$value){

                $user = Utenti::find($value['idPaziente']);

                $notification = Notification::send($user, new EsamiPromemoriaNotification($value));
            }

            return response()->json([
                'message'=> 'Notifiche Inviate'
            ]);

        }
        else{
            return response()->json([
                'message'=> 'Nessun esame in promemoria'
            ]);
        }

    }

    /** SEND ESAMI SCADUTI */
    public function sendEsamiScadutiNot(Request $request){

        $esami = Esami::getEsamiScaduti($request->giorni);

        if($esami){
            foreach($esami as $key=>$value){

                $user = Utenti::find($value['idPaziente']);

                $notification = Notification::send($user, new EsamiScadutiNotification($value));
            }

            return response()->json([
                'message'=> 'Notifiche Inviate'
            ]);

        }
        else{
            return response()->json([
                'message'=> 'Nessun esame scaduto'
            ]);
        }

    }

    public function sendPrescrizioniEsamiScaduti(Request $request){

        $prescrizioni_esami = PrescrizioniEsami::getPrescrzioniEsamiScaduti($request->giorni);

        if($prescrizioni_esami){
            foreach($prescrizioni_esami as $key=>$value){

                $user = Utenti::find($value['idPaziente']);
                $notification = Notification::send($user, new PrescrizioniEsamiScaduti($value));
            }

            return response()->json([
                'message'=> 'Notifiche Inviate'
            ]);

        }
        else{
            return response()->json([
                'message'=> 'Nessuna Prescrizione scaduta'
            ]);
        }

    }


    public function sendPrescrizioniEsamiPromemoria(Request $request){

        $prescrizioni_esami = PrescrizioniEsami::getPrescrizioniEsamiAttesa($request->giorni);

        if($prescrizioni_esami){
            foreach($prescrizioni_esami as $key=>$value){

                $user = Utenti::find($value['idPaziente']);
                $notification = Notification::send($user, new PrescrizioniEsamiPromemoria($value));
            }

            return response()->json([
                'message'=> 'Notifiche Inviate'
            ]);

        }
        else{
            return response()->json([
                'message'=> 'Nessuna Prescrizione in promemoria'
            ]);
        }
    }


    //TODO: FIX SEND REMINDER INR
    public function sendReminderInr(){

        $current_date = date('Y-m-d');
        $subtractday = strtotime('- 1 days', strtotime($current_date));
        $final_date = date('Y-m-d',$subtractday);

        $anticoagulanti = Anticoagulante::getAnticoagulantiAttivi();

        if($anticoagulanti){
                foreach($anticoagulanti as $key=>$anticoagulante){
                    if($anticoagulante['ultima_modifica']<$final_date){
                        $user = Utenti::find($anticoagulante['idPaziente']);
                        $notification = Notification::send($user, new ReminderInr($anticoagulante));
                    }
                }
            return true;
        }
        else{
            return false;
        }

    }


    /** SEND CODICE PRESCRIZIONE */
    public static function sendCodicePrescrizione(Request $request){
        
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $prescrizione = Prescrizioni::getPrescrizioneUtenteCorrente($request->idPrescrizione);

        $utente = Utenti::find($userId);

        $notification = Notification::send($utente, new CodicePrescrizione($prescrizione));

        return response()->json([
            'success'=> true
        ]);

    }

}
