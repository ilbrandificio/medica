<?php

namespace App\Http\Controllers;

use App\Models\PazientiInfermieri;
use App\Models\InfermieriMeta;
use Illuminate\Http\Request;

class InfermieriController extends Controller
{

    /** GET INFERMIERI */
    public function getInfermieri(Request $request){

        $infermieri = InfermieriMeta::getInfermieri($request);

        return response()->json([
            $infermieri
        ]);

    }

    /** GET PAZIENTE INFERMIERE */
    public function getPazientiInfermieri(Request $request){

        $pazienti_infermieri = PazientiInfermieri::getPazientiInfermieri($request);

        return response()->json([
            $pazienti_infermieri
        ]);


    }


}
