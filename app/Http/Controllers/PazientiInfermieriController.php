<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PazientiInfermieri;

class PazientiInfermieriController extends Controller
{
    public function getRichieste(){

        $richieste = PazientiInfermieri::getRichieste();

        return response()->json([
            $richieste
        ]);


    }

   
    public function accettaRichiesta(Request $request){

        $result = PazientiInfermieri::accettaRichiesta($request);

        if ($result === true) {
            return response()->json([
                'success'=>true
            ]);
        }
        else{
            return response()->json([
                'success'=> false
            ]); 
        }
    

    }

    /*
    public function PazientiInfermieri(Request $request){

        $richiesta = PazientiMedici::accettaRichiesta($request);

        if($richiesta===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $richiesta
            ]);
        }
    
    }
        */


    public function getPazientiInfermiere(Request $request){

        $result = PazientiInfermieri::getPazientiInfermiere($request->idInfermiere);
        return response()->json($result);

    }



}
