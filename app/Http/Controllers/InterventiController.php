<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interventi;

class InterventiController extends Controller
{
    public function store(Request $request){
        $intervento = Interventi::store($request);
        if($intervento===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $intervento
            ]);
        }
    }

    public function getInterventi(Request $request){
        $interventi = Interventi::getInterventi($request);


        return response()->json([
            $interventi
        ]); 
    }


    public function aggiorna(Request $request){
        $result = Interventi::aggiorna($request);


        if($result === true){
            return response()->json([
                'intervento'=> $result,
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'message'=> $result,
                'success'=> false
            ]);
        }
   
    }


    public function cancella(Request $request){
        $intervento = Interventi::cancella($request->idIntervento);

        if($intervento === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
    }


}
