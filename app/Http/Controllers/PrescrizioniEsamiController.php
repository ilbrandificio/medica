<?php

namespace App\Http\Controllers;

use App\Models\PrescrizioniEsami;
use Illuminate\Http\Request;
use PDF;

class PrescrizioniEsamiController extends Controller
{

    public function store(Request $request){
        $result = PrescrizioniEsami::store($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }

    public function aggiorna(Request $request){
        $result = PrescrizioniEsami::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }


    /** AGGIORNA STATO ESAME (Segna come registrato)
     * @param $request
     * @return json
     */
    public function aggiornaStatoEsame(Request $request){

        $idPrescrizioneEsame = $request->idPrescrizioneEsame;
        $codice_esame = $request->codice_esame;
        $tipo_esame = $request->tipo_esame;

        $result = PrescrizioniEsami::aggiornaStatoEsame($idPrescrizioneEsame,$codice_esame,$tipo_esame);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false
            ]);
        }


    }

    public function stampa(Request $request){
        $prescrizioni=PrescrizioniEsami::stampa($request->idPrescrizioneEsame);

        $pdf = PDF::loadView('prescrizioni_esami',array('data' =>$prescrizioni))->setPaper('A4','potrait');
        return $pdf->stream();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPrescrizioniEsamiPromemoria(Request $request){

        $prescrizioni = PrescrizioniEsami::getPrescrizioneEsamiPromemoria($request->idPaziente);

        return response()->json($prescrizioni);

    }

    public function getPrescrizioniEsamiByIdPaziente(Request $request){

        $prescrizioni = PrescrizioniEsami::getPrescrizioniEsamiByIdPaziente($request);

        return response()->json([
            $prescrizioni
        ]);

    }

    public function getPrescrizioneByIdPrescrizioneEsame(Request $request){

        $prescrizione = PrescrizioniEsami::getPrescrizioneByIdPrescrizioneEsame($request);

        return response()->json(
            $prescrizione
        );

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEsamiPromemoriaByIdPrescrizione(Request $request){

        $esami = PrescrizioniEsami::getEsamiPromemoriaByIdPrescrizione($request->idPrescrizioneEsame);

        return response()->json(
            $esami
        );

    }

    public function cancella(Request $request){

        $result = PrescrizioniEsami::cancella($request->idPrescrizioneEsame);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }

}
