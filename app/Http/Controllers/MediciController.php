<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utenti;
use App\Models\MediciMeta;
use App\Models\PazientiMedici;
use App\Models\Appuntamenti;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class MediciController extends Controller{

/* INSERISCI MEDICO */
public function storeMedico(Request $request){
    $utente='';
    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $userRole = $user->role;

    DB::transaction(function() use ($request,&$utente,$userRole,$userId)
        {
        $utente = Utenti::store($request,'medico');
            if($utente===true){
                $idMedico=Utenti::getLastId($request);
                MediciMeta::store($request,$idMedico);
                if($userRole=='admin'){
                    PazientiMedici::assegnaMedico($userId,$idMedico);
                }
            }
    });

    if($utente===true){
        return response()->json([
            'success' => true,
        ]);
    }
    else{
        return response()->json([
            'success' => false,
            'message'=> $utente
        ]);
    }
}

/* CARICA MEDICI */
public function getMedici(Request $request){

    $medici = MediciMeta::getMedici($request);

    return response()->json([
        $medici
    ]);
}

/* CARICA MEDICO */
public function getMedico(Request $request){
    $medico = MediciMeta::getMedico($request->idMedico);
        return response()->json([
            $medico
        ]);
    }


/* CARICA DASHBOARD */
public function getDashboard(Request $request){

    $pazienti_in_cura = PazientiMedici::getPazientiMediciAttivi();
    $pazienti_in_attesa = PazientiMedici::getPazientiMediciInAttesa();
    $visite = Appuntamenti::getAppuntamentiDataCorrente();

    return response()->json([
        'pazienti_in_cura'=> $pazienti_in_cura,
        'pazienti_in_attesa'=> $pazienti_in_attesa,
        'visite'=> $visite
    ]);
}

/** AGGIORNA TELECONSULTO */
public function aggiornaTeleconsulto(Request $request){

    $result = MediciMeta::aggiornaTeleconsulto($request);


    if ($result === true) {
        return response()->json([
            'success'=> true,
            'message'=> 'Teleconsulto Aggiornato'
        ]);
    }
    else{
        return response()->json([
            'success'=> false,
            'message'=> $result
        ]);
    }


}


/** AGGIORNA MEDICO */

public function aggiorna(Request $request){

    $result = MediciMeta::aggiorna($request);

    if($result === true){
        return response()->json([
            'success'=> true
        ]);
    }
    else{
        return response()->json([
            'success'=> false
        ]);
    }

}


}

?>
