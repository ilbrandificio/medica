<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PazientiMedici;
use JWTAuth;

class PazientiMediciController extends Controller
{

/*ASSEGNA MEDICO */
        public function assegnaMedico(Request $request){

            $pazientemedico = PazientiMedici::assegnaMedico($request->medico);

            if($pazientemedico===true){
                return response()->json([
                    'success' => true,
                ]);
            }
            else{
                return response()->json([
                    'success' => false,
                    'message'=> $pazientemedico
                ]);
            }
        }

    public function getRichieste(){

        $richieste = PazientiMedici::getRichieste();

        return response()->json([
            $richieste
        ]);


    }

/*
    RECUPERO I MEDICI DI UN PAZIENTE
*/
    public function getPazienteMedici(Request $request) {
        $pazientemedici = PazientiMedici::getPazienteMedici($request->idPaziente);

            return response()->json([
                $pazientemedici
            ]);
        }

/*
    RECUPERO I PAZIENTI DI UN MEDICO
*/
    public function getPazientiMedici(Request $request){

        $pazienti = PazientiMedici::getPazientiMedici($request);

        return response()->json([
            $pazienti
        ]);

    }


    public function getMediciPaziente(){

        $medici = PazientiMedici::getMediciPaziente();

        return response()->json([
            $medici
        ]);
    }

    /** GET PAZIENTI MEDICO  */
    public function getPazientiMedico(Request $request){

        $pazientimedico = PazientiMedici::getPazientiMedico($request->idMedico);

        return response()->json($pazientimedico);
    }

    public function accettaRichiesta(Request $request){

        $richiesta = PazientiMedici::accettaRichiesta($request);

        if($richiesta===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $richiesta
            ]);
        }

    }

    /** CANCELLA PAZIENTE ASSEGNATO
     * @param $request
     * @return json
    */
    public static function cancellaPazienteAssegnato(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $result = PazientiMedici::cancellaPazienteAssegnato($userId,$request->idPazienteMedico);

        if($result===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'Errore Cancellazione Paziente Assegnato'
            ]);
        }
    }

    /** CANCELLA MEDICO ASSEGNATO
     * @param $request 
     * @return json
     */
    public static function cancellaMedicoAssegnato(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $result = PazientiMedici::cancellaMedicoAssegnato($userId,$request->idPazienteMedico);

        if($result===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'Errore Cancellazione Medico Assegnato'
            ]);
        }
    }
}
