<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PianiTerapeutici;
use \PDF;

class PianiTerapeuticiController extends Controller{

    public static function store(Request $request){
        $pianoTerapeutico = PianiTerapeutici::store($request);

        if($pianoTerapeutico===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $pianoTerapeutico
            ]);
        }
    }


    public static function getPianiTerapeutici(Request $request){

        $pianiTerapeutici = PianiTerapeutici::getPianiTerapeutici($request);

        return response()->json([
            $pianiTerapeutici
        ]);
    }

    public static function aggiorna(Request $request){
        $pianoTerapeutico = PianiTerapeutici::aggiorna($request);

        if($pianoTerapeutico===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $pianoTerapeutico
            ]);
        }
    }

    public static function getPianoById(Request $request){

        $pianoTerapeutico = PianiTerapeutici::getPianoById($request->idPianoTerapeutico);

        return response()->json([
            $pianoTerapeutico
        ]);
    }
/*
    STAMPA PIANO TERAPEUTICO
*/
    public function stampaPiano(Request $request)
    {
        $piano=PianiTerapeutici::getPianoById($request->idPianoTerapeutico);
        $tipo_piano=$piano['codicePianoTerapeutico'];
            $pdf = PDF::loadView('piani.'.$tipo_piano,array('piano' => $piano) );
            return $pdf->stream();

    }

    /** CANCELLA  */
    public function cancella(Request $request){

        $result = PianiTerapeutici::cancella($request->idPianoTerapeutico);

        if ($result === true) {
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Errore Cancellazione'
            ]);
        }
    }

}
