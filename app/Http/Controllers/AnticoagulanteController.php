<?php

namespace App\Http\Controllers;

use App\Models\Anticoagulante;
use Illuminate\Http\Request;
use PDF;

class AnticoagulanteController extends Controller
{

    public function store(Request $request){

        $result = Anticoagulante::store($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }
    }


    public function aggiorna(Request $request){

        $result = Anticoagulante::aggiorna($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    public function aggiornaSchedaAnticoagulante (Request $request){

        $result = Anticoagulante::aggiornaSchedaAnticoagulante ($request);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> $result
            ]);
        }

    }

    public function getAnticoagulanti(Request $request){

       $anticoagulante =  Anticoagulante::getAnticoagulanti($request);

       return response()->json([
         $anticoagulante
       ]);

    }

    public function getAnticoagulanteById(Request $request){

        $anticoagulante =  Anticoagulante::getAnticoagulanteById($request);

        return response()->json([
          $anticoagulante
        ]); 
    }


    /**
     * STAMPA SCHEDA ANTICOAGULANTE
     */
    public function stampa(Request $request){

        $scheda_anticoagulante = Anticoagulante::getAnticoagulanteById($request);

        $pdf = PDF::loadView('scheda_anticoagulante',array('data'=> $scheda_anticoagulante));
        return $pdf->stream();

    }

    public function cancella(Request $request){

        $result = Anticoagulante::cancella($request->idAnticoagulante);

        if($result === true){
            return response()->json([
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'success'=> false,
                'message'=> 'Errore Cancellazione'
            ]);
        }


    }

}
