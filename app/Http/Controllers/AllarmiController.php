<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Allarmi;

class AllarmiController extends Controller
{
    public function store(Request $request){
        $allarme = Allarmi::store($request);
        if($allarme===true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> $allarme
            ]);
        }
    }

    public function getAllarmi(Request $request){
        $allarmi = Allarmi::getAllarmi($request);
        return response()->json([
            $allarmi
        ]);
    }


    public function getAllarmiAttiviMedico(Request $request){
        $allarmi = Allarmi::getAllarmiAttiviMedico($request);
        return response()->json([
            $allarmi
        ]);
    }

/*
    RECUPERO ALLARMI PER ESAME
*/
    public function getAllarmiAttiviByEsame(Request $request){
        $allarmi = Allarmi::getAllarmiAttiviByEsame($request);
        return response()->json([
            $allarmi
        ]);
    }



    public function aggiorna(Request $request){
        $result = Allarmi::aggiorna($request);


        if($result === true){
            return response()->json([
                'allarme'=> $result,
                'success'=> true
            ]);
        }
        else{
            return response()->json([
                'message'=> $result,
                'success'=> false
            ]);
        }

    }

    public function cancella(Request $request){
        $allarme = Allarmi::cancella($request->idAllarme);

        if($allarme === true){
            return response()->json([
                'success' => true,
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message'=> 'errore cancellazione'
            ]);
        }
    }

    public function countAllarmiAttivi(Request  $request){

        $result = Allarmi::countAllarmiAttivi($request);

        return response()->json([
            'allarmi_attivi'=> $result
        ]);

    }

    public function segnaComeLetto(Request $request){

        $result = Allarmi::segnaComeLetto($request->idPaziente,$request->idAllarme);

        if($result['success'] === true){
            return response()->json([
                'success' => true
            ]);
        }
        else{
            return response()->json([
                'success' => false,
                'message' => $result['message']
            ]);
        }
    }


}
