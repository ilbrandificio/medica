<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Respect\Validation\Validator as v;
use App\Models\PazientiMedici;
use App\Models\Utenti;
use App\Models\Documenti;
use App\Models\PrescrizioniEsami;
use Carbon\Carbon;

use JWTAuth;

class Esami extends Model
{

    protected $table = 'esami';
    protected $primaryKey = 'idEsame';
    protected $guarded = ['idEsame'];

/* CARICA ESAMI */
    public static function getEsamiPaziente($request){

        $caricamento = $request->caricamento;
        $codiceEsame=$request->codiceEsame;
        $idPaziente=$request->idPaziente;


        $esami = Esami::select()->where('idPaziente','=',$idPaziente);

        if($codiceEsame!='0'){
            $esami=$esami->where('codiceEsame','=',$codiceEsame);
        }

        if($caricamento != '0'){
            $esami = $esami->where('caricamento','=',$caricamento);
        }


        $esami=$esami->orderBy('data','DESC')->limit(50)->get()->toArray();

        foreach($esami as $key => $esame){

            $esami[$key]['esame']=unserialize($esame['esame']);
        }

        return $esami;
    }

/* CARICA ESAMI */
public static function getEsamiPazienteGrafico($request){
    $codiceEsame=$request->codiceEsame;
    $idPaziente=$request->idPaziente;
    $esami = Esami::select()
    ->where('idPaziente','=',$idPaziente)
    ->where('caricamento','=','manuale');

    if($codiceEsame!='0'){
        $esami=$esami->where('codiceEsame','=',$codiceEsame);
    }

    $esami=$esami->orderBy('data','DESC')->limit(10)->get()->toArray();

    foreach($esami as $key => $esame){
        $esami[$key]['esame']=unserialize($esame['esame']);
    }

    return $esami;
}

/* CARICA ESAME */

    public static function getEsamePaziente($request){
        $idEsame=$request->idEsame;
        $esame = Esami::Select()->where('idEsame','=',$idEsame)->get()->toArray();
        $esame['0']['esame']=unserialize($esame['0']['esame']);

        if( $esame['0']['caricamento']=='documento'){

            $documenti= Documenti::getDocumentiByIdEntita($idEsame,'esame');

            $api_documento= $_ENV['APP_URL'].'/api/apri-documento/';

            foreach($documenti as $key=>$documento){
                $documenti[$key]['url']=$api_documento.$documento['url'];
            }

            $esame['0']['documenti']= $documenti;
        }
        return $esame['0'];
    }

/* CARICA ESAME */

    /** GET  ULTIMO ECG */
    public static function getLastEcg($id){
        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','ecg')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();
        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }
        return false;
    }

    /** GET ULTIMO EMOGAS ARTERIOSO */
    public static function getLastEmogasArterioso($id){
        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','emogas_arterioso')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();
        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }
        return false;
    }


    /** GET ULTIMO MARKERS SCOPMENSO */
    public static function getLastMarkersScompenso($id){
        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','markers_scompenso')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();
        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }
        return false;
    }

     /** GET ULTIMO ECOCARDIOGRAMMA */
     public static function getLastEcocardiogramma($id){
        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','ecocardiogramma')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();
        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }
        return false;
    }

    /** GET ULTIMO MARKERS INFETTIVI */
    public static function getLastMarkersInfettivo($id){

        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','markers_infettivi')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();

        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }

        return false;
    }


    /** GET ULTIMO PROTIDOGRAMMA */
    public static function getLastProtidogramma($id){

        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','protidogramma')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();

        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }

        return false;
    }

     /** GET ULTIMO ESAME FUNZIONE RENALE */
     public static function getLastFunzioneRenale($id){

        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','funzione_renale')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();

        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }

        return false;
    }

    /** GET ULTIMO ESAME URINE */
    public static function getLastUrine($id){

        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','urine')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();

        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }

        return false;
    }


    /** GET ULTIMO ESAME URINE */
    public static function getLastMetabolico($id){

        $esame = Esami::Select()->where('idPaziente','=',$id)
        ->where('codiceEsame','=','metabolico')
        ->where('caricamento','=','manuale')
        ->orderby('data','DESC')
        ->limit(1)->get()->toArray();

        if($esame){
            $esame['0']['esame']=unserialize($esame['0']['esame']);
            return $esame['0'];
        }

        return false;
    }


/* INSERIMENTO ESAME */
    public static function store($request){
        $validate=Esami::validate($request);

        $idPrescrizioneEsame = $request->idPrescrizioneEsame;

        if($validate===true){
            if($request->tipoCaricamento=='manuale'){
            $esame = Esami::create(array(
                    "idPaziente"=>$request->idPaziente,
                    'data'=>$request->data,
                    'caricamento'=>$request->tipoCaricamento,
                    'codiceEsame'=>$request->codiceEsame,
                    'esame'=>serialize($request->esame),
                    ));
            }
            else{
                $esame = Esami::create(array(
                    "idPaziente"=>$request->idPaziente,
                    'data'=>$request->data,
                    'caricamento'=>$request->tipoCaricamento,
                    'codiceEsame'=>$request->codiceEsame,
                    ));
            }

            if($esame){

                if($request->tipoCaricamento=='documento'){
                    Documenti::store($request,$esame->idEsame);
                }

                if($request->tipoCaricamento == 'manuale' && $idPrescrizioneEsame!='' && $request->codiceEsame != ''){
                    PrescrizioniEsami::aggiornaStatoEsame($idPrescrizioneEsame,$request->codiceEsame,$request->tipo);
                }

                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

/* MODIFICA ESAME */
    public static function aggiorna($request){
        $validate=Esami::validate($request);
        $idEsame = $request->idEsame;

        if($validate===true){
            $esame = Esami::where('idEsame','=',$idEsame)->update(array(
                'caricamento'=> $request->tipoCaricamento,
                'data'=>$request->data,
                'esame'=>serialize($request->esame),
                ));
            if($esame){

                if($request->tipoCaricamento=='documento'){
                    Documenti::store($request,$idEsame);
                }

                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }


    /** REGISTRA ESAME */
    public static function registraEsame($idEsame){

        $esame = Esami::where('idEsame','=',$idEsame)
        ->update(array(
            'caricamento'=>'manuale'
        ));


        if($esame){
            return true;
        }
        else{
            return false;
        }

    }

    public static function cancella($idEsame){
        $esame = Esami::where('idEsame','=',$idEsame)->delete();

        if($esame){
            return true;
        }
        else{
            return false;
        }

        return $esame;
    }


    /** GET ESAMI PROMEMORIA */
    public static function getEsamiPromemoria($request){

        $idPaziente=$request->idPaziente;

        $datacorrente = date("Y-m-d");

        $esami =  Esami::Select('codiceEsame','data','caricamento')
        ->where('caricamento','=','promemoria')
        ->where('idPaziente','=',$idPaziente)
        ->orderBy('data','ASC')
        ->get()->toArray();


        foreach($esami as $key=>$esame){

            if($esame['data'] < $datacorrente){
                $esami[$key]['stato'] = 'scaduto';
            }
            else{
                $esami[$key]['stato'] = 'in attesa';
            }

        }


        return $esami;

    }

    /** GET ESAMI IN SCADENZA NOTIFICATION */
    public static function getEsamiPromemoriaNot($giorni){

        if($giorni==null){
            $giorni=10;
        }

        $current_date = date('Y-m-d');

        $addday = strtotime('+'.$giorni.' days', strtotime($current_date));

        $final_date = date('Y-m-d',$addday);

        $esami = Esami::Select('idEsame','idPaziente','caricamento','data','codiceEsame','name')
        ->join('users','users.id','=','esami.idPaziente')
        ->where('caricamento','=','promemoria')
        ->whereBetween('data',[$current_date,$final_date])
        ->get()->toArray();

        return $esami;

    }


    /** GET ESAMI SCADUTI NOTIFICATION */
    public static function getEsamiScaduti($giorni){

        if($giorni==null){
            $giorni=10;
        }

        $current_date = date('Y-m-d');

        $subtractday = strtotime('-'.$giorni.' days', strtotime($current_date));

        $final_date = date('Y-m-d',$subtractday);

        $esami = Esami::Select('idEsame','idPaziente','caricamento','data','codiceEsame','name')
        ->join('users','users.id','=','esami.idPaziente')
        ->where('caricamento','=','promemoria')
        ->whereBetween('data',[$final_date,$current_date])
        ->get()->toArray();

        return $esami;

    }


    public static function validate($request){

        $datacorrente = Carbon::now();
        $message = '';
        $idPaziente = $request->idPaziente;

        $data = $request->data;
        $tipoCaricamento = $request->tipoCaricamento;
        $codiceEsame = $request->codiceEsame;


        if($tipoCaricamento == 'promemoria'){
            if($data == $datacorrente || $data < $datacorrente){
                $message = 'Inserisci una Data Futura';
            }
        }

        if(!v::notEmpty()->validate($data)){
            $message = 'Inserisci Data';
        }
        else if(!v::notEmpty()->validate($tipoCaricamento)){
            $message = 'Seleziona Tipo Caricamento';



        }
        else if(!v::notEmpty()->validate($codiceEsame)){
            $message = 'Inserisci Codice Esame';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }
    }
}
