<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;

class Prontuario extends Model
{
    protected $table = 'prontuario';
    protected $primaryKey = 'id_principio';
    protected $guarded = ['id_principio'];

    public static function store($request){
        $validate = self::validate($request);

        if($validate === true){

            $principio =  self::create(array(
                'nomePrincipio'=> $request->nomePrincipio,
                'nome_commerciale'=> $request->nome_commerciale
            ));

            if($principio){
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return $validate;
        }

    }

    /**
     * @param $request
     * @return bool|string
     */
    public static function aggiorna($request){

        $validate = self::validate($request);
        $id_principio = $request->id_principio;

        if($validate === true){
            $principio =  self::where('id_principio','=',$id_principio)
                ->update(array(
                    'nomePrincipio'=> $request->nomePrincipio,
                    'nome_commerciale'=> $request->nome_commerciale
                ));

            if($principio){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }
    }

    /**
     * @return mixed
     */
    public static function getProntuario($request){

        $prontuario = self::Select()->get()->toArray();

        return $prontuario;
    }

    /**
     * @param $id_principio
     * @return bool
     */
    public static function cancella($id_principio){

        $principio = self::where('id_principio','=',$id_principio)->delete();

        if($principio){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * @param $request
     * @return bool|string
     */
    public static function validate($request){
        $message = '';

        $nomePrincipio = $request->nomePrincipio;
        $nome_commerciale = $request->nome_commerciale;

        if(!v::notEmpty()->validate($nomePrincipio)){
            $message = 'Inserici nome principio';
        }
        else if(!v::notEmpty()->validate($nome_commerciale)){
            $message = 'Inserici nome commerciale';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }
    }

}
