<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;

class Reparti extends Model
{
    protected $table = 'reparti';
    protected $primaryKey = 'idReparto';
    protected $guarded = ['idReparto'];

/* FILTRA REPARTI */
    public static function getReparti($request){
        $nomeReparto = $request->nomeReparto;
        $reparti = Reparti::Select();
        if(!empty($nomeReparto)){
            $reparti = $reparti->where('nomeReparto','like','%'.$nomeReparto.'%');
        }
        else{
            $reparti = $reparti->Select();
        }

        $reparti = $reparti->get()->toArray();

        return $reparti;
    }





/*INSERISCI REPARTO */
    public static function store($request){
        $validate = Reparti::validate($request);
        if($validate===true){
                $reparto = Reparti::create(array(
                    "codiceReparto"=>$request->codiceReparto,
                    "nomeReparto"=>$request->nomeReparto
                ));
                if($reparto){
                    return true;
                }
                else{
                    return false;
                }
            }
        else{
            return $validate;
        }
    }

/*AGGIORNA REPARTO */
public static function aggiorna($request){
    $validate = Reparti::validate($request);
        if($validate===true){
            $idReparto=$request->idReparto;
                $reparto = Reparti::where('idReparto','=',$idReparto)->update(array(
                    'codiceReparto'=>$request->codiceReparto,
                    'nomeReparto'=>$request->nomeReparto,
                ));

            if($reparto){
                return true;
            }
            else{
                return false;
            }
         }
         else{

         }
         return $validate;
    }

    /*VALIDATE UTENTI */
    public static function validate($request)
    {
        $message='';

            $codice = $request->codiceReparto;
            $nome = $request->nomeReparto;

            if(!v::notEmpty()->validate($codice)){
                $message = 'Inserisci Codice';
            }

            if(!v::notEmpty()->validate($nome)){
                $message = 'Inserisci Nome';
            }

            if($message!=''){
                return $message;
            }
            else{
                return true;
            }
        }

}
