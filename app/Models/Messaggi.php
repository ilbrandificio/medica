<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use JWTAuth;
use DB;

class Messaggi extends Model
{
    protected $table = 'messaggi';
    protected $primaryKey = 'idMessaggio';
    protected $guarded = ['idMessaggio'];


    public static function getChat($request){
        $idRicevente=$request->idChat;

        $user = JWTAuth::parseToken()->authenticate();
        $idMittente = $user->id;
        $role = $user->role;

        $chat = Messaggi::Select('idMessaggio','messaggio','idMittente','created_at','letto')
        ->where('idMittente','=',$idMittente)
        ->where('idRicevente','=',$idRicevente)
        ->orWhere(function($query) use ($idMittente,$idRicevente) {
            $query->where('idMittente','=',$idRicevente)
                  ->where('idRicevente','=',$idMittente);
            });

        $chat=$chat->orderBy('created_at','ASC')->get()->toArray();
        if($chat){
            //Messaggi::setMessaggiLetti($idRicevente, $idMittente);
        }
        return $chat;
    }
/*
    IMPOSTO MESSAGGI LETTI
*/
    public static function setMessaggiLetti($idRicevente, $idMittente){

        $chat = Messaggi::where('idMittente','=',$idMittente)
        ->where('idRicevente','=',$idRicevente)
        ->orWhere(function($query) use ($idMittente,$idRicevente) {
            $query->where('idMittente','=',$idRicevente)
                  ->where('idRicevente','=',$idMittente);
            })
            ->update(array(
                'letto'=>1,
                ));


        if($chat){
            return true;
        }    
        else {
            return false;
        }     
    }

/*
    IMPOSTA MESSAGGI LETTI
*/
public static function setMessaggioLetto($idMessaggio){

    $user = JWTAuth::parseToken()->authenticate();
    $idRicevente = $user->id;
    $role = $user->role;

    $chat = Messaggi::where('idRicevente','=',$idRicevente)
    ->where('idMessaggio','=',$idMessaggio)
    ->update(array(
        'letto'=>1,
    ));
    return true;
}
/*
    MESSAGGI DA LEGGERE
*/
    public static function  NotificationMessage(){

        $user = JWTAuth::parseToken()->authenticate();
        $idRicevente = $user->id;
        $role = $user->role;

        $messaggi = Messaggi::Select(DB::raw('count(*) as messaggi, idMittente'))
        ->where('idRicevente','=',$idRicevente)
        ->where('letto','=',0)
        ->groupBy('idMittente')
        ->get()->toArray();


        foreach($messaggi as $key=>$messaggio){
            $utente=Utenti::getAvatar($messaggio['idMittente']);
            $messaggi[$key]['utente']= $utente['0'];
            /* if($role == 'medico'){
                $messaggi[$key]['utente'] = PazientiMeta::getDatiPaziente($messaggio['idMittente']);
             }

             if($role == 'paziente'){
                $messaggi[$key]['utente'] = MedciMeta::getDatiMedico($messaggio['idMittente']);
             }*/
        }


        return $messaggi;

    }

    /**
     * COUNTER CHAT DA LEGGERE
     * @return int|mixed
     */
    public static function countMessaggi(){

        $count = 0;

        $messaggi = self::NotificationMessage();

        $count = count($messaggi);

        /*
        if($messaggi){
            foreach($messaggi as $key=>$value){
                $count += $value['messaggi'];
            }
        }*/

        return $count;

    }


/*INSERISCI MESSAGGIO */
    public static function store($request){
        $user = JWTAuth::parseToken()->authenticate();
        $idMittente = $user->id;

        $role = $user->role;
        $idRicevente=$request->idRicevente;

        $messaggio = Messaggi::create(array(
                    "idMittente"=>$idMittente,
                    "idRicevente"=>$idRicevente,
                    "tipo"=>'text',
                    "messaggio"=>$request->messaggio,
                    "letto"=>0
                ));
                if($messaggio){
                    return true;
                }
                else{
                    return false;
                }
            }
    }
