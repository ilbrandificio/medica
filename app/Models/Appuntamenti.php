<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use JWTAuth;
use App\Models\PazientiMedici;

class Appuntamenti extends Model
{

    protected $table = 'appuntamenti';
    protected $primaryKey = 'idAppuntamento';
    protected $guarded = ['idAppuntamento'];


    public static function store($request){

        $validate = Appuntamenti::validate($request);

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        if($validate === true){
            $visita = Appuntamenti::create(array(
                'idPaziente' => $request->idPaziente,
                'idMedico' => $userId,
                'dataAppuntamento' => $request->dataAppuntamento,
                'ora_inizio' => $request->ora_inizio,
                'ora_fine' => $request->ora_fine,
                'descrizione'=> $request->descrizione
            ));

            if($visita){
                return true;
            }
            else{
               return false;
            }
        }
        else{
            return $validate;
        }
    }

    public static function getAppuntamentiPaziente($request){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;
        $idPaziente = $request->idPaziente;


        $datacorrente = date('Y-m-d');
        $dataInizio = $request->dataInizio;
        $dataFine = $request->dataFine;


        $appuntamenti = Appuntamenti::Select('idAppuntamento','appuntamenti.idPaziente','dataAppuntamento','ora_inizio','ora_fine','descrizione');

        //CHECK DATA
        if(!empty($dataInizio) && !empty($dataFine)){
            $appuntamenti = $appuntamenti->whereBetween('dataAppuntamento',[$dataInizio,$dataFine]);
        }

        if($dataInizio == '' || $dataFine == ''){
            $appuntamenti = $appuntamenti->where('dataAppuntamento','>=',$datacorrente);
        }

        //CONTROLLO SE ADMIN
        if($role== 'admin'){
            $appuntamenti = $appuntamenti->where('appuntamenti.idPaziente','=',$idPaziente);
        }
        //CONTROLLO SE MEDICO
        else if($role=='medico'){
            $appuntamenti = $appuntamenti->join('pazienti_medici','pazienti_medici.idPaziente','=','appuntamenti.idPaziente');
            $appuntamenti = $appuntamenti->where('pazienti_medici.idMedico','=',$userId);
            $appuntamenti = $appuntamenti->where('appuntamenti.idPaziente','=',$idPaziente);
        }
        //CONTROLLO SE PAZIENTE
        else if($role=='paziente'){
            $appuntamenti = $appuntamenti->where('appuntamenti.idPaziente','=',$userId);
        }

        $appuntamenti = $appuntamenti->join('users','appuntamenti.idPaziente','=','users.id')
        ->orderBy('dataAppuntamento','DESC')->limit(50)->get()->toArray();

        return $appuntamenti;
    }


    /** 
     * COUNT APPUNTAMENTI DATA CORRENTE
     * @return int
     */
    public static function getAppuntamentiDataCorrente(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        //$idPaziente = $request->idPaziente;

        $datacorrente = date('Y-m-d');
       
        $appuntamenti = Appuntamenti::Select('idAppuntamento','appuntamenti.idPaziente','dataAppuntamento','ora_inizio','ora_fine','descrizione');

        $appuntamenti = $appuntamenti->join('pazienti_medici','pazienti_medici.idPaziente','=','appuntamenti.idPaziente');
        $appuntamenti = $appuntamenti->where('dataAppuntamento','=',$datacorrente);
        $appuntamenti = $appuntamenti->where('pazienti_medici.idMedico','=',$userId);
        
        $appuntamenti = $appuntamenti->join('users','appuntamenti.idPaziente','=','users.id')
        ->orderBy('dataAppuntamento','DESC')->get()->toArray();

        return count($appuntamenti);
    }
    


    public static function getAppuntamenti($request){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;


        $appuntamenti = Appuntamenti::Select('idAppuntamento','dataAppuntamento','ora_inizio','ora_fine','descrizione');

        //CONTROLLO SE MEDICO
        if($role=='medico'){
            $appuntamenti = $appuntamenti->join('pazienti_medici','pazienti_medici.idPaziente','=','appuntamenti.idPaziente');
            $appuntamenti = $appuntamenti->where('pazienti_medici.idMedico','=',$userId);
        }
        //CONTROLLO SE PAZIENTE
        else if($role=='paziente'){
            $appuntamenti = $appuntamenti->where('idPaziente','=',$userId);
        }

        $appuntamenti = $appuntamenti->join('users','appuntamenti.idPaziente','=','users.id')
        ->orderBy('dataAppuntamento','DESC')->limit(50)->get()->toArray();

        return $appuntamenti;
    }


    public static function aggiorna($request){

        $idAppuntamento = $request->idAppuntamento;
        $validate = Appuntamenti::validate($request);

        if($validate === true){
            $visita = Appuntamenti::where('idAppuntamento','=',$idAppuntamento)->update(array(
                'dataAppuntamento' => $request->dataAppuntamento,
                'ora_inizio' => $request->ora_inizio,
                'ora_fine' => $request->ora_fine,
                'descrizione'=> $request->descrizione
            ));

            if($visita){
                return array('success'=>true);
            }
            else{
               return array('appuntamento'=> '', 'success'=>false);
            }
        }
        else{
            return array('appuntamento'=>$validate, 'success'=>false);
        }
    }

    public static function cancellaAppuntamento($idAppuntamento){


        $appuntamento = Appuntamenti::where('idAppuntamento','=',$idAppuntamento)->delete();

        if($appuntamento){
            return true;
        }
        else{
            return false;
        }


    }

    /**
     * GET APPUNTAMENTI MEDICO PER VISUALIZZAZIONE CALENDARIO
     * @return array
     */
    public static function getAppuntamentiMedico(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $appuntamenti = Appuntamenti::select('name','idAppuntamento','idMedico','idPaziente','ora_fine','ora_inizio','descrizione','dataAppuntamento')
        ->where('idMedico','=',$userId)
        ->join('users','users.id','=','appuntamenti.idPaziente')
        ->where('role','=','paziente')
        ->get()->toArray();

        return $appuntamenti;

    }

    public static function validate($request){

        //$idPaziente=$request->idPaziente;
        $dataAppuntamento=$request->dataAppuntamento;
        $ora_inizio=$request->ora_inizio;
        $ora_fine = $request->ora_fine;
        $message='';


        if(!v::notEmpty()->validate($dataAppuntamento)){
            $message = 'Inserisci Data Appuntamento';
        }
        else if(!v::notEmpty()->validate($ora_inizio)){
            $message = 'Inserisci Ora inizio';
        }
        else if(!v::notEmpty()->validate($ora_fine)){
            $message = 'Inserisci Ora Fine';
        }



        if($message!=''){
            return $message;
        }
        else{
            return true;
        }

    }
}
