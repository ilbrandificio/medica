<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UtentiMeta;
use Respect\Validation\Validator as v;
use JWTAuth;

class PrescrizioniEsami extends Model
{

    protected $table = 'prescrizioni_esami';
    protected $primaryKey = 'idPrescrizioneEsame';
    protected $guarded = ['idPrescrizioneEsame'];

    /** STORE PRESCRIZIONE */
    public static function store($request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $validate = PrescrizioniEsami::validate($request);

        if($validate === true){

           //$esami = PrescrizioniEsami::generateArrayEsami($request);

           $prescrizione =  PrescrizioniEsami::create(array(
                'idMedico' => $userId,
                'idPaziente'=> $request->idPaziente,
                'esami'=> serialize($request->esami),
                'data_scadenza'=>$request->data_scadenza
            ));

            if($prescrizione){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

    /** GENERATE ARRAY Esami */
    public static function generateArrayEsami($request){

        $esami = [];
        $numero_esami = $request->numero_esami;

        for($i =0; $i<$numero_esami; $i++){

            $esame = array(
                'esame'=> $request['esame_'.$i],
                'dettagli_esame'=> $request['dettagli_esame_'.$i],
                'stato'=> 'attesa'
            );

            array_push($esami,$esame);

        }


        return $esami;

    }

    /** AGGIORNA PRESCRIZIONE */
    public static function aggiorna($request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $idPrescrizioneEsame = $request->idPrescrizioneEsame;

        $validate = PrescrizioniEsami::validate($request);

        if($validate === true){

           $esami = PrescrizioniEsami::generateArrayEsami($request);

           $prescrizione =  PrescrizioniEsami::where('idPrescrizioneEsame','=',$idPrescrizioneEsame)->update(array(
                'esami'=> serialize($esami),
            ));

            if($prescrizione){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

    /** STAMPA PRESCRIZIONE */
    public static function stampa($idPrescrizioneEsame){

        $prescrizione = PrescrizioniEsami::Select('idMedico','idPaziente','esami','created_at')
        ->where('idPrescrizioneEsame','=',$idPrescrizioneEsame)
        ->get()->toArray();

        $prescrizione['0']['esami'] = unserialize($prescrizione['0']['esami']);

        $prescrizione['0']['medico'] = UtentiMeta::getMetaInfo($prescrizione['0']['idMedico']);
        $prescrizione['0']['paziente'] = UtentiMeta::getMetaInfo($prescrizione['0']['idPaziente']);

        return $prescrizione;
        
    }

    /** GET PRESCRIZIONI BY ID PAZIENTE*/
    public static function getPrescrizioniEsamiByIdPaziente($request){

        $idPaziente = $request->idPaziente;

        $prescrizioni = PrescrizioniEsami::Select('idPrescrizioneEsame','idPaziente','name','prescrizioni_esami.created_at')
        ->where('idPaziente','=',$idPaziente)
        ->join('users','users.id','=','prescrizioni_esami.idMedico')
        ->get()->toArray();

        return $prescrizioni;

    }


    /**
     * @param $idPrescrizioneEsame
     * @return array
     */
    public static function getEsamiPromemoriaByIdPrescrizione($idPrescrizioneEsame){

        $prescrizione = PrescrizioniEsami::Select('esami')
        ->where('idPrescrizioneEsame','=',$idPrescrizioneEsame)
        ->get()->toArray();

        if($prescrizione){
            $prescrizione['0']['esami'] = unserialize($prescrizione['0']['esami']);
            return $prescrizione['0']['esami'];
        }
        else{
            return [];
        }

    }


    /**
     * CALCOLA PERCENTUALE
     * @param $idPrescrizioneEsame
     * @return float|int
     */
    public static function calcolaPercentuale($idPrescrizioneEsame){

        $esami = self::getEsamiPromemoriaByIdPrescrizione($idPrescrizioneEsame);
        $esami_totali = count($esami['esami_laboratorio']) + count($esami['esami_strumentali']);
        $esami_registrati = 0;
        $percentuale = 0;

        if($esami){
            
            foreach($esami['esami_laboratorio'] as $key=>$value){
                if($value['stato'] == 'registrato'){
                    $esami_registrati++;
                }
            }

            foreach($esami['esami_strumentali'] as $key=>$value){
                if($value['stato'] == 'registrato'){
                    $esami_registrati++;
                } 
            }

            $percentuale = ($esami_registrati/$esami_totali) * 100;
        }

        return $percentuale;
    }


    /** 
     * AGGIORNA STATO ESAME
     * @param $idPrescrizioneEsame
     * @param $codice_esame
     * @param $tipo_esame
     * @return bool
     */
    public static function aggiornaStatoEsame($idPrescrizioneEsame,$codice_esame,$tipo_esame){

        $esami = self::getEsamiPromemoriaByIdPrescrizione($idPrescrizioneEsame);
        
        if($tipo_esame == 'esame'){
            $esami['esami_laboratorio'][$codice_esame]['stato'] = 'registrato';
        }

        if($tipo_esame == 'esame_strumentale'){
            $esami['esami_strumentali'][$codice_esame]['stato'] = 'registrato';
        }
       
    
        $prescrizione = self::where('idPrescrizioneEsame','=',$idPrescrizioneEsame)->update(array(
            'esami'=> serialize($esami),
        ));

        if($prescrizione){
            $percentuale = self::aggiornaPercentuale($idPrescrizioneEsame);

            if($percentuale){
                return true;
            }
        
        }
        else{
            return false;
        }

    }


    /** AGGIORNA PERCENTUALE */
    public static  function  aggiornaPercentuale($idPrescrizioneEsame){

        $perc = self::calcolaPercentuale($idPrescrizioneEsame);

        $prescrizione = self::where('idPrescrizioneEsame','=',$idPrescrizioneEsame)->update(array(
            'perc'=> $perc
        ));


        if($prescrizione){
            return true;
        }
        else{
            return false;
        }
    }


    /**
     * GET PRESCRIZIONI ESAMI PROMEMORIA
     * @param $idPaziente
     * @return mixed
     */
    public static function getPrescrizioneEsamiPromemoria($idPaziente){

        $data_corrente = date('Y-m-d');

        $prescrizioni = self::Select('idPrescrizioneEsame','data_scadenza','perc')
        ->where('idPaziente','=',$idPaziente)
        ->where('data_scadenza','>=',$data_corrente)
        ->get()->toArray();

        return $prescrizioni;

    }


    /** GET PRESCRIZIONE BY ID PRESCRIZIONE ESAME */
    public static function getPrescrizioneByIdPrescrizioneEsame($request){

        $idPrescrizioneEsame = $request->idPrescrizioneEsame;

        $prescrizione = PrescrizioniEsami::Select('esami','data_scadenza')
        ->where('idPrescrizioneEsame','=',$idPrescrizioneEsame)
        ->get()->toArray();

        $prescrizione['0']['esami'] = unserialize($prescrizione['0']['esami']);

        return $prescrizione['0'];

    }

    /** GET PRESCRIZIONI ESAMI SACDUTE */
    public static function getPrescrzioniEsamiScaduti($giorni){

        if($giorni==null){
            $giorni=10;
        }

        $current_date = date('Y-m-d');
        $subtractday = strtotime('-'.$giorni.' days', strtotime($current_date));
        $final_date = date('Y-m-d',$subtractday);

        $prescrizione = self::Select('idPaziente','name','esami','data_scadenza')
            ->join('users','users.id','=','prescrizioni_esami.idPaziente')
            ->whereBetween('data_scadenza',[$final_date,$current_date])
            ->where('perc','!=',100.00)
            ->get()->toArray();

        foreach($prescrizione as $key=>$value){
            $prescrizione[$key]['esami'] = unserialize($value['esami']);
        }

        return $prescrizione;
    }

    /** GET PRESCRIZIONI ESAMI IN SCADENZA */
    public static function getPrescrizioniEsamiAttesa($giorni){

        if($giorni==null){
            $giorni=10;
        }

        $current_date = date('Y-m-d');
        $addday = strtotime('+'.$giorni.' days', strtotime($current_date));
        $final_date = date('Y-m-d',$addday);

        $prescrizione = self::Select('idPaziente','name','esami','data_scadenza')
            ->join('users','users.id','=','prescrizioni_esami.idPaziente')
            ->whereBetween('data_scadenza',[$current_date,$final_date])
            ->where('perc','!=',100.00)
            ->get()->toArray();

        foreach($prescrizione as $key=>$value){
            $prescrizione[$key]['esami'] = unserialize($value['esami']);
        }

        return $prescrizione;

    }


    /** CANCELLA PRESCRIZIONE  */
    public static function cancella($idPrescrizioneEsame){

        $prescrizione = PrescrizioniEsami::where('idPrescrizioneEsame','=',$idPrescrizioneEsame)
        ->delete();

        if($prescrizione){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * @param $data_scadenza
     * @return bool
     */
    public static function checkDate($data_scadenza){
        $current_date = date('Y-m-d');

        if($data_scadenza >= $current_date ){
            return true;
        }
        else{
            return false;
        }
    }


    /**
     * @param $request
     * @return bool|string
     */
    public static function validate($request){

        $message = '';
        $data_scadenza = $request->data_scadenza;


         if(!v::notEmpty()->validate($data_scadenza) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $message = 'Inserisci Data Scadenza';
        }
        else if (!self::checkDate($data_scadenza) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $message = 'Inserisci una data uguale o futura';
        }
       
        if($message != ''){
            return $message;
        }
        else{
            return true;
        }
    }


}
