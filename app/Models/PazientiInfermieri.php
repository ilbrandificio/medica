<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use JWTAuth;

class PazientiInfermieri extends Model
{
    protected $table = 'pazienti_infermieri';
    protected $primaryKey = 'idPazienteInfermiere';
    protected $guarded = ['idPazienteInfermiere'];

/*
    STORE RICHIESTA INFERMIERE
*/
public static function assegnaInfermiere($idInfermiere){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;

        $validate = PazientiInfermieri::validate($idInfermiere);

        if($validate === true){
            $utente = PazientiInfermieri::create(array(
                'idPaziente'=>$userId,
                'idInfermiere'=>$idInfermiere,
            ));

            if($utente){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }
/*
    CONTROLLO SE IL MEDICO HA ACCESSO AL PAZIENTE
*/
public static function checkAccessoPaziente($idPaziente){
    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role=$user->role;

    //SE INFERMIERE CONTROLLO CHE ABBIA ACCESSO A QUEL PAZIENTE
        if($role=='infermiere'){
            $paziente = PazientiInfermieri::Select('idPaziente')
            ->where('idInfermiere','=',$userId)
            ->where('idPaziente','=',$idPaziente)
            ->get()->toArray();
        //SE IL MEDICO HA ACCESSO
            if($paziente){
                return true;
            }
            else{
                return false;
            }
        }
    //SE ADMIN ACCESSO A TUTTO
        else if($role=='admin'){
            return true;
        }
        else{
            return false;
        }
    }

    public static function getPazienteInfermieri($idPaziente){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;

        $infermieri =  PazientiInfermieri::Select('pazienti_infermieri.idPazienteInfermiere','pazienti_infermieri.idInfermiere','idPaziente','name','pazienti_infermieri.stato')
        ->where('idPaziente','=',$idPaziente)
        ->join('users','users.id','=','pazienti_infermieri.idInfermiere')->get()->toArray();

        return $infermieri;
    }


    /** RECUPERO I PAZIENTI DI UN INFERMIERE  */
    public static function getRichieste(){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;

        $richieste = PazientiInfermieri::Select('pazienti_infermieri.idPazienteInfermiere','name','pazienti_infermieri.stato')
        ->where('pazienti_infermieri.idInfermiere', '=', $userId)
        ->join('users','users.id','=','pazienti_infermieri.idPaziente')
        ->get()->toArray();

        return $richieste;

    }


/*RECUPERO I PAZIENTI DI UN INFERMIERE*/
    public static function getPazientiInfermieri($request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;

        $keyword = $request->keyword;
        $pazienti = PazientiInfermieri::Select('idUser','nome','cognome','url_media')
        ->where('idInfermiere','=',$userId)
        ->where('pazienti_infermieri.stato','=',1);

        if(!empty($keyword)){
        $pazienti=$pazienti->where(function($query) use ($keyword) {
            $query->where('nome','like','%'.$keyword.'%')
                ->orWhere('cognome','like','%'.$keyword.'%');
            });
        }

        $pazienti=$pazienti->leftjoin('pazienti_meta','pazienti_infermieri.idPaziente','=','pazienti_meta.idUser')
        ->leftjoin('users','users.id','=','pazienti_meta.idUser');
        $pazienti=$pazienti->leftjoin('media','media.idMedia','=','users.idMedia');

        $pazienti = $pazienti->get()->toArray();


        return $pazienti;
    }

/** ACCETTA RICHIESTA */
    public static function accettaRichiesta($request){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;

        $richiesta = PazientiInfermieri::where('idPazienteInfermiere','=',$request->idRichiesta)
        ->where('idInfermiere','=',$userId)
        ->update(array(
            'stato'=>1,
            ));

            if($richiesta){
                return true;
            }
            else{
                return false;
            }
    }

    /** CANCELLA MEDICO ASS. */
    public static function cancellaMedicoAss($idPaziente,$idPazienteMedico){

        if(count(PazientiInfermieri::getPazienteInfermieri($idPaziente)) == 1){
            return array('success'=>false,'message'=>'Impossibile Cancellare');
        }
        else{
            $medicoass = PazientiInfermieri::where('idPazienteMedico','=',$idPazienteMedico)->delete();

            if($medicoass){
                return array('success'=>true);
            }
            else{
                return array('success'=>false,'message'=>'Errore Cancellazione');
            }

        }

    }

    /** GET PAZIENTI INFERMIERE ADMIN 
     * @param $idInfermiere
     * @return array
    */
    public static function getPazientiInfermiere($idInfermiere){

        $pazientimedico = self::Select('name','idPaziente','idInfermiere','pazienti_infermieri.stato')->where('idInfermiere','=',$idInfermiere)
        ->join('users','users.id','=','pazienti_infermieri.idPaziente')->get()->toArray();

        foreach($pazientimedico as $key=>$paziente){
            $pazientimedico[$key]['meta'] = UtentiMeta::getMetaInfo($paziente['idPaziente']);
        }

        return $pazientimedico;

    }

    /** CANCELLA INFERMIERE ASS (PROFILO)*/
    public static function cancellaInfermiereAssProfilo($userId,$role){

        if($role == 'infermiere'){
            $infermiere = PazientiInfermieri::where('idInfermiere','=',$userId)->delete();
        }

        if($role == 'paziente'){
            $infermiere = PazientiInfermieri::where('idPaziente','=',$userId)->delete();
        }

        if($infermiere){
            return true;
        }
        else{
            return false;
        }

    }   

/*
 CHECK PAZIENTI ASSEGNATI
*/

public static function checkPazientiInfermieri($request){
    $idPaziente=$request->idPaziente;
    $user = JWTAuth::parseToken()->authenticate();
    $role = $user->role;
    $userId= $user->id;

    if(PazientiInfermieri::checkPaziente($idPaziente,$userId)){
        return true;
    }
    else{
        return false;
    }
 }

/*
    CHECK MEDICI ASSEGNATI
*/

public static function checkInfermieriPazienti($request){
    $idInfermiere=$request->idInfermiere;
    $user = JWTAuth::parseToken()->authenticate();
    $role = $user->role;
    $userId= $user->id;

    if(PazientiInfermieri::checkPaziente($userId,$idInfermiere)){
        return true;
    }
    else{
        return false;
    }
 }

/*
    CONTROLLO CHE IL PAZIENTE SIA ASSEGNATO
*/
    public static function checkPaziente($idPaziente,$idInfermiere){
      $pazienti = PazientiInfermieri::Select('idPaziente','idInfermiere')
      ->where('idPaziente','=',$idPaziente)
      ->where('idInfermiere','=',$idInfermiere)
      ->get()->toArray();

      if($pazienti){
         return true;
      }
      else{
          return false;
      }
   }


    public static function checkMedicoAss($idInfermiere){

        $user = JWTAuth::parseToken()->authenticate();
        $role=$user->role;

        if($role == 'paziente'){
            $userId = $user->id;
        }

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $infermieri = PazientiInfermieri::Select('idInfermiere')->where('idInfermiere','=',$idInfermiere)->
            where('idPaziente','=',$userId)->
            get()->toArray();

            return $infermieri;
        }


    }

    public static function validate($idInfermiere){

        $message = '';


        if(count(PazientiInfermieri::checkMedicoAss($idInfermiere))>=1){
            $message = 'Infermiere già assegnato';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }
}
