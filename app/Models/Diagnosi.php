<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use Respect\Validation\Validator as v;

class Diagnosi extends Model
{

    protected $table = 'pazienti_diagnosi';
    protected $primaryKey = 'idDiagnosi';
    protected $guarded = ['idDiagnosi'];

    public static function store($request){

        $validate = Diagnosi::validate($request);

        if($validate === true){
          $diagnosi = Diagnosi::create(array(
                "idPaziente"=>$request->idPaziente,
                "dataDiagnosi"=> $request->dataDiagnosi,
                "codice"=> $request->codice
           ));


           if($diagnosi){
            return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }


    /* GET DIANGOSI PAZIENTE */
    public static function getDiagnosiPaziente($request){
        $idPaziente=$request->idPaziente;

        $diagnosi = Diagnosi::Select('idDiagnosi','diagnosi.codice','diagnosi.descrizione','dataDiagnosi');
        $diagnosi = $diagnosi->join('diagnosi','diagnosi.codice','=','pazienti_diagnosi.codice');
        $diagnosi = $diagnosi->where('idPaziente','=',$idPaziente);



        $diagnosi = $diagnosi->join('users','pazienti_diagnosi.idPaziente','=','users.id')
        ->orderBy('dataDiagnosi','DESC')->limit(50)->get()->toArray();

        return $diagnosi;
    }

    /* CANCELLA */
    public static function cancella($idDiagnosi){
        $diagnosi = Diagnosi::where('idDiagnosi','=',$idDiagnosi)->delete();

        if($diagnosi){
            return true;
        }
        else{
            return false;
        }

        return $diagnosi;
    }



    /*
    public function aggiorna($request){
        $validate=Diagnosi::validate($request);
        $idDiagnosi = $request->idDiagnosi;

        if($validate===true){
            $diagnosi = Esami::where('idDiagnosi','=',$idDiagnosi)->update(array(

            ));
            if($diagnosi){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }
    }
    */

    public static function validate($request){
        return true;
    }
}
