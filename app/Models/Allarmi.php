<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use JWTAuth;
use function Symfony\Component\String\s;


class Allarmi extends Model
{
    protected $table = 'allarmi';
    protected $primaryKey = 'idAllarme';
    protected $guarded = ['idAllarme'];

    //TODO: MULTIPLE CHIAVI PER ULTIMI ALLARMI
    //TODO: TRIGGER PER ALLARMI CLINICI

    //TODO: CHECK ACCESSI MEDICO -> PAZIENTE


    public static  function store($request){

        $validate=Allarmi::validate($request);

        if($validate===true){

            $allarme = Allarmi::create(array(
                'idPaziente' => $request->idPaziente,
                'codiceEsame'=>$request->codiceEsame,
                'chiave'=>$request->chiave,
                'valore_min'=>$request->valoreMin,
                'valore_max'=>$request->valoreMax,
                'stato' => 'pendente',
                'ripetizioni'=> $request->ripetizioni
            ));

            if($allarme){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }
    }



    public static function aggiorna($request){

        $idIntervento = $request->idIntervento;
        $validate = Allarmi::validate($request);

        if($validate === true){
            $allarme = Allarmi::where('idIntervento','=',$idIntervento)->update(array(
                'dataIntervento'=>$request->dataIntervento,
                'nomeIntervento' => $request->nomeIntervento,
                'noteIntervento'=>$request->noteIntervento
            ));

            if($allarme){
                return true;
            }
            else{
                return false;
            }
        }
        else{
          return false;
        }
    }


/*RECUPERO ALLARMI PER PAZIENTE*/
public static function getAllarmi($request){
        $idPaziente=$request->idPaziente;

        $allarmi = Allarmi::Select('allarmi.idPaziente','idAllarme','codiceEsame','chiave','valore_min','valore_max','valore_registrato','stato','ripetizioni')
        ->where('idPaziente',$idPaziente)
        ->get()->toArray();

        return $allarmi;
    }


/*RECUPERO ALLARMI PER PAZIENTE*/
public static function getAllarmiAttiviByEsame($request){

    $idPaziente=$request->idPaziente;
    $esame=$request->esame;

    $allarmi = Allarmi::Select('allarmi.idPaziente','idAllarme','codiceEsame','chiave','valore_min','valore_max','valore_registrato','stato','ripetizioni')
    ->where('idPaziente',$idPaziente)
    ->where('codiceEsame',$esame)
    ->get()->toArray();

    return $allarmi;
}

/*RECUPERO ALLARMI SCATTATI PER PAZIENTE*/
public static function getAllarmiAttiviMedico($request){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role=$user->role;

    $pazienti=PazientiMedici::getPazientiMedici($request);

    $idPazienti=[];

    foreach($pazienti as $key=>$paziente){
        array_push($idPazienti,$paziente['idUser']);
    }

    $allarmi = Allarmi::Select('allarmi.idPaziente','idAllarme','name','codiceEsame','chiave','valore_min','valore_max','valore_registrato','allarmi.stato','url_media','ripetizioni','allarmi.updated_at')
    ->where('allarmi.stato','=','attivo')
    ->whereIn('idPaziente',$idPazienti)
    ->join('users','users.id','=','allarmi.idPaziente')
    ->leftjoin('media','media.idMedia','=','users.idMedia')
    ->get()->toArray();

    return $allarmi;
}

/*RECUPERO ALLARMI PER PAZIENTE*/
    public static function getAllarmiAttivi($idPaziente){
        $allarmi = Allarmi::Select()->where('idPaziente','=',$idPaziente)
        ->where('stato','=','attivo')
        ->get()->toArray();

        return $allarmi;
    }

/*CANCELLAZIONE ALLARME*/
    public static function cancella($idAllarme){

        $allarme = Allarmi::where('idAllarme','=',$idAllarme)->delete();

        if($allarme){
            return true;
        }
        else{
            return false;
        }

    }


public static function validate($request){
        return true;
    }


/*CONTOLLO ALLARMI*/
public static function checkAllarmi($request){

    $esame=$request->esame;
    $idPaziente=$request->idPaziente;
    $esame=(array) $esame;

    foreach($esame as $key=>$valore){

        $allarme=Allarmi::getAllarmebyChiave($key,$idPaziente);

        if($allarme){
            if( ($valore !== null) && ($valore<$allarme['valore_min']) || ($valore>$allarme['valore_max'])){
                Allarmi::triggerAllarme($allarme['idAllarme'],$valore);
            }
        }
    }

    return true;

}

/*CONTROLLO ALLARMI CLINICI*/
public static function checkAllarmiClinici($daticlinici,$idPaziente){

    $daticlinici=(array) $daticlinici;

    foreach($daticlinici as $key=>$valore){

        $allarme=Allarmi::getAllarmebyChiave($key,$idPaziente);

        if($allarme){
            if(($valore !== null) && ($valore<$allarme['valore_min']) || ($valore>$allarme['valore_max'])){
                Allarmi::triggerAllarme($allarme['idAllarme'],$valore);
            }
        }
    }

    return true;

}


/*RECUPERO ALLARMI PER PAZIENTE*/
public static function getAllarmebyChiave($chiave,$idPaziente){

    $allarme = Allarmi::Select()->where('chiave','=',$chiave)
    ->where('idPaziente','=',$idPaziente)->limit(1)
    ->get()->toArray();

    if($allarme){
        return $allarme[0];
    }
    else{
        return false;
    }
}

public static function triggerAllarme($idAllarme,$valore){

        $allarme = Allarmi::where('idAllarme','=',$idAllarme)->update(array(
            'stato'=>'attivo',
            'valore_registrato'=>$valore
        ));

        return true;
}


    /**
     * COUNT ALLARMI ATTIVI MEDICO
     * @param $request
     * @return int
     */
    public static function countAllarmiAttivi($request){

        $count = 0;

        $allarmi = self::getAllarmiAttiviMedico($request);

        $count = count($allarmi);

        return $count;

    }


    /**
     * GET ALLARME BY ID
     * @param $idAllarme
     * @return mixed
     */
     public static function getAllarmeById($idAllarme){

        $allarme = self::where('idAllarme','=',$idAllarme)->get()->toArray();


        return $allarme['0'];

     }

    /**
     * SEGNA COME LETTO
     * @param $idPaziente
     * @param $idAllarme
     * @return array
     */
     public static function segnaComeLetto($idPaziente,$idAllarme){

         $allarme = self::getAllarmeById($idAllarme);

         if($allarme['ripetizioni'] === 1){
             $cancella = self::cancella($idAllarme);

             if($cancella){
                 return array('success'=> true,'message'=>'');
             }
             else{
                 return array('success'=> false,'message'=>'Errore Cancellazione');
             }

         }
         else{
             $aggiorna = self::aggiornaStato($idAllarme);

             if($aggiorna){
                 return array('success'=> true ,'message'=>'');
             }
             else{
                 return array('success'=> false ,'message'=>'Errore Aggiornamento');
             }
         }

     }


    /**
     * AGGIORNA STATO
     * @param $idAllarme
     * @return bool
     */
     public static function aggiornaStato($idAllarme){

         $allarme = self::where('idAllarme','=',$idAllarme)->update(array(
             'stato'=> 'pendente'
         ));

         if($allarme){
             return true;
         }
         else{
             return false;
         }

     }

}
