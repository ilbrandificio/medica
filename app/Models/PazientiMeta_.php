<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use Carbon\Carbon;
use App\Models\Terapie;
use App\Models\Esami;
use JWTAuth;


class PazientiMeta extends Model
{
    protected $table = 'pazienti_meta';
    protected $primaryKey = 'idPazienteMeta';
    protected $guarded = ['idPazienteMeta'];
    protected $hidden = ['password','remember_token','created_at','email_verified_at','last_login','role'];
/*
    STORE META DI UN UTENTE
*/
public static function store($request,$idUtente){

    $utente = PazientiMeta::create(array(
        'idPazienteMeta'=>$idUtente,
        'idUser'=>$idUtente,
        'nome'=>$request->nome,
        'cognome'=>$request->cognome,
        'telefono'=>$request->telefono,
        'indirizzo'=>$request->indirizzo,
        'data_nascita'=>$request->data_nascita,
        'provincia_nascita'=>$request->provincia_nascita,
        'comune_nascita'=>$request->comune_nascita,
        'codice_fiscale'=>$request->codice_fiscale,
        'provincia_residenza'=>$request->provincia_residenza,
        'comune_residenza'=>$request->comune_residenza,
        'provincia_asl'=>$request->provincia_asl,
        'comune_asl'=>$request->comune_asl,
        'sesso'=>$request->sesso,
       /* 'superficie_corporea'=>$request->superficie_corporea,
        'peso'=>$request->peso,
        'altezza'=>$request->altezza,
        'gruppo'=>$request->gruppo,
        'epicrisi'=>$request->epicrisi,
        'problematiche'=>$request->problematiche,
        'diabete'=>$request->diabete,
        'fumo'=>$request->fumo,
        'anni_diabete'=>$request->anni_diabete,
        'ipertensione_arteriosa'=>$request->anni_diabete,
        'anni_ipertensione'=>$request->anni_diabete,
        'anemia'=> $request->anemia,
        'fibrillazioneatriale'=> $request->fibrillazioneatriale,
        'terapiaottimale'=> $request->terapiaottimale,
        'diagnosi_discorsiva'=> $request->diagnosi_discorsiva,
        'familiarita'=> serialize($request->familiarita)*/
    ));

    return true;
}





/** AGGIORNA PROFILO */
public static function aggiorna($request){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;

    $utente =PazientiMeta::where('idUser','=',$userId)->update(array(
        'nome'=>$request->nome,
        'cognome'=>$request->cognome,
        'telefono'=>$request->telefono,
        'indirizzo'=>$request->indirizzo,
        'data_nascita'=>$request->data_nascita,
        'provincia_nascita'=>$request->provincia_nascita,
        'comune_nascita'=>$request->comune_nascita,
        'codice_fiscale'=>$request->codice_fiscale,
        'provincia_residenza'=>$request->provincia_residenza,
        'comune_residenza'=>$request->comune_residenza,
        'provincia_asl'=>$request->provincia_asl,
        'comune_asl'=>$request->comune_asl,
    ));

    if($utente){
        return true;

    }
    else{
        return false;
    }

}

/* FILTRA PAZIENTI */
public static function getPazienti($request){
    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role = $user->role;

    $keyword = $request->keyword;
    $pazienti = PazientiMeta::Select('idUser','nome','cognome','url_media');

    if(!empty($keyword)){
    $pazienti=$pazienti->where(function($query) use ($keyword) {
        $query->where('nome','like','%'.$keyword.'%')
            ->orWhere('cognome','like','%'.$keyword.'%');
        });
    }

    $pazienti=$pazienti->leftjoin('media','media.idMedia','=','users.idMedia');

    if($role == 'medico'){
        $pazienti=$pazienti->leftjoin('pazienti_medici','pazienti_medici.idPaziente','=','pazienti_meta.idUser')->
        where('pazienti_medici.idMedico','=',$userId);
    }


    $pazienti = $pazienti->get()->toArray();


    return $pazienti;
    }






 /** GET IMAGE */
    public static function getImagePaziente($idUser){

        $media = PazientiMeta::Select('users.idMedia','url_media')->where('idUser','=',$idUser)
        ->leftjoin('users','users.id','=',$idUser)
        ->leftjoin('media','media.idMedia','=','users.idMedia')
        ->limit(1)->get()->toArray();

        return $media;
    }

 /** UPDATE IMAGE */
   public static function updateImage($idUser,$idMedia){

    $avatarpaziente = PazientiMeta::where('idUser','=',$idUser)->update(array(
        'idMedia'=> $idMedia
    ));

    if($avatarpaziente){
        return true;
    }
    else{
        return false;
    }

  }


  /** GET DATI PAZIENTE PER I MESSAGGI */
    public static function getDatiPaziente($idMittente){

        $paziente = PazientiMeta::Select('nome','cognome','url_media','idUser')
        ->where('idUser','=',$idMittente)
        ->leftjoin('users','users.id','=','pazienti_meta.idUser')
        ->leftjoin('media','media.idMedia','=','users.idMedia')
        ->get()->toArray();

        return $paziente[0];

    }

}
