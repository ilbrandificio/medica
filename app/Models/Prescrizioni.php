<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UtentiMeta;
use JWTAuth;

class Prescrizioni extends Model
{

    protected $table = 'prescrizioni';
    protected $primaryKey = 'idPrescrizione';
    protected $guarded = ['idPrescrizione'];

    /** STORE PRESCRIZIONE */
    public static function store($request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $code = self::generateUniqueCode();
      
        $validate = Prescrizioni::validate($request);

        if($validate === true){
 
           $farmaci = Prescrizioni::generateArrayFarmaci($request);

           $prescrizione =  Prescrizioni::create(array(
                'idMedico' => $userId,
                'idPaziente'=> $request->idPaziente,
                'farmaci'=> serialize($farmaci),
                'codice'=> $code
            ));

            if($prescrizione){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }


    /**
     * GENERATE UNIQUE CODE 6 DIGIT
     * @return string
     */
    public static function generateUniqueCode(){
        
        $code = random_int(100000, 999999);

        return (string) $code;
    }


    /**  
     * CHECK CODICE
     * @param $idPrescrizione
     * @param $codice
     * @return bool
    */
    public static function checkCodice($idPrescrizione,$codice){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $prescrizione =  self::where('idPrescrizione','=',$idPrescrizione)
        ->where('idPaziente','=',$userId)
        ->where('codice', '=', $codice)->get()->toArray();

        if(count($prescrizione) == 1){
            return true;
        }
        else{
            return false;
        }

    }

     /**
     * GET PRESCRIZIONE UTENTE CORRENTE 
     * @param $idPrescrizione
     * @return array
     */
    public static function getPrescrizioneUtenteCorrente($idPrescrizione){
        
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $prescrizione = self::Select('codice','name')
        ->join('users','users.id','=','prescrizioni.idPaziente')
        ->where('idPrescrizione','=',$idPrescrizione)
        ->where('idPaziente','=',$userId)->limit(1)->get()->toArray();
  
        return $prescrizione[0];

    }


    /**
     * AGGIORNA STATO PRESCRIZIONE 
     * @param $idPrescrizione
     * @return bool 
    */
    public static function aggiornaStatoPrescrizione($idPrescrizione){

        $prescrizione = self::where('idPrescrizione','=',$idPrescrizione)
        ->update(array(
            'stato'=> 1
        ));

        if($prescrizione){
            return true;
        }
        else{
            return false;
        }
        
    }
    


    /** GENERATE ARRAY FARMACI */
    public static function generateArrayFarmaci($request){
      
        $farmaci = [];
        $numero_farmaci = $request->numero_farmaci;

        for($i =0; $i<$numero_farmaci; $i++){

            $farmaco = array(
                'dosaggio'=> $request['dosaggio_'.$i],
                'durata_trattamento'=> $request['durata_trattamento_'.$i],
                'farmaco'=> $request['farmaco_'.$i],
                'numero_orario_somministrazione'=> $request['numero_orario_somministrazione_'.$i],
                'somministrazione'=> $request['somministrazione_'.$i],
                'ssn'=> $request['ssn_'.$i]
            );

            array_push($farmaci,$farmaco);
           
        }

       
        return $farmaci;

    }

    /** AGGIORNA PRESCRIZIONE */
    public static function aggiorna($request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $idPrescrizione = $request->idPrescrizione;
    
        $validate = Prescrizioni::validate($request);

        if($validate === true){
 
           $farmaci = Prescrizioni::generateArrayFarmaci($request);

           $prescrizione =  Prescrizioni::where('idPrescrizione','=',$idPrescrizione)->update(array(
                'farmaci'=> serialize($farmaci)
            ));

            if($prescrizione){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

    /** STAMPA PRESCRIZIONE */
    public static function stampa($idPrescrizione){
    
        $prescrizione = Prescrizioni::Select('idMedico','idPaziente','farmaci','created_at')
        ->where('idPrescrizione','=',$idPrescrizione)
        ->get()->toArray();

        $prescrizione['0']['farmaci'] = unserialize($prescrizione['0']['farmaci']);
        
        $prescrizione['0']['medico'] = UtentiMeta::getMetaInfo($prescrizione['0']['idMedico']);
        $prescrizione['0']['paziente'] = UtentiMeta::getMetaInfo($prescrizione['0']['idPaziente']);

        return $prescrizione;

    }

    /** GET PRESCRIZIONI BY ID PAZIENTE*/
    public static function getPrescrizioniByIdPaziente($request){

        $idPaziente = $request->idPaziente;

        $prescrizioni = Prescrizioni::Select('idPrescrizione','name','prescrizioni.created_at','prescrizioni.stato')
        ->where('idPaziente','=',$idPaziente)
        ->join('users','users.id','=','prescrizioni.idMedico')
        ->get()->toArray();

        return $prescrizioni;

    }

    /** GET PRESCRIZIONE BY ID PRESCRIZIONE */
    public static function getPrescrizioneByIdPrescrizione($request){

        $idPrescrizione = $request->idPrescrizione;

        $prescrizione = Prescrizioni::Select('farmaci')
        ->where('idPrescrizione','=',$idPrescrizione)
        ->get()->toArray();

        $prescrizione['0']['farmaci'] = unserialize($prescrizione['0']['farmaci']);

        return $prescrizione;

    }

    /** CANCELLA PRESCRIZIONE  */
    public static function cancella($idPrescrizione){

        $prescrizione = Prescrizioni::where('idPrescrizione','=',$idPrescrizione)
        ->delete();

        if($prescrizione){
            return true;
        }
        else{
            return false;
        }

    }


    public static function validate($request){

        $message = '';
        $numero_farmaci = $request->numero_farmaci;

        if($numero_farmaci == 0){
            $message = 'Seleziona almeno un farmaco';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }
    }


}
