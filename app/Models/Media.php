<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Str;
use Intervention\Image\ImageManager as Image;
use App\Models\PazientiMeta;
use App\Models\MediciMeta;
use JWTAuth;
use File;


class Media extends Model
{
    protected $table = 'media';
    protected $primaryKey = 'idMedia';
    protected $guarded = ['idMedia'];


    /** STORE IMAGE */

    /*
    public static function storeAvatar($type,$url_media){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;

        $lastIdMedia = '';
        $lastUrlMedia = '';

        $lastMedia = Media::checkUserImage($userId,$role);


        if($lastMedia){
            $lastIdMedia = $lastMedia['0']['idMedia'];
            $lastUrlMedia = $lastMedia['0']['url_media'];

            if($lastIdMedia != null){
                Media::cancellaImmagine($lastIdMedia,$lastUrlMedia);
            }
        }


        if($media){

            $idMedia = $media->idMedia;

            if($role == 'paziente'){
               $avatarpaziente = PazientiMeta::updateImage($userId,$idMedia);

               if($avatarpaziente){
                   return array('success'=>false,'url'=>$url_media);
               }
               else{
                return array('success'=>false,'message'=>'Errore Inserimento');
               }
            }
            else if($role == 'medico'){

                $avatarmedico = MediciMeta::updateImage($userId,$idMedia);

                if($avatarmedico){
                    return array('success'=>false,'url'=>$url_media);
                }
                else{
                    return array('success'=>false,'message'=>'Errore Inserimento');
                }
            }
        }
        else{
            return false;
        }

    }
    */


/*SALVATAGGIO IMMAGINE*/
    public static function salvaImmagine($request){

        $file = $request->get('img');
        $anno=date('Y');
        $mese=date('m');

        $storage_path=public_path('uploads/'.$anno.'/'.$mese.'/');
        if(!is_dir($storage_path)) {
            mkdir($storage_path, 0755, true);
        }

        //SE IL FILE ESISTE
        if($file)
        {
            $pos  = strpos($file, ';');
            $type = explode(':', substr($file, 0, $pos))[1];
            $random=Str::random();
            //GENERO UN NOME
            $name = time().$random.'.'. explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];

        if($type=='image/jpg' || $type=='image/jpeg' || $type=='image/png' ){
              $upload_status= \Image::make($file)->save($storage_path.$name);
        }
        else{
            return false;
        }

        if (!file_exists($storage_path.$name)) {
            return false;
        }
        else{
            $url_media = $anno.'/'.$mese.'/'.$name;

            $media = Media::create(array(
                'tipo'=> $type,
                'url_media'=> $url_media
            ));

            if($media){
                return $media->idMedia;;
            }
            else{
                return false;
            }

        }
      }
        else{
            return true;
        }

    }





/** CANCELLA IMMAGINE DAL DB E FILE SYSTEM */
    public static function cancellaImmagine($idMedia){

        $media= Media::select('url_media','=',$idMedia)->find($idMedia);
        $storage_path = public_path('uploads/'.$media['url_media']);

        $media = Media::where('idMedia','=',$idMedia)->delete();

        if($media){
            if(File::exists($storage_path)){
                File::delete($storage_path);
            }
            return true;
        }
        else{
            return false;
        }


    }



}
