<?php
namespace App\Models;
    use Illuminate\Database\Eloquent\Model;
    use Respect\Validation\Validator as v;
    use JWTAuth;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Hash;
    use App\Models\UtentiMeta;
    use App\Models\PazientiInfermieri;
    use App\Models\PazientiMedici;
    use App\Models\DatiClinici;
    use App\Models\Media;
    use App\Http\Controllers\EmailHelperController;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Contracts\Auth\MustVerifyEmail;
    use App\Notifications\ResetPassword;
    use App\Notifications\VerifyEmail;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Support\Facades\DB;

class Utenti extends Authenticatable implements MustVerifyEmail{

        use Notifiable;

        protected $table = 'users';
        protected $primaryKey = 'id';
        protected $guarded = ['id'];
        protected $hidden = ['password', 'remember_token'];



        /** INSERISCI UTENTE */
        public static function storeForAdmin($request){
            $validate = Utenti::validateUtente($request);
            if($validate === true){
                $name=$request->meta['nome'].' '.$request->meta['cognome'];
                $utente = Utenti::create(array(
                    'name'=>$name,
                    'role'=>$request->role,
                    'email'=>$request->email,
                    'password'=>Hash::make($request->password),
                    'stato'=>1,
                ));

                if($utente){
                    $response=array(
                        "success"=>true,
                        "idUtente"=>$utente->id
                    );
                    return $response;
                }
                else{
                    $response=array(
                        "success"=>false,
                        "idUtente"=>'Errore Registrazione'
                    );

                return $response;
                }
            }
            else{
                $response=array(
                    "success"=>false,
                    "idUtente"=>$validate
                );
                return $response;
            }
        }

/*
    INSERISCI UTENTE
*/
        public static function store($request){

            $validate = Utenti::validate($request);
            if($validate === true){
                $name=$request->meta['nome'].' '.$request->meta['cognome'];
                $utente = Utenti::create(array(
                    'name'=>$name,
                    'role'=>$request->ruolo,
                    'email'=>$request->email,
                    'password'=>Hash::make($request->password),
                    'stato'=>1,
                ));

                if($utente){
                    $response=array(
                        "success"=>true,
                        "idUtente"=>$utente->id
                    );
                    return $response;
                }
                else{
                    $response=array(
                        "success"=>false,
                        "idUtente"=>'Errore Registrazione'
                    );

                return $response;
                }
            }
            else{
                $response=array(
                    "success"=>false,
                    "idUtente"=>$validate
                );
                return $response;
            }
        }


      /** AGGIORNA UTENTE */
      public static function aggiornaUtente($request){

        $validate =  self::validateUtente($request);

        if($validate === true){
            $name = $request->meta['nome'].' '.$request->meta['cognome'];
            $utente = Utenti::where('id','=',$request->id)->update(array(
                'name'=>$name,
                'role'=>$request->role,
                'email'=>$request->email,
            ));

            if($utente){
                $response=array(
                    "success"=>true,
                    "idUtente"=>$request->id
                );
                return $response;
            }
            else{
                $response=array(
                    "success"=>false,
                    "idUtente"=>'Errore Registrazione'
                );
                return $response;
            }
        }
        else{
            $response=array(
                "success"=>false,
                "idUtente"=>$validate
            );
            return $response;
        }


      }


/*
    GET PROFILO
*/
    public static function getProfilo($request){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $utente = Utenti::Select('email')
        ->where('id','=',$userId)
        ->get()->toArray();

        $utente_meta = UtentiMeta::getMetaInfo($userId);

        $utente['0']['meta']=$utente_meta;
        return $utente['0'];
    }


    /** 
     * GET ALL PAZIENTI ROLE ADMIN
     * @param $request
     * @return array
     */
    public static function getAllPazienti($request){

        $keyword = $request->keyword;

        $pazienti = self::
        Select('id as idUser','name','url_media')
        ->leftjoin('media','users.idMedia','=','media.idMedia')
        ->where('role','=','paziente');

        if(!empty($keyword)){
            $pazienti = $pazienti->where('name','like','%'.$keyword.'%');
        }

        $pazienti = $pazienti->get()->toArray();

      
        return $pazienti;

    }

    /** GET ALL MEDICI ADMIN */
    public static function getAllMedici($request){

        $keyword = $request->keyword;

        $medici = self::
        Select('id as idUser','name','url_media')
        ->leftjoin('media','users.idMedia','=','media.idMedia')
        ->where('role','=','medico');

        if(!empty($keyword)){
            $medici = $medici->where('name','like','%'.$keyword.'%');
        }

        $medici = $medici->get()->toArray();

      
        return $medici;

    }

    public static function getAllInfermieri($request){

        $keyword = $request->keyword;

        $infermieri = self::
        Select('id as idUser','name','url_media')
        ->leftjoin('media','users.idMedia','=','media.idMedia')
        ->where('role','=','infermiere');

        if(!empty($keyword)){
            $infermieri = $infermieri->where('name','like','%'.$keyword.'%');
        }

        $infermieri = $infermieri->get()->toArray();

      
        return $infermieri;

    }


/*
    GET AVATAR
*/
public static function getAvatar($idUtente){
    $utente = Utenti::Select('name','users.idMedia','url_media')
    ->where('id','=',$idUtente)
    ->leftjoin('media','media.idMedia','=','users.idMedia')
    ->get()->toArray();
    return $utente;
}
/*
    GET MEDICO
*/
    public static function getMedico($idMedico){
        $medico = Utenti::Select('name','users.idMedia','url_media')
        ->where('id','=',$idMedico)
        ->where('role','=','medico')
        ->join('media','media.idMedia','=','users.idMedia')
        ->get()->toArray();

        return $medico;
    }
/*
    AGGIORNAMENTO UTENTE
*/
public static function aggiornaProfilo($request){

$utente='';

$validate = Utenti::validateAggiorna($request);
    if($validate === true){

        DB::transaction(function() use (&$request,&$utente)
            {
            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;
            $ruolo = $user->role;

            $name=$request->meta['nome'].' '.$request->meta['cognome'];
            $utente=Utenti::where('id','=',$userId)->update(array(
                'name'=>$name
            ));
            if($utente){
                foreach($request['meta'] as $key=>$valore){
                    UtentiMeta::aggiorna($userId,$key,$valore);
                }
            }
        });

        if($utente){
            $response=array(
                "success"=>true,
            );
        }
        else{
            $response=array(
                "success"=>false,
                "idUtente"=>'Errore Aggiornamento'
            );
        }
    }
    else{
        $response=array(
            "success"=>false,
            "idUtente"=>$validate
        );
    }

    return $response;
}

/*
     public static function registrazioneMedico($request){
        $validate = Utenti::validateMedico($request);
        if($validate === true){
            $name=$request->nome.' '.$request->cognome;
            $utente = Utenti::create(array(
                'name'=>$name,
                'role'=>'medico',
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'stato'=>1,
            ));

            if($utente){
                MediciMeta::store($request,$utente->id);
                EmailHelperController::invioVerificaEmail($utente->id);
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

public static function registrazioneInfermiere($request){
    $validate = Utenti::validateInfermiere($request);
    if($validate === true){
        $name=$request->nome.' '.$request->cognome;
        $utente = Utenti::create(array(
            'name'=>$name,
            'role'=>'infermiere',
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'stato'=>1,
        ));

        if($utente){
            InfermieriMeta::store($request,$utente->id);
            EmailHelperController::invioVerificaEmail($utente->id);
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return $validate;
    }

}

public static function registrazionePaziente($request){
    $validate = Utenti::validateMedico($request);
    if($validate === true){
        $name=$request->nome.' '.$request->cognome;
        $utente = Utenti::create(array(
            'name'=>$name,
            'role'=>'paziente',
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'stato'=>1,
        ));

        if($utente){
            PazientiMeta::store($request,$utente->id);
            EmailHelperController::invioVerificaEmail($utente->id);
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return $validate;
    }

}

*/
/*
    GET LAST ID
*/
public static function getLastId(){
    $utenteId = Utenti::latest()->first()->id;
    return $utenteId;

}


/* FILTRA MEDICI */
public static function getMedici($request){

    $keyword = $request->keyword;

    $medici = Utenti::Select('id','name')->where('role','=','medico');

    if(!empty($keyword)){
        $medici = $medici->where('name','like','%'.$keyword.'%');
    }

    $medici = $medici->orderBy('name')->get()->toArray();
    return $medici;
}


/* FILTRA MEDICI */
public static function getInfermieri($request){

    $keyword = $request->keyword;

    $infermieri = Utenti::Select('id','name')->where('role','=','infermiere');

    if(!empty($keyword)){
        $infermieri = $infermieri->where('name','like','%'.$keyword.'%');
    }

    $infermieri = $infermieri->orderBy('name')->get()->toArray();
    return $infermieri;

}


/* GET UTENTI  */
    public static function getUtenti($request){
            $parolachiave = $request['keywords'];
            $ruolo = $request['ruolo'];

            $utenti = Utenti::Select();

            if(!empty($parolachiave)){
                $utenti = $utenti->where('name','like','%'.$parolachiave.'%');
            }
            if(!empty($ruolo) && $ruolo!='0'){
                $utenti = $utenti->where('role','=',$ruolo);
            }

            $utenti = $utenti->get()->toArray();

            foreach($utenti as $key=>$value){
                $utenti[$key]['meta'] = UtentiMeta::getMetaInfo($value['id']);
            }

            return $utenti;
        }

        /*
    PER SELEZIONARE UN PAZIENTE PER LO STORE
*/
 public static function selezionaPaziente($request){

    $idPaziente=$request->idPaziente;

    $paziente = Utenti::Select('id','name','url_media')
    ->where('id','=',$idPaziente)
    ->leftjoin('media','media.idMedia','=','users.idMedia')
    ->get()->toArray();

    return $paziente;

 }

/*GET UTENTE */
/*
        public static function getUtente($request){

            $utente = Utenti::Select('id','name','email','role')
            ->where('id','=',$idUtente)
            ->get()->toArray();
            return $utente;
        }


*/

/** GET PROFILO */
    /*public static function getProfiloGeneric(){


        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;


        $utente = Utenti::Select()
        ->where('users.id','=',$userId)
        ->leftjoin('utenti_meta','utenti_meta.idUser','=','users.id')
        ->limit(1)->get()->toArray();


        foreach($utente as $key=>$value){
            $utente['0']['meta'][$value['chiave']] = $value['valore'];
        }

        return $utente;
    }

/*



// GET PROFILO MEDICO
    public static function getProfiloMedico(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $medico =  Utenti::Select('nome','cognome','email','telefono','idReparto','codice_odm',
        'provincia_odm','numero_enpam','url_media')
        ->where('users.id','=',$userId)
        ->leftjoin('medici_meta','users.id','=','medici_meta.idUser')
        ->leftjoin('media','media.idMedia','=','users.idMedia')
        ->limit(1)->get()->toArray();

        return $medico;
    }


//GET PROFILO  PAZIENTE
public static function getProfiloPaziente(){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;

    $paziente =  Utenti::Select('nome','cognome','data_nascita','provincia_nascita','comune_nascita',
    'provincia_residenza','comune_residenza','provincia_asl','comune_asl','codice_fiscale','email','telefono','url_media')
    ->where('users.id','=',$userId)
    ->leftjoin('pazienti_meta','users.id','=','pazienti_meta.idUser')
    ->leftjoin('media','media.idMedia','=','users.idMedia')
    ->limit(1)->get()->toArray();

    return $paziente;
}
*/
//GET PROFILO UTENTE CORRENTE
public static function utenteCorrente(){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $ruolo = $user->role;

    $utente =  Utenti::Select()
    ->where('users.id','=',$userId);

    if($ruolo=='paziente'){
        $utente=$utente->leftjoin('utenti_meta','users.id','=','utenti_meta.idUser');
        $utente=$utente->leftjoin('media','media.idMedia','=','users.idMedia');
    }
    else if ($ruolo=='medico'){
        $utente=$utente->leftjoin('utenti_meta','users.id','=','utenti_meta.idUser');
        $utente=$utente->leftjoin('media','media.idMedia','=','users.idMedia');
    }
    else if ($ruolo=='infermiere'){
        $utente=$utente->leftjoin('utenti_meta','users.id','=','utenti_meta.idUser');
        $utente=$utente->leftjoin('media','media.idMedia','=','users.idMedia');
    }

    $utente=$utente->limit(1)->get()->toArray();

    return $utente['0'];
}



/** AGGIORNA PASSWORD ROLE ADMIN */
public static function aggiornaPasswordRoleAdmin($request){

    $validate = Utenti::validatePassword($request);


    if($validate === true){

        $utente = Utenti::where('id','=',$request->id)->update(array(
            'password'=>Hash::make($request->password)
        ));

        if($utente){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return $validate;
    }
}


public static function aggiornaPassword($request){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;

    $validate = Utenti::validatePassword($request);


    if($validate === true){

        $utente = Utenti::where('id','=',$userId)->update(array(
            'password'=>Hash::make($request->password)
        ));

        if($utente){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return $validate;
    }

}

/** UPDATE IMAGE */
public static function aggiornaAvatar($idMedia){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role = $user->role;

    $lastIdMedia = '';
    $lastUrlMedia = '';

    $lastMedia = Utenti::checkUserImage($userId);

    if($lastIdMedia != null){
        Media::cancellaImmagine($lastIdMedia);
    }

    $avatarmedico = Utenti::where('id','=',$userId)->update(array(
        'idMedia'=> $idMedia
    ));

    if($avatarmedico){
        return true;
    }
    else{
        return false;
    }


}



/** GET EMAIL  */
public static function getEmail($idPaziente){

    $paziente = Utenti::Select('email')
    ->where('id','=',$idPaziente)->limit(1)->get()->toArray();

    return $paziente['0']['email'];

}


/* GET SCHEDA PAZIENTE CON TERAPIA */
public static function getPaziente($request){

    $id=$request->idPaziente;
    $paziente = Utenti::Select('name','email');
    $paziente=$paziente->where('id','=',$id)->get()->toArray();

    $paziente[0]['meta'] = UtentiMeta::getMetaInfo($id);

    $ultimo_ecg=Esami::getLastEcg($id);
    $ultimo_emogas_arterioso = Esami::getLastEmogasArterioso($id);
    $ultimo_markers_scompenso = Esami::getLastMarkersScompenso($id);
    $ultimo_ecocardiogramma = Esami::getLastEcocardiogramma($id);
    $ultimo_markers_infettivi = Esami::getLastMarkersInfettivo($id);
    $ultimo_protidogramma = Esami::getLastProtidogramma($id);
    $ultimo_funzione_renale = Esami::getLastFunzioneRenale($id);
    $ultimo_urine = Esami::getLastUrine($id);
    $ultimo_metabolico = Esami::getLastMetabolico($id);


    $paziente[0]['esami']['ecg']=$ultimo_ecg;
    $paziente[0]['esami']['emogas_arterioso'] = $ultimo_emogas_arterioso;
    $paziente[0]['esami']['markers_scompenso'] = $ultimo_markers_scompenso;
    $paziente[0]['esami']['ecocardiogramma'] = $ultimo_ecocardiogramma;
    $paziente[0]['esami']['markers_infettivi'] = $ultimo_markers_infettivi;
    $paziente[0]['esami']['protidogramma'] = $ultimo_protidogramma;
    $paziente[0]['esami']['funzione_renale'] = $ultimo_funzione_renale;
    $paziente[0]['esami']['urine'] = $ultimo_urine;
    $paziente[0]['esami']['metabolico'] = $ultimo_metabolico;


    if(array_key_exists('scheda_clinica',$paziente['0']['meta'])){
        $paziente[0]['meta']['scheda_clinica'] = unserialize($paziente[0]['meta']['scheda_clinica']);
    }

    $paziente[0]['esami']['daticlinici'] =  DatiClinici::getLastByIdPaziente($id);
    $paziente[0]['allarmi']=  Allarmi::getAllarmi($request);

    return $paziente['0'];
}



/** AGGIORNA SCHEDA CLINICA */
public static function aggiornaSchedaClinica($request){

    $idUtente = $request->idPaziente;
    $scheda_clinica = $request->scheda_clinica;

    if($scheda_clinica != ''){

        $meta = UtentiMeta::aggiorna($idUtente,'scheda_clinica',$scheda_clinica);

        if($meta){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}


/*
    CHECK IF USER HAVE IMAGE
*/
public static function checkUserImage($idUser){

    $idMedia = Utenti::select('idMedia')->where('id','=',$idUser);

    if($idMedia==null){
        return $idMedia;
    }
    else{
        return true;
    }
}

public static function validatePassword($request){

    $password = $request->password;
    $password_2 = $request->password_2;

    $message = '';

    if(!v::stringType()->length(6, null)->validate($password)){
        $message = 'Password troppo corta';
    }
    else if(!v::notEmpty()->validate($password)){
        $message = 'Inserisci Password';
    }
    else if(!v::notEmpty()->validate($password_2)){
        $message = 'Conferma Password';
    }
    else if($password != $password_2){
        $message = 'Le password non combaciano';
    }


    if($message != ''){
        return $message;
    }
    else{
        return true;
    }

}


    /** CANCELLA PROFILO */
    public static function cancellaProfilo(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;


        if($role == 'paziente'){

            //CANCELLA PAZIENTI INFERMIERI
            $medici = PazientiInfermieri::cancellaInfermiereAssProfilo($userId,$role);

            //CANCELLA PAZIENTI MEDICI
            $medici = PazientiMedici::cancellaMedicoAssProfilo($userId,$role);

            //CANCELLA META
            $meta = UtentiMeta::cancellaMeta($userId);
        }


        if($role == 'medico'){

            //CANCELLA PAZIENTI MEDICI
            $medici = PazientiMedici::cancellaMedicoAssProfilo($userId,$role);

            //CANCELLA META
            $meta = UtentiMeta::cancellaMeta($userId);
        }

        if($role == 'infermiere'){

            //CANCELLA PAZIENTI INFERMIERI
            $medici = PazientiInfermieri::cancellaInfermiereAssProfilo($userId,$role);


            //CANCELLA META
            $meta = UtentiMeta::cancellaMeta($userId);
        }


        $profilo = Utenti::where('id','=',$userId)->delete();

        if($profilo){
            return true;
        }
        else{
            return false;
        }


    }



 /*
    CANCELLA UTENTE
 */
        public static function cancellaUtente($request){
            $idUtente = $request->id;

            $utenti = Utenti::where('id','=',$idUtente)->delete();

            if($utenti){
                return true;
            }
            else{
                return false;
            }
        }


/*
    VALIDATE EMAIL ESISTENTE
*/
    public static function checkEmail($request){
        $email = $request->email;
        $idUtente = $request->id;

        if(empty($idUtente)){
            $user = JWTAuth::parseToken()->authenticate();
            $idUtente = $user->id;
        }

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $utenti = Utenti::Select('email')->where('email','=',$email)->get()->toArray();
            return $utenti;
        }
        else{
            $utenti = Utenti::Select('email')->where('email','=',$email)->where('id','!=',$idUtente)->get()->toArray();
            return $utenti;
        }


    }


    /** VALIDATE UTENTE ROLE ADMIN **/
    public static function validateUtente($request){

        $message = '';
        $nome = $request->meta['nome'];
        $cognome = $request->meta['cognome'];
        $email = $request->email;
        $role= $request->role;
        $password = $request->password;
        $password2 = $request->password_2;

        if(!v::notEmpty()->validate($nome)){
            $message = 'Inserisci Nome';
        }
        if(!v::notEmpty()->validate($cognome)){
            $message = 'Inserici Cognome';
        }
        else if(!v::notEmpty()->validate($email)){
            $message = 'Inserisci Cognome';
        }
        else if(count(self::checkEmail($request))){
            $message = 'Email già esistente';
        }
        else if(!v::notEmpty()->validate($role)){
            $message = 'Inserisci Ruolo';
        }   

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(!v::stringType()->length(6, null)->validate($password)){
                $message = 'Password troppo corta';
            }
            else if(!v::notEmpty()->validate($password)){
                $message = 'Inserisci Password';
            }
            else if(!v::notEmpty()->validate($password2)){
                $message = 'Conferma Password';
            }
            else if($password != $password2){
                $message = 'Le password non combaciano';
            }
        }
    


        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }
    


/*
    VALIDATE REGISTRAZIONE PROFILO UTENTE
*/
    public static function validate($request){
        $nome = $request->meta['nome'];
        $cognome = $request->meta['cognome'];
        $email = $request->email;
        $password = $request->password;
        $password2 = $request->password_2;
        $ruolo=$request->ruolo;
        $message='';
        if($ruolo=='medico'|| $ruolo=='infermiere' || $ruolo=='paziente'){

        }
        else{
            $message='Errore Registrazione';
        }
        if(!v::notEmpty()->validate($nome)){
            $message = 'Inserisci Nome';
        }
        else if(!v::notEmpty()->validate($cognome)){
            $message = 'Inserisci Cognome';
        }
        else if(!v::notEmpty()->validate($email)){
            $message = 'Inserisci Email';
        }

        if($message!=''){
            return $message;
        }
        else{
            return true;
        }
    }

/*
    VALIDATE REGISTRAZIONE PROFILO UTENTE
*/
public static function validateAggiorna($request){
    $nome = $request->meta['nome'];
    $cognome = $request->meta['cognome'];
    $message='';

    if(!v::notEmpty()->validate($nome)){
        $message = 'Inserisci Nome';
    }
    else if(!v::notEmpty()->validate($cognome)){
        $message = 'Inserisci Cognome';
    }

    if($message!=''){
        return $message;
    }
    else{
        return true;
    }
}

    /**
     * Get the oauth providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}




?>
