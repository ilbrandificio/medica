<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Anticoagulante;
use Respect\Validation\Validator as v;
use DateTime;

class SchedaAnticoagulante extends Model
{

    protected $table = 'scheda_anticoagulante';
    protected $primaryKey = 'idSchedaAnticoagulante';
    protected $guarded = ['idSchedaAnticoagulante'];

    /** STORE SCHEDA ANTICOAGULANTE */
    public static function store($request){

        $validate = SchedaAnticoagulante::validate($request);

        if($validate === true){

            $scheda_anticoagulante = SchedaAnticoagulante::create(array(
                'idAnticoagulante'=> $request->idAnticoagulante,
                'data'=> $request->data,
                'inr'=> $request->inr
            ));

            if($scheda_anticoagulante){
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return $validate;
        }

    }


    /** CHECK INR */
    public static function checkInr($data){

        $inr = SchedaAnticoagulante::where('data','=',$data)
        ->get()->toArray();

        if(count($inr) == 1){
            return true;
        }
        else{
            return false;
        }

    }





    /** GET ANTICOAGULANTE By Id */
    public static function getSchedaAnticoagulanteById($idAnticoagulante){

        $new_array = array();

        $scheda_anticoagulante = SchedaAnticoagulante::Select('idSchedaAnticoagulante','data','inr','dose')
        ->where('scheda_anticoagulante.idAnticoagulante','=',$idAnticoagulante)
        /*->where(function($query){
            $query->where('data','>=','data_inizio')
            ->orwhere('data','<=','data_fine');
        })*/
        ->join('anticoagulante','scheda_anticoagulante.idAnticoagulante','=','anticoagulante.idAnticoagulante')
        ->orderBy('data','ASC')
        ->get()->toArray();



        foreach($scheda_anticoagulante as $key=>$value){

            $month =  date('m',strtotime($value['data']));

            if(array_key_exists($month,$new_array)){
                array_push($new_array[$month],$value);
            }
            else{
                $new_array[$month] = [];
                array_push($new_array[$month],$value);
            }


        }



        return $new_array;


    }



    /** CANCELLA SCHEDA ANTICOAGULANTE */
    public static function cancella($idSchedaAnticoagulante){

        $scheda_anticoagulante = SchedaAnticoagulante::where('idSchedaAnticoagulante','=',$idSchedaAnticoagulante)->delete();

        if($scheda_anticoagulante){
            return true;
        }
        else{
            return false;
        }
    }


    public static function validate($request){

        $message = '';

        $idAnticoagulante = $request->idAnticoagulante;
        $data = $request->data;
        $inr = $request->inr;

        if(!v::notEmpty()->validate($data)){
            $message = 'Inserisci Data';
        }
        else if(SchedaAnticoagulante::checkInr($data)){
            $message = 'INR Già inserito per questa data';
        }
        else if(Anticoagulante::checkRangeDate($idAnticoagulante,$data)){
            $message = 'Data non Valida';
        }
        else if(!v::notEmpty()->validate($inr)){
            $message = 'Inserisci INR';
        }


        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }



}
