<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use Carbon\Carbon;
use App\Models\UtentiMeta;
use JWTAuth;


class PazientiMedici extends Model
{
    protected $table = 'pazienti_medici';
    protected $primaryKey = 'idPazienteMedico';
    protected $guarded = ['idPazienteMedico'];

/*
    ASSEGNAZIONE MEDICO
*/
public static function assegnaMedico($idMedico){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;

        $validate = PazientiMedici::validate($idMedico);

        if($validate === true){
            $utente = PazientiMedici::create(array(
                'idPaziente'=>$userId,
                'idMedico'=>$idMedico,
            ));

            if($utente){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }
/*
    CONTROLLO SE IL MEDICO HA ACCESSO AL PAZIENTE
*/
public static function checkAccessoPaziente($idPaziente){
    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role=$user->role;

    //SE MEDICO CONTROLLO CHE ABBIA ACCESSO A QUEL PAZIENTE
        if($role=='medico'){
            $paziente = PazientiMedici::Select('idPaziente')
            ->where('idMedico','=',$userId)
            ->where('idPaziente','=',$idPaziente)
            ->get()->toArray();
        //SE IL MEDICO HA ACCESSO
            if($paziente){
                return true;
            }
            else{
                return false;
            }
        }
    //SE ADMIN ACCESSO A TUTTO
        else if($role=='admin'){
            return true;
        }
        else{
            return false;
        }
    }


/** RECUPERO I MEDICI DI UN PAZIENTE */
    public static function getPazienteMedici(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;

        $medici =  PazientiMedici::Select('pazienti_medici.idPazienteMedico','pazienti_medici.idMedico','idPaziente','name','pazienti_medici.stato')
        ->where('idPaziente','=',$userId)
        ->join('users','pazienti_medici.idMedico','=','users.id')->get()->toArray();

        return $medici;
    }

/** RECUPERO I PAZIENTI DI UN MEDICO ATTIVI */
public static function getPazientiMediciAttivi(){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role=$user->role;

    $pazienti =  PazientiMedici::Select('pazienti_medici.idPazienteMedico')
    ->where('idMedico','=',$userId)
    ->where('stato','=',1)->get()->count();
    return $pazienti;
}

/** RECUPERO I PAZIENTI DI UN MEDICO ATTIVI */
public static function getPazientiMediciInAttesa(){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role=$user->role;

    $pazienti =  PazientiMedici::Select('pazienti_medici.idPazienteMedico')
    ->where('idMedico','=',$userId)
    ->where('stato','=',0)->get()->count();
    return $pazienti;
}

    /** RECUPERO I PAZIENTI DI UN MEDICO  */
    public static function getRichieste(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role=$user->role;

        $richieste = PazientiMedici::Select('pazienti_medici.idPazienteMedico','name','pazienti_medici.stato')
        ->where('pazienti_medici.idMedico', '=', $userId)
        ->join('users','users.id','=','pazienti_medici.idPaziente')
        ->get()->toArray();

        return $richieste;

    }

    /** RECUPERO I MEDICI ASSEGNATI   */
    public static function getMediciPaziente(){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        $medici = PazientiMedici::Select('idMedico','name')
        ->join('users','users.id','=','pazienti_medici.idMedico')
        ->where('idPaziente','=',$userId)->get()->toArray();

        foreach($medici as $key=>$medico){
            $medici[$key]['meta'] = UtentiMeta::getMetaInfo($medico['idMedico']);
        }

        return $medici;

    }


    /*RECUPERO I PAZIENTI DI UN MEDICO*/
    public static function getPazientiMedici($request){

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;

        $keyword = $request->keyword;
        $pazienti = PazientiMedici::Select('idPaziente as idUser','name','url_media')
        ->where('idMedico','=',$userId)
        ->where('pazienti_medici.stato','=',1);

        if(!empty($keyword)){
            $pazienti=$pazienti->where('name','like','%'.$keyword.'%');
        }

        $pazienti=$pazienti->join('users','users.id','=','pazienti_medici.idPaziente');
        $pazienti=$pazienti->leftjoin('media','media.idMedia','=','users.idMedia');

        $pazienti = $pazienti->get()->toArray();

        return $pazienti;
    }



/** ACCETTA RICHIESTA */
    public static function accettaRichiesta($request){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        $role = $user->role;

        $richiesta = PazientiMedici::where('idPazienteMedico','=',$request->idRichiesta)
        ->where('idMedico','=',$userId)
        ->update(array(
            'stato'=>1,
            ));

            if($richiesta){
                return true;
            }
            else{
                return false;
            }
    }


    /** CANCELLA PAZIENTE ASSEGNATO
     * @param $idMedico
     * @param $idPazienteMedico
     * @return bool
     */
    public static function cancellaPazienteAssegnato($idMedico,$idPazienteMedico){

        $paziente_assegnato = PazientiMedici::where('idPazienteMedico','=',$idPazienteMedico)
        ->where('idMedico','=',$idMedico)->delete();

        if($paziente_assegnato){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * CANCELLA MEDICO ASSEGNATO
     * @param $idPaziente
     * @param $idPazienteMedico
     * @return bool
    */
    public static function cancellaMedicoAssegnato($idPaziente,$idPazienteMedico){
            
        $medicoass = PazientiMedici::where('idPazienteMedico','=',$idPazienteMedico)
        ->where('idPaziente','=',$idPaziente)
        ->delete();

        if($medicoass){
            return true;
        }
        else{
            return false;
        }
       
    }

/*
 CHECK PAZIENTI ASSEGNATI
*/

public static function checkPazientiMedici($request){
    $idPaziente=$request->idPaziente;
    $user = JWTAuth::parseToken()->authenticate();
    $role = $user->role;
    $userId= $user->id;

    if(PazientiMedici::checkPaziente($idPaziente,$userId)){
        return true;
    }
    else{
        return false;
    }
 }

/*
    CHECK MEDICI ASSEGNATI
*/

public static function checkMediciPazienti($request){
    $idMedico=$request->idMedico;
    $user = JWTAuth::parseToken()->authenticate();
    $role = $user->role;
    $userId= $user->id;

    if(PazientiMedici::checkPaziente($userId,$idMedico)){
        return true;
    }
    else{
        return false;
    }
 }

/*
    CONTROLLO CHE IL PAZIENTE SIA ASSEGNATO
*/
    public static function checkPaziente($idPaziente,$idMedico){
      $pazienti = PazientiMedici::Select('idPaziente','idMedico')
      ->where('idPaziente','=',$idPaziente)
      ->where('idMedico','=',$idMedico)
      ->get()->toArray();

      if($pazienti){
         return true;
      }
      else{
          return false;
      }
   }


    public static function checkMedicoAss($idMedico){

        $user = JWTAuth::parseToken()->authenticate();
        $role=$user->role;

        if($role == 'paziente'){
            $userId = $user->id;
        }

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $medici = PazientiMedici::Select('idMedico')->where('idMedico','=',$idMedico)->
            where('idPaziente','=',$userId)->
            get()->toArray();

            return $medici;
        }


    }



    /** CANCELLA MEDICO ASS  (PROFILO) */
    public static function cancellaMedicoAssProfilo($userId,$role){

        if($role == 'medico'){
            $medicoass = PazientiMedici::where('idMedico','=',$userId)->delete();
        }

        if($role == 'paziente'){
            $medicoass = PazientiMedici::where('idPaziente','=',$userId)->delete();
        }

        if($medicoass){
            return true;
        }
        else{
            return false;
        }

    }


    /** GET PAZIENTI MEDICO ADMIN 
     * @param $idMedico
     * @return array
    */
    public static function getPazientiMedico($idMedico){

        $pazientimedico = self::Select('name','idPaziente','idMedico','pazienti_medici.stato')->where('idMedico','=',$idMedico)
        ->join('users','users.id','=','pazienti_medici.idPaziente')->get()->toArray();

        foreach($pazientimedico as $key=>$paziente){
            $pazientimedico[$key]['meta'] = UtentiMeta::getMetaInfo($paziente['idPaziente']);
        }

        return $pazientimedico;

    }



    public static function validate($idMedico){

        $message = '';


        if(count(PazientiMedici::checkMedicoAss($idMedico))>=1){
            $message = 'Medico già assegnato';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }

}



