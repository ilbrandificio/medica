<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use JWTAuth;


class Anticoagulante extends Model
{
    protected $table = 'anticoagulante';
    protected $primaryKey = 'idAnticoagulante';
    protected $guarded = ['idAnticoagulante'];

    public static function store($request){

        $validate = Anticoagulante::validate($request);

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        if($validate === true){

           $anticoagulante =  Anticoagulante::create(array(
                'idPaziente'=> $request->idPaziente,
                'idMedico'=> $userId,
                'data_inizio'=> $request->data_inizio,
                'data_fine'=> $request->data_fine,
                'anticoagulante'=> $request->anticoagulante,
            ));

            if($anticoagulante){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }


    public static function aggiorna($request){

        $idAnticoagulante = $request->idAnticoagulante;

        $validate = Anticoagulante::validate($request);

        if($validate === true){

           $anticoagulante =  Anticoagulante::where('idAnticoagulante','=',$idAnticoagulante)->update(array(
                'data_inizio'=> $request->data_inizio,
                'data_fine'=> $request->data_fine,
                'anticoagulante'=> $request->anticoagulante
            ));

            if($anticoagulante){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }
/*
    AGGIORNA SCHEDA ANTICOAGULANTE
*/
    public static function aggiornaSchedaAnticoagulante ($request){

        $idAnticoagulante = $request->idAnticoagulante;
        $registrazioni=$request->registrazioni;
        $registrazioni_database=[];

        foreach($registrazioni as $key=>$mese){

            foreach($mese as $key=>$registrazione){
                if($registrazione['dose']!=''){
                    $registrazione_database=array(
                        'data'=>$registrazione['data'],
                        'dose'=>$registrazione['dose'],
                        'inr'=>$registrazione['inr']
                    );
                    array_push($registrazioni_database,$registrazione_database);
                }
            }
        }

        $anticoagulante =  Anticoagulante::where('idAnticoagulante','=',$idAnticoagulante)->update(array(
            'inr'=> serialize($registrazioni_database)
        ));

        if($anticoagulante){
            return true;
        }
        else{
            return false;
        }
        return true;
    }
/*
    RECUPERO GLI ANTICOAGULANTI
*/
    public static function getAnticoagulanti($request){

        $data_inizio = $request->data_inizio;
        $data_fine = $request->data_fine;

        $anticoagulante = Anticoagulante::Select()->where('idPaziente','=',$request->idPaziente);


        if(!empty($data_inizio) && !empty($data_fine)){

            $anticoagulante = $anticoagulante->whereBetween('data_inizio',[$data_inizio,$data_fine]);

        }


        $anticoagulante = $anticoagulante->get()->toArray();

        return $anticoagulante;
    }

/*
    RECUPERO L'ANTICOAGULANTE PER L'ID
*/
public static function getAnticoagulanteById($request){

    $id = $request->idAnticoagulante;

    $anticoagulante = Anticoagulante::Select()->where('idAnticoagulante','=',$id );
    $anticoagulante = $anticoagulante->get()->toArray();

    if($anticoagulante){

        $dates = [];
        $period = CarbonPeriod::create($anticoagulante['0']['data_inizio'], $anticoagulante['0']['data_fine']);

        foreach ($period as $date) {
            $mese=$date->format('n-Y');
            $date=$date->format('Y-m-d');

            if(!array_key_exists($mese,$dates)){
                $dates[$mese]=array();
              }
              $registrazione=array(
                'data'=>$date,
                'dose'=>'',
                'inr'=>''
            );
        $dates[$mese][$date]=$registrazione;
        }

        $inr=unserialize($anticoagulante['0']['inr']);
        if($inr){
            foreach($inr as $key=>$inr_singolo){
                $data=strtotime($inr_singolo['data']);
                $mese_registrazione=date('n-Y',$data);
                $dates[$mese_registrazione][$inr_singolo['data']]=$inr_singolo;
            }
        }
        $anticoagulante['0']['registrazioni']=$dates;
        return $anticoagulante['0'];
        }
        else{
            return false;
        }
}


    public static function checkRangeDate($idAnticoagulante,$data){

        $date_range = Anticoagulante::Select('data_inizio','data_fine')->where('idAnticoagulante','=',$idAnticoagulante)
        ->get()->toArray();

        if($data < $date_range['0']['data_inizio'] || $data > $date_range['0']['data_fine']){
            return true;
        }
        else{
            return false;
        }

    }


    public static function cancella($idAnticoagulante){

        $anticoagulante = Anticoagulante::where('idAnticoagulante','=',$idAnticoagulante)->delete();

        if($anticoagulante){
            return true;
        }
        else{
            return false;
        }

    }

    /**
     * GET ANTICOAGULANTI PER NOTIFICA
     * @return mixed
     */
    public static function getAnticoagulantiAttivi(){
        $current_date = date('Y-m-d');
        //$subtractday = strtotime('- 1 days', strtotime($current_date));
        //$final_date = date('Y-m-d',$subtractday);

        $anticoagulanti = Anticoagulante::Select('name','idPaziente','idAnticoagulante','data_inizio','data_fine',
            'anticoagulante','inr','anticoagulante.updated_at as ultima_modifica')

            ->join('users','users.id','=','anticoagulante.idPaziente')
            ->whereDate('data_inizio','<',$current_date)
            ->whereDate('data_fine','>',$current_date)
            ->get()->toArray();
        return $anticoagulanti;
    }


    public static function validate($request){

        $message = '';

        $data_inizio = $request->data_inizio;
        $data_fine = $request->data_fine;
        $anticoagulante = $request->anticoagulante;


        if(!v::notEmpty()->validate($data_inizio)){
            $message = 'Inserisci Data Inizio';
        }
        if(!v::notEmpty()->validate($data_fine)){
            $message = 'Inserisci Data Fine';
        }
        else if(!v::notEmpty()->validate($anticoagulante)){
            $message = 'Inserisci Anticoagulante';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }

}
