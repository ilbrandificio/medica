<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use App\Models\PazientiMedici;

class ApiKey extends Model
{

    protected $table = 'api_key';
    protected $primaryKey = 'idApiKey';
    protected $guarded = ['idApiKey'];

/* CARICA API KEY */
    public static function getApiKey($idPaziente){
        $apiKey = ApiKey::select()->where('idPaziente','=',$idPaziente)->get()->toArray();
        return $apiKey;
    }

/* CARICA ID PAZIENTE DA API KEY*/
public static function getIdPazienteByApiKey($api_key){
    $apiKey = ApiKey::select()->where('api_key','=',$api_key)->get()->toArray();
    return $apiKey['0']['idPaziente'];
}

/*  INSERISCI TERAPIA */
    public static function store($request){

        $validate=ApiKey::validate($request);
        $api_key=implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));

        if($validate===true){
            $api_key = ApiKey::create(array(
                "idPaziente"=>$request->idPaziente,
                'api_key'=>$api_key,
                ));

            if($api_key){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }
    public static function cancella($idApiKey){
        $api_key = ApiKey::where('idApiKey','=',$idApiKey)->delete();

        if($api_key){
            return true;
        }
        else{
            return false;
        }
    }

    public static function validateApi($api){
        $apiKey = ApiKey::select()->where('api_key','=',$api)->limit(1)->get()->toArray();
        if($apiKey){
            return true;
        }
        else{
            return false;
        }
        
    }

    public static function validate($request){
        $idPaziente=$request->idPaziente;
        //AGGIUNGERE CONTROLLO CHE LA KEY NON ESISTA GIà PER IL PAZIENTE
        return true;
    }
}
