<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use App\Models\Esami;
use App\Models\Utenti;
use Carbon\Carbon;

use JWTAuth;


class UtentiMeta extends Model
{
    protected $table = 'utenti_meta';
    protected $primaryKey = 'iUtenteMeta';
    protected $guarded = ['iUtenteMeta'];
/*
    STORE META DI UN UTENTE
*/
public static function store($idUtente, $chiave, $valore){


    if(is_array($valore)){
        $valore=serialize($valore);
    }

    $utente = UtentiMeta::create(array(
        'idUser'=>$idUtente,
        'chiave'=>$chiave,
        'valore'=>$valore,
    ));

    if($utente){
        return true;
    }
    else{
        return false;
    }


}

/*
    AGGIORNA USER META
*/
public static function aggiorna($idUtente, $chiave, $valore){

    if(UtentiMeta::checkKey($idUtente,$chiave)){

        if(is_array($valore)){
            $valore=serialize($valore);
        }

        $utente =UtentiMeta::where('idUser','=',$idUtente)
        ->where('chiave','=',$chiave)
        ->update(array(
            'valore'=>$valore
        ));

        if($utente){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        UtentiMeta::store($idUtente,$chiave,$valore);
    }

}


public static function checkKey($idUtente, $chiave){

    $checkKey=UtentiMeta::select()
    ->where('idUser','=',$idUtente)
    ->where('chiave','=',$chiave)
    ->get()->toArray();

    if($checkKey){
        return true;
    }
    else{
        return false;
    }

}


public static function getMetaInfo($userId){


    $utente_meta = UtentiMeta::select()
    ->where('idUser','=',$userId)
    ->get()->toArray();
    $meta_array=[];

    if($utente_meta){
        foreach($utente_meta as $key=>$meta){
            $meta_array[$meta['chiave']]= $meta['valore'];
        }

        return $meta_array;
    }
    else{
        return [];
    }

}


/** AGGIORNA SCHEDA CLINICA */
/*
public static function aggiornaSchedaClinica($request){

    $defibrillatore = $request->defibrillatore;
    $dati_defibrillatore = null;

    if($defibrillatore == 1){
        $dati_defibrillatore = serialize($request->dati_defibrillatore);
    }


    $utente =PazientiMeta::where('idUser','=',$request->idPaziente)->update(array(
        'superficie_corporea'=>$request->superficie_corporea,
        'peso'=>$request->peso,
        'altezza'=>$request->altezza,
        'gruppo'=>$request->gruppo,
        'epicrisi'=>$request->epicrisi,
        'problematiche'=>$request->problematiche,
        'diabete'=>$request->diabete,
        'fumo'=>$request->fumo,
        'sesso'=>$request->sesso,
        'anni_diabete'=>$request->anni_diabete,
        'ipertensione_arteriosa'=>$request->anni_diabete,
        'anni_ipertensione'=>$request->anni_diabete,
        'anemia'=> $request->anemia,
        'fibrillazioneatriale'=> $request->fibrillazioneatriale,
        'terapiaottimale'=> $request->terapiaottimale,
        'diagnosi_discorsiva'=> $request->diagnosi_discorsiva,
        'nyha'=> $request->nyha,
        'familiarita'=> serialize($request->familiarita),
        'defibrillatore'=> $defibrillatore,
        'dati_defibrillatore'=> $dati_defibrillatore
    ));

    if($utente){
        return true;

    }
    else{
        return false;
    }

}
/** AGGIORNA PROFILO */
/*
public static function aggiorna($request){

    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;

    $utente =PazientiMeta::where('idUser','=',$userId)->update(array(
        'nome'=>$request->nome,
        'cognome'=>$request->cognome,
        'telefono'=>$request->telefono,
        'indirizzo'=>$request->indirizzo,
        'data_nascita'=>$request->data_nascita,
        'provincia_nascita'=>$request->provincia_nascita,
        'comune_nascita'=>$request->comune_nascita,
        'codice_fiscale'=>$request->codice_fiscale,
        'provincia_residenza'=>$request->provincia_residenza,
        'comune_residenza'=>$request->comune_residenza,
        'provincia_asl'=>$request->provincia_asl,
        'comune_asl'=>$request->comune_asl,
    ));

    if($utente){
        return true;

    }
    else{
        return false;
    }

}

/* FILTRA PAZIENTI */
public static function getPazienti($request){
    $user = JWTAuth::parseToken()->authenticate();
    $userId = $user->id;
    $role = $user->role;

    $keyword = $request->keyword;
    $pazienti = PazientiMeta::Select('idUser','nome','cognome','url_media');

    if(!empty($keyword)){
    $pazienti=$pazienti->where(function($query) use ($keyword) {
        $query->where('nome','like','%'.$keyword.'%')
            ->orWhere('cognome','like','%'.$keyword.'%');
        });
    }

    $pazienti=$pazienti->leftjoin('media','media.idMedia','=','users.idMedia');

    if($role == 'medico'){
        $pazienti=$pazienti->leftjoin('pazienti_medici','pazienti_medici.idPaziente','=','pazienti_meta.idUser')->
        where('pazienti_medici.idMedico','=',$userId);
    }


    $pazienti = $pazienti->get()->toArray();


    return $pazienti;
    }




/*
    PER SELEZIONARE UN PAZIENTE PER LO STORE
*/
 public static function selezionaPaziente($request){
    $idPaziente=$request->idPaziente;
    $paziente = PazientiMeta::Select('idUser','nome','cognome','url_media')
    ->where('idUser','=',$idPaziente)
    ->leftjoin('users','users.id','=','pazienti_meta.idUser')
    ->leftjoin('media','media.idMedia','=','users.idMedia')
    ->get()->toArray();

    return $paziente;
 }


 /** GET IMAGE */
    public static function getImagePaziente($idUser){

        $media = PazientiMeta::Select('users.idMedia','url_media')->where('idUser','=',$idUser)
        ->leftjoin('users','users.id','=',$idUser)
        ->leftjoin('media','media.idMedia','=','users.idMedia')
        ->limit(1)->get()->toArray();

        return $media;
    }

 /** UPDATE IMAGE */
   public static function updateImage($idUser,$idMedia){

    $avatarpaziente = PazientiMeta::where('idUser','=',$idUser)->update(array(
        'idMedia'=> $idMedia
    ));

    if($avatarpaziente){
        return true;
    }
    else{
        return false;
    }

  }


  /** GET DATI PAZIENTE PER I MESSAGGI */
    public static function getDatiPaziente($idMittente){

        $paziente = PazientiMeta::Select('nome','cognome','url_media','idUser')
        ->where('idUser','=',$idMittente)
        ->leftjoin('users','users.id','=','pazienti_meta.idUser')
        ->leftjoin('media','media.idMedia','=','users.idMedia')
        ->get()->toArray();

        return $paziente[0];

    }


    /** CANCELLA META */
    public static function cancellaMeta($userId){

        $meta = UtentiMeta::where('idUser','=',$userId)->delete();

        if($meta){
            return true;
        }
        else{
            return false;
        }

    }

}
