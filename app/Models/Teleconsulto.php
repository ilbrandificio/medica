<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use JWTAuth;


class Teleconsulto extends Model
{

    protected $table = 'teleconsulti';
    protected $primaryKey = 'idTeleconsulto';
    protected $guarded = ['idTeleconsulto'];

/** STORE TELECOSNULTO */
    public static function store($request){

        $validate = Teleconsulto::validateTeleconsulto($request);

        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;

        if($validate === true){

            $teleconsulto = Teleconsulto::create(array(
                'idMedico'=> $request->idMedico,
                'idPaziente'=> $userId,
                'stato'=> 'attesa',
                'messaggio'=> $request->messaggio
            ));


            if($teleconsulto){
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return $validate;
        }
    }

/** CONFERMA RICHIESTA */
    public static function confermaTeleconsulto($request){
        $idTeleconsulto = $request->idTeleconsulto;

        $teleconsulto = Teleconsulto::where('idTeleconsulto','=',$idTeleconsulto)->update(
            array(
                'dataTeleconsulto'=> $request->dataTeleconsulto,
                'oraTeleconsulto' => $request->oraTeleconsulto,
                'ora_fine'=> $request->ora_fine,
                'prezzo'=> $request->prezzo,
                'stato'=> 'confermato'
            )
        );

        if($teleconsulto){
            return true;
        }
        else{
            return false;
        }

    }
    
/** AGGIORNA STATO E PREZZO TELECONSULTO */
    public static function aggiorna($request){

        $idTeleconsulto = $request->idTeleconsulto;


        $teleconsulto = Teleconsulto::where('idTeleconsulto','=',$idTeleconsulto)
        ->update(array(
            'stato'=> $request->stato,
            'prezzo'=> $request->prezzo
        ));

        if($teleconsulto){
            return true;
        }
        else{
            return false;
        }

    }


    /** GET TELECONSULTI */ 
    //!FIXME: RETURN DUPLICATED RECORD WHEN ROLE IS MEDICO 
    public static function getTeleconsulti($request){

        $dataInizio = $request->dataInizio;
        $dataFine = $request->dataFine;

        $stato = $request->stato;
        
        $user = JWTAuth::parseToken()->authenticate();
        $role = $user->role;
        $userId = $user->id;

        $teleconsulto = Teleconsulto::Select('idTeleconsulto','dataTeleconsulto','oraTeleconsulto','ora_fine','teleconsulti.stato','prezzo','messaggio')
        ->where('pazienti_medici.idMedico','=',$userId);
/*
        if(!empty($request->dataInizio) && !empty($request->dataFine)){
            $teleconsulto = $teleconsulto->whereBetween('dataTeleconsulto',[$dataInizio,$dataFine]);
        }

        if(!empty($stato) && $stato!= '0'){
            $teleconsulto = $teleconsulto->where('stato','=',$stato);
        }
        */

        //CONTROLLO SE ADMIN
        /*if($role== 'admin'){
            $teleconsulto = $teleconsulto->where('teleconsulti.idPaziente','=',$idPaziente);
        }*/
        //CONTROLLO SE MEDICO
         if($role=='medico'){
            $teleconsulto = $teleconsulto->leftjoin('pazienti_medici','pazienti_medici.idMedico','=','teleconsulti.idMedico');
        }

        $teleconsulto = $teleconsulto->orderBy('dataTeleconsulto','DESC')->limit(50)->get()->toArray();

        return $teleconsulto;
    

    }


    /** VALIDATE TELECONSULTO */
    public static function validateTeleconsulto($request){

        $message = '';

        $idMedico = $request->idMedico;
        $dataTeleconsulto = $request->dataTeleconsulto;
        $oraTeleconsulto = $request->oraTeleconsulto;
        $messaggio = $request->messaggio;
        
        if(!v::notEmpty()->validate($idMedico)){
            $message = 'Seleziona Medico';
        }
        else if(!v::notEmpty()->validate($messaggio)){
            $message = 'Inserisci Messaggio';
        }
       
        if($message != ''){
            return $message;
        }
        else{
            return true;
        }


    }

}
