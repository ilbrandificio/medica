<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use App\Models\Allarmi;

class DatiClinici extends Model
{
    protected $table = 'dati_clinici';
    protected $primaryKey = 'idDatiClinici';
    protected $guarded = ['idDatiClinici'];

    public static function store($request){

        $validate = DatiClinici::validate($request);
        $data = '';

        if(!empty($request->data) && !empty($request->ora)){
            $data = $request->data . ' ' . $request->ora ;
        }
        else{
            $data = date('Y-m-d H:i:s');
        }

        if($validate === true){

            $dati_clinici =  DatiClinici::create(array(
                'idPaziente'=>$request->idPaziente,
                'dati_clinici'=> serialize($request->daticlinici),
                'data'=> $data
            ));

            if($dati_clinici){
                Allarmi::checkAllarmiClinici($request->daticlinici, $request->idPaziente);
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return $validate;
        }

    }

    public static function aggiorna($request){

        $validate = DatiClinici::validate($request);
        $idDatiClinici = $request->idDatiClinici;

        if($validate === true){

            $dati_clinici =  DatiClinici::where('idDatiClinici','=',$idDatiClinici)
            ->update(array(
                'dati_clinici'=> serialize($request->daticlinici)
            ));

            if($dati_clinici){
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return $validate;
        }

    }

    public static function getDatiClinici($request){

        $idPaziente = $request->idPaziente;

        $dati_clinici = DatiClinici::Select('idDatiClinici','dati_clinici','data')
        ->where('idPaziente','=',$idPaziente)
        ->orderBy('data','DESC')
        ->get()->toArray();

        foreach($dati_clinici as $key=>$value){
            $dati_clinici[$key]['dati_clinici'] = unserialize($value['dati_clinici']);
        }

        return $dati_clinici;


    }


    /** GET LAST  */
    public static function getLastByIdPaziente($idPaziente){

        $dati_clinici = DatiClinici::Select('dati_clinici')->where('idPaziente','=',$idPaziente)
        ->latest('idDatiClinici')->limit(1)->get()->toArray();

        if($dati_clinici ){
            if(array_key_exists('dati_clinici',$dati_clinici[0])){
                $dati_clinici[0]['dati_clinici'] = unserialize($dati_clinici[0]['dati_clinici']);

                return $dati_clinici['0']['dati_clinici'];
            }
            else{
                return [];
            }
        }
        else{
            return [];
        }


    }


    public static function cancella($idDatiClinici){

        $dati_clinici = DatiClinici::where('idDatiClinici','=',$idDatiClinici)->delete();

        if($dati_clinici){
            return true;
        }
        else{
            return false;
        }

    }

    public static function validate($request){

        $message = '';

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }

}
