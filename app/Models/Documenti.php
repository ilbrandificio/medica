<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use JWTAuth;
use App\Models\Utenti;
use App\Models\Richieste;
use Intervention\Image\ImageManager as Image;
use Illuminate\Support\Str;

class Documenti extends Model
{
    protected $table = 'documenti';
    protected $primaryKey = 'idDocumento';
    protected $guarded = ['idDocumento'];


    public static function store($request,$idEntita= null)
    {

        $tipo = $request->tipo;

        if($tipo === 'esame' && $idEntita!== null){
            $idEntita = $idEntita;
        }

        if($tipo === 'visita'){
            $idEntita = $request->idAppuntamento;
        }

        $storage_path=storage_path('documenti/');
        if(!is_dir($storage_path)) {
            mkdir($storage_path, 0755, true);
        }

        $files=$request->get('files');

        //SE IL FILE ESISTE
        if($files)
        {

        foreach($files as $key=>$file){
            $random=Str::random();
        //GENERO UN NOME
                $name = time().$random.'.'. explode('/', explode(':', substr($file['file'], 0, strpos($file['file'], ';')))[1])[1];
                //SE è UN PDF
                if($file['estensione']=='application/pdf'){
                    $strip_file= explode(',', $file['file']);
                    file_put_contents($storage_path.$name,base64_decode($strip_file['1']));
                }
                    else if($file['estensione']=='image/jpg' || $file['estensione']=='image/jpeg' || $file['estensione']=='image/png' ){
                    // $name = time().'.' . explode('/', explode(':', substr($file['file'], 0, strpos($file['file'], ';')))[1])[1];
                        $upload_status=\Image::make($file['file'])->save($storage_path.$name);
                        }
                if (!file_exists($storage_path.$name)) {
                    return array("status"=>false,"message"=>'Errore caricamento');
                }

                $documento =  Documenti::create(array(
                    'idEntita'=> $idEntita,
                    'nome_file'=> $file['nome'],
                    'tipo'=> $tipo,
                    'url'=>$name,
                ));

        //SE è CARICATO CORRETTAMENTE AGGIORNO LA RICHIESTA
                if($documento){
                        return array('status'=> true, 'message' => '');
                    }
                    else{
                        //ERRORE AGGIORNAMENTO RICHIESTA
                        return array('status'=> false, 'message' => 'Errore Aggiornamento Richiesta');
                    }

            }

        }//SE NON CI SONO FILES
            else{
                return array("status"=>false,"message"=>'Nessun documento selezionato');
            }

        }


    /**
     * @param $idEntita
     * @param $tipo
     * @return array
     */
    public static function getDocumentiByIdEntita($idEntita,$tipo)
    {
        
        $documenti=Documenti::where('idEntita','=',$idEntita)
        ->where('tipo','=',$tipo)->get()->toArray();

        return $documenti;
    }

}
