<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use App\Models\PazientiMedici;

class Terapie extends Model
{

    protected $table = 'terapie';
    protected $primaryKey = 'idTerapia';
    protected $guarded = ['idTerapia'];

/* CARICA TERAPIA */
    public static function getTerapiaPaziente($id){
        $terapia = Terapie::select()->where('idPaziente','=',$id)->get()->toArray();
        return $terapia;
    }


    /* APRI TERAPIA */
    public static function getTerapia($idTerapia){
        $terapia = Terapie::Select('idTerapia','idPaziente','farmaco','dosaggio','somministrazione','numero_orario_somministrazione','durata_trattamento','ssn')
        ->where('idTerapia','=',$idTerapia)->get()->toArray();

        return $terapia;
    }


/*  INSERISCI TERAPIA */
    public static function store($request){

        $validate=Terapie::validate($request);
        if($validate===true){
            $terapia = Terapie::create(array(
                "idPaziente"=>$request->idPaziente,
                'farmaco'=>$request->farmaco,
                'dosaggio'=>$request->dosaggio,
                'somministrazione'=>$request->somministrazione,
                'numero_orario_somministrazione'=>$request->numero_orario_somministrazione,
                'durata_trattamento'=>$request->durata_trattamento,
                'ssn'=>$request->ssn
                ));

            if($terapia){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

    /* Aggiorna Terapia */
    public static function aggiorna($request){
        $validate = Terapie::validate($request);
        $idTerapia = $request->idTerapia;

        if($validate===true){
            $terapia = Terapie::where('idTerapia','=',$idTerapia)->update(array(
                'farmaco'=>$request->farmaco,
                'dosaggio'=>$request->dosaggio,
                'somministrazione'=>$request->somministrazione,
                'numero_orario_somministrazione'=>$request->numero_orario_somministrazione,
                'durata_trattamento'=>$request->durata_trattamento,
                'ssn'=>$request->ssn
                ));
            if($terapia){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }
    }


    public static function cancella($idTerapia){
        $terapia = Terapie::where('idTerapia','=',$idTerapia)->delete();

        if($terapia){
            return true;
        }
        else{
            return false;
        }
    }

    public static function validate($request){
        $idPaziente=$request->idPaziente;
        $farmaco=$request->farmaco;
        $dosaggio=$request->dosaggio;
        $somministrazione=$request->somministrazione;
        $numero_orario_somministrazione=$request->numero_orario_somministrazione;
        $durata_trattamento=$request->durata_trattamento;
        $ssn=$request->ssn;

        return true;
    }
}
