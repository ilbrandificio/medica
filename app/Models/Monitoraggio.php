<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use App\Models\ApiKey;

class Monitoraggio extends Model
{

    protected $table = 'monitoraggio';
    protected $primaryKey = 'idMonitoraggio';
    protected $guarded = ['idMonitoraggio'];


/*  INSERISCI TERAPIA */
    public static function store($request){

        $validate=Monitoraggio::validate($request);
        if($validate===true){
            $idPaziente=ApiKey::getIdPazienteByApiKey($request->api_key);

            $result = Monitoraggio::create(array(
                'idPaziente'=>$idPaziente,
                'tipo_dispositivo' =>$request->tipo_dispositivo,
                'codice_dispositivo' =>$request->codice_dispositivo,
                'valori'=>$request->valori,
                ));

            if($result){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }

    }

    public static function validate($request){
        $idPaziente=$request->idPaziente;
        $tipo_dispositivo=$request->tipo_dispositivo;
        $codice_dispositivo=$request->codice_dispositivo;
        $valori=$request->valori;
        //AGGIUNGERE CONTROLLO CHE LA KEY NON ESISTA GIà PER IL PAZIENTE
/*
        if(!v::notEmpty()->validate($tipo_dispositivo)){
            $message = 'Inserisci tipo dispositivo';
        }
        if(!v::notEmpty()->validate($codice_dispositivo)){
            $message = 'Inserisci codice dispositivo';
        }
        if(!v::notEmpty()->validate($valori)){
            $message = 'Inserisci valori';
        }
*/
        return true;
    }
}
