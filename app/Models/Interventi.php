<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use App\Models\PazientiMedici;

class Interventi extends Model
{
    protected $table = 'interventi';
    protected $primaryKey = 'idIntervento';
    protected $guarded = ['idIntervento'];


    public static  function store($request){
        $validate=Interventi::validate($request);
        if($validate===true){
            $intervento = Interventi::create(array(
                'idIntervento'=>$request->idIntervento,
                'idPaziente' => $request->idPaziente,
                'dataIntervento'=>$request->dataIntervento,
                'nomeIntervento' => $request->nomeIntervento,
                'noteIntervento'=>$request->noteIntervento
                ));

            if($intervento){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return $validate;
        }
    }

    public static function aggiorna($request){


        $idIntervento = $request->idIntervento;
        $validate = Interventi::validate($request);


        if($validate === true){
            $intervento = Interventi::where('idIntervento','=',$idIntervento)->update(array(
                'dataIntervento'=>$request->dataIntervento,
                'nomeIntervento' => $request->nomeIntervento,
                'noteIntervento'=>$request->noteIntervento
            ));

            if($intervento){
                return true;
            }
            else{
                return false;
            }
        }
        else{
          return false;
        }


    }


    public static function getInterventi($request){

        $dataIntervento = $request->dataIntervento;
        $idPaziente = $request->idPaziente;

        $interventi = Interventi::Select('idIntervento','dataIntervento','nomeIntervento','noteIntervento')
        ->where('idPaziente','=',$idPaziente);

        if(!empty($dataIntervento)){
            $interventi = $interventi->where('dataIntervento','=',$dataIntervento);
        }

        $interventi = $interventi->get()->toArray();

        return $interventi;
    }

    public static function cancella($idIntervento){

        $intervento = Interventi::where('idIntervento','=',$idIntervento)->delete();

        if($intervento){
            return true;
        }
        else{
            return false;
        }

    }

    public static function validate($request){

        $message = '';

        $idPaziente=$request->idPaziente;
        $dataIntervento = $request->dataIntervento;
        $nomeIntervento = $request->nomeIntervento;
        $noteIntervento = $request->noteIntervento;

        if(!v::notEmpty()->validate($dataIntervento)){
            $message = 'Inserisci Data Intervento';
        }
        else if(!v::notEmpty()->validate($nomeIntervento)){
            $message = 'Inserisci Nome Intervento';
        }
        else if(!v::notEmpty()->validate($noteIntervento)){
            $message = 'Inserisci Note Intervento';
        }

        if($message != ''){
            return $message;
        }
        else{
            return true;
        }

    }
}
