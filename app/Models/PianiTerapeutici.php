<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use App\Models\Impostazioni;
    use Respect\Validation\Validator as v;
    use App\Models\PazientiMedici;
    use JWTAuth;

    class PianiTerapeutici extends Model{
        
        protected $table = 'piani_terapeutici';
        protected $primaryKey = 'idPianoTerapeutico';
        protected $guarded = ['idPianoTerapeutico'];


        public static function store($request){

            $validate = PianiTerapeutici::validate($request);

            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;

            if($validate === true){

                $new_request = $request->all();

                unset($new_request['busy']);
                unset($new_request['successful']);
                unset($new_request['errors']);
                unset($new_request['originalData']);
                unset($new_request['codicePianoTerapeutico']);
                unset($new_request['idPaziente']);

                $pianoTerapeutico = PianiTerapeutici::create(array(
                    'idPaziente' => $request->idPaziente,
                    'idMedico' => $userId,
                    'codicePianoTerapeutico' => $request->codicePianoTerapeutico,
                    'piano' => serialize($new_request)
                ));


                if($pianoTerapeutico){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return $validate;
            }

        }

        /*
            RECUPERO PIANO TERAPEUITICI
        */
        public static function getPianiTerapeutici($request){

            $idPaziente = $request->idPaziente;
            $codicepianoterapeutico = $request->codicepianoterapeutico;

            $pianiterapeutici = PianiTerapeutici::Select()
            ->where('idPaziente','=',$idPaziente);

            if($codicepianoterapeutico !='0'){
                $pianiterapeutici = $pianiterapeutici->where('codicePianoTerapeutico','=',$codicepianoterapeutico);
            }

            $pianiterapeutici = $pianiterapeutici->orderBy('created_at','DESC')->get()->toArray();

            return $pianiterapeutici;
        }


        /* AGGIORNA PIANO TERAPEUTICO */
        public static function aggiorna($request){

            $idPianoTerapeutico = $request->idPianoTerapeutico;
            $validate = PianiTerapeutici::validate($request);

            if($validate === true){
                $new_request = $request->all();
                unset($new_request['busy']);
                unset($new_request['successful']);
                unset($new_request['errors']);
                unset($new_request['originalData']);
                unset($new_request['codicePianoTerapeutico']);
                unset($new_request['idPaziente']);

                $pianoTerapeutico = PianiTerapeutici::where('idPianoTerapeutico','=',$idPianoTerapeutico)->update(array(
                    'piano' => serialize($new_request)
                ));


                if($pianoTerapeutico){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return $validate;
            }
        }

     
        public static function getPianoById($idPianoTerapeutico){
            
            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;
            

            $piano = PianiTerapeutici::Select()
            ->where('idPianoTerapeutico','=',$idPianoTerapeutico)->get()->toArray();

            if(!empty($piano)){
                $piano['0']['piano']= unserialize($piano['0']['piano']);
                
                $meta = UtentiMeta::getMetaInfo($piano['0']['idMedico']);

                $piano['0']['intestazione'] = $meta['intestazione_piano'];

            }
          
            return $piano['0'];
        }

        public static function cancella($idPianoTerapeutico){

            $piano = PianiTerapeutici::where('idPianoTerapeutico','=',$idPianoTerapeutico)
            ->delete();

            if($piano){
                return true;
            }
            else{
                return false;
            }

        }

        public static function validate($request){

            $message = '';

            $idPaziente = $request->idPaziente;
            $codicepianoterapeutico = $request->codicePianoTerapeutico;
            
      
            if(!v::notEmpty()->validate($codicepianoterapeutico)){
                $message = 'Inserisci Codice Piano Terapeutico';
            }


            if($message != ''){
                return $message;
            }
            else{
                return true;
            }
        }




    }


?>
