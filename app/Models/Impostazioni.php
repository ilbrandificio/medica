<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Respect\Validation\Validator as v;
use Carbon\Carbon;


class Impostazioni extends Model
{

    protected $table = 'impostazioni';
    protected $primaryKey = 'idImpostazione';
    protected $guarded = ['idImpostazione'];

    public static function store($key,$value){

          $impostazione = Impostazioni::create(array(
            'nomeImpostazione'=> $key,
            'valoreImpostazione'=> $value
          ));

        return true;
    }


    public static function aggiorna($request){

        $impostazioni = json_decode($request->getContent(),true);

        foreach($impostazioni as $key => $value){

            if(Impostazioni::checkNomeimpostazione($key)){
                $impostazione = Impostazioni::where('nomeImpostazione','=',$key)->update(array(
                    'valoreImpostazione'=> $value
                ));
            }
            else{
                Impostazioni::store($key,$value);
            }

        }

        return true;

    }

    /* Check Nome impostazione */
    public static function checkNomeimpostazione($key){

        $impostazione = Impostazioni::Select('nomeImpostazione')->where('nomeImpostazione','=',$key)->get()->toArray();

        if(count($impostazione)>0){
            return true;
        }
        else{
            return false;
        }
    }
//GET IMPOSTAZIONE SINGOLA
    public static function getImpostazione($nome){
        $impostazioni = Impostazioni::Select('valoreImpostazione')
        ->where('nomeImpostazione','=',$nome)
        ->get()->toArray();

        return $impostazioni[0]['valoreImpostazione'];
    }

    public static function getImpostazioni(){
        $new_array = array();
        $impostazioni = Impostazioni::Select('nomeImpostazione','valoreImpostazione')->get()->toArray();

        foreach($impostazioni as $key=>$value){
            $new_array[$impostazioni[$key]['nomeImpostazione']] = $impostazioni[$key]['valoreImpostazione'];
        }

        return $new_array;
    }

//RECUPERO IL GIORNO DIRETTAMENTE CON IL SUO ID
    public static function getGiornoImpostazioni($id){
        $impostazioni = Impostazioni::Select('valoreImpostazione')
        ->where('nomeImpostazione','=','giorno_'.$id)
        ->get()->toArray();

            return $impostazioni[0]['valoreImpostazione'];
    }

//GET ORARI DISPONIBILI

public static function getOrari(){

    $dataCorrente = Carbon::now();
    //$dataCorrente = $dataCorrente->isoFormat('d');
    $giornoSuccessivo=Carbon::parse($dataCorrente);

    $date_disponibili=[];
    array_push($date_disponibili, Carbon::parse($dataCorrente)->format('Y-m-d'));

    for($i=0;$i<4;$i++){
        $giornoSuccessivo->addDays(1);
        $idgiorno = $giornoSuccessivo->isoFormat('d');
        $giorniScarto=Impostazioni::getGiornoImpostazioni($idgiorno);

        $dataPartenza=Carbon::parse($giornoSuccessivo)->subDays($giorniScarto);
        //var_dump($dataCorrente);
        //var_dump($dataPartenza);
        //var_dump($giornoSuccessivo);

            if($dataCorrente->between($dataPartenza,$giornoSuccessivo)){
                array_push($date_disponibili, $giornoSuccessivo->format('Y-m-d'));
            }
        }
        return $date_disponibili;
    }
}
