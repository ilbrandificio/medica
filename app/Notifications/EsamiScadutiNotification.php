<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EsamiScadutiNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($esame)
    {
        $this->esame = $esame;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $codiceEsame = str_replace('_'," ",strtoupper($this->esame['codiceEsame']));

        try{
          $message = (new MailMessage)
            ->subject('Esame: '.$codiceEsame.' scaduto - Medica')
            ->view(
                'emails.esami.esamiscaduti',['esame'=> $this->esame]
            );
            
          return $message; 
        }
        catch(\Exception $e){
            echo ($e);
            exit();
        }
       

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'esame'=> $this->$esame
        ];
    }
}
