<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PrescrizioniEsamiPromemoria extends Notification
{
    use Queueable;
    private $prescrizione;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($prescrizione)
    {
        $this->prescrizione = $prescrizione;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //$nome_prescrizione = $this->prescrizione['nome_prescrizione'];

        try{
            $message = (new MailMessage)
                ->subject('Promemoria Prescrizione - Medica')
                ->view(
                    'emails.prescrizioni.prescrizionipromemoria',['prescrizione'=> $this->prescrizione]
                );

            return $message;
        }
        catch(\Exception $e){
            echo ($e);
            exit();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
