<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Auth\Notifications\VerifyEmail as Notification;
use Illuminate\Support\Facades\URL;
use Mail;

class VerifyEmail extends Notification
{
    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {

        $email=$notifiable->email;
        $nome=$notifiable->nome;

        $url = URL::temporarySignedRoute(
            'verification.verify', Carbon::now()->addMinutes(60), ['user' => $notifiable->id]
        );

        $url=str_replace('/api', '', $url);
        $data = array(
            'email'=>$email,
            'nome'=>$nome,
            'url'=>$url,
        );

        Mail::send('emails.verify',$data, function($message) use($email,$nome,$url) {
            $message->to($email, $nome)
                    ->subject('Verifica la tua Email');
        });
        exit();
    }
}
