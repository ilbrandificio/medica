<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReminderInr extends Notification
{
    use Queueable;
    private $anticoagulante;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($anticoagulante)
    {
        $this->anticoagulante = $anticoagulante;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $nome_anticoagulante = str_replace('_'," ",strtoupper($this->anticoagulante['anticoagulante']));

        try{
            $message = (new MailMessage)
                ->subject('Promemoria: '.$nome_anticoagulante.' inr non registrato - Medica')
                ->view(
                    'emails.anticoagulante.reminderinr',['anticoagulante'=> $this->anticoagulante]
                );

            return $message;
        }
        catch(\Exception $e){
            return true;
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
