<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(...$roles)
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user) {
            return $user->role == 'admin';
         });
         Gate::define('isCliente', function($user) {
             return $user->role == 'cliente';
         });
         Gate::define('isConsulente', function($user) {
            return $user->role == 'consulente';
        });
        Gate::define('isConsulenteFea', function($user) {
            return $user->role == 'consulente_fea';
        });
    }
}
