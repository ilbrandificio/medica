<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescrizioniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescrizioni', function (Blueprint $table) {
            $table->id('idPrescrizione');
            $table->bigInteger('idMedico');
            $table->bigInteger('idPaziente');
            $table->longText('farmaci');
            $table->boolean('stato')->default(0);
            $table->string('codice',6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescrizioni');
    }
}
