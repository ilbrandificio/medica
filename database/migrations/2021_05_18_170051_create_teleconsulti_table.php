<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeleconsultiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teleconsulti', function (Blueprint $table) {
            $table->id('idTeleconsulto');
            $table->bigInteger('idMedico');
            $table->bigInteger('idPaziente');
            $table->date('dataTeleconsulto')->nullable();
            $table->time('oraTeleconsulto')->nullable();
            $table->time('ora_fine')->nullable();
            $table->string('stato',255);
            $table->longText('messaggio');
            $table->decimal('prezzo',8,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teleconsulti');
    }
}
