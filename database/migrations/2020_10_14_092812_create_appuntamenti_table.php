<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppuntamentiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appuntamenti', function (Blueprint $table) {
            $table->bigIncrements('idAppuntamento');
            $table->bigInteger('idPaziente');
            $table->bigInteger('idMedico');
            $table->date('dataAppuntamento');
            $table->time('ora_inizio');
            $table->time('ora_fine');
            $table->longText('descrizione')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appuntamenti');
    }
}
