<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTerapiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terapie', function (Blueprint $table) {
            $table->bigIncrements('idTerapia');
            $table->unsignedBigInteger('idPaziente');
            $table->string('farmaco');
            $table->string('dosaggio')->nullable();
            $table->string('somministrazione')->nullable();
            $table->string('numero_orario_somministrazione')->nullable();
            $table->string('durata_trattamento')->nullable();
            $table->boolean('ssn')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terapie');
    }
}
