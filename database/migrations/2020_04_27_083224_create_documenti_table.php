<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documenti', function (Blueprint $table) {
            $table->bigIncrements('idDocumento');
            $table->unsignedBigInteger('idEntita');
            $table->string('tipo',255);
            $table->string('nome_file',512)->nullable();
            $table->string('url','512');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documenti');
    }
}
