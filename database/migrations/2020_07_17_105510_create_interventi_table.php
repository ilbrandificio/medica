<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterventiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interventi', function (Blueprint $table) {
            $table->id('idIntervento');
            $table->bigInteger('idPaziente');
            $table->date('dataIntervento');
            $table->string('nomeIntervento');
            $table->longText('noteIntervento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interventi');
    }
}
