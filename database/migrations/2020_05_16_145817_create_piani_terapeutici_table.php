<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePianiTerapeuticiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piani_terapeutici', function (Blueprint $table) {
            $table->bigIncrements('idPianoTerapeutico');
            $table->unsignedBigInteger('idPaziente');
            $table->bigInteger('idMedico');
            $table->string('codicePianoTerapeutico');
            $table->longText('piano');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piani_terapeutici');
    }
}
