<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePazientiDiagnosiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pazienti_diagnosi', function (Blueprint $table) {
            $table->id('idDiagnosi');
            $table->bigInteger('idPaziente');
            $table->date('dataDiagnosi');
            $table->string('codice');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pazienti_diagnosi');
    }
}
