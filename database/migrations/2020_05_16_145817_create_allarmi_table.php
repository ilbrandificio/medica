<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllarmiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allarmi', function (Blueprint $table) {
            $table->bigIncrements('idAllarme');
            $table->unsignedBigInteger('idPaziente');
            $table->string('codiceEsame');
            $table->string('chiave');
            $table->string('valore_min')->nullable();
            $table->string('valore_max')->nullable();
            $table->string('valore_registrato')->nullable();
            $table->set('stato',['pendente','attivo'])->default('pendente');
            $table->integer('ripetizioni')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allarmi');
    }
}
