<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePazientiMediciTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pazienti_medici', function (Blueprint $table) {
            $table->bigIncrements('idPazienteMedico');
            $table->unsignedBigInteger('idPaziente');
            $table->unsignedBigInteger('idMedico');
            $table->boolean('stato')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pazienti_medici');
    }
}
