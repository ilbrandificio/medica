<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitoraggioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoraggio', function (Blueprint $table) {
            $table->id('idMonitoraggio');
            $table->bigInteger('idPaziente');
            $table->string('tipo_dispositivo')->nullable();
            $table->string('codice_dispositivo')->nullable();
            $table->longText('valori')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoraggio');
    }
}
