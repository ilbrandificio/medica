<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEsamiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esami', function (Blueprint $table) {
            $table->bigIncrements('idEsame');
            $table->unsignedBigInteger('idPaziente');
            $table->string('caricamento');
            $table->date('data');
            $table->string('codiceEsame');
            $table->longText('esame')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esami');
    }
}
