<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescrizioniEsamiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescrizioni_esami', function (Blueprint $table) {
            $table->id('idPrescrizioneEsame');
            $table->bigInteger('idMedico');
            $table->bigInteger('idPaziente');
            $table->longText('esami');
            $table->date('data_scadenza');
            $table->decimal('perc',8,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescrizioni_esami');
    }
}
