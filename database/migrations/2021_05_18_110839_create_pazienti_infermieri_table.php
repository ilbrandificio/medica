<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePazientiInfermieriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pazienti_infermieri', function (Blueprint $table) {
            $table->id('idPazienteInfermiere');
            $table->bigInteger('idPaziente');
            $table->bigInteger('idInfermiere');
            $table->boolean('stato');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pazienti_infermieri');
    }
}
