<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatiCliniciTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dati_clinici', function (Blueprint $table) {
            $table->id('idDatiClinici');
            $table->bigInteger('idPaziente');
            $table->longText('dati_clinici');
            $table->dateTime('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrazione_dati_clinici');
        Schema::dropIfExists('dati_clinici');
    }
}
