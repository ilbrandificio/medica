<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnticoagulanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anticoagulante', function (Blueprint $table) {
            $table->id('idAnticoagulante');
            $table->bigInteger('idPaziente');
            $table->bigInteger('idMedico');
            $table->date('data_inizio');
            $table->date('data_fine');
            $table->string('anticoagulante',255);
            $table->longText('inr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anticoagulante');
    }
}
